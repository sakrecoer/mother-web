---
title: Coolometrics
description: >-
  After having released a first record, the urge of getting more out there was
  strong
date: 2005-06-21 00:00:00
author: set
category: releases
album: coolometrics
tags:
  - Coolometrics
  - Digital
  - reSet
  - Yamaha
  - Inti inc
image: /assets/img/posts/coolometrics.jpg
download:
link:
check_this_if_cannot_be_bought: true
stores:
  - name: ''
    url:
    icon:
    download: false
---

The digital era had&nbsp; just begun. Everyone was flashing portable MP3 players, and my website was up and running\! I knew digital distribution would get big. But i wanted to have something to sell. Because i never really believed in music being goods for trade.

I teamed up with Inti Inc who had access to screen-printing equipement to create a prestidigtal cover art. The packaging was a "magical" player the size of a CD containing a CD-R and a cardboard disk with a URL to download the music. It looked like one of those fancy MP3 players, with control wheel and a plug for your headphones. We made about 20 copies and they sold out fairly quick.