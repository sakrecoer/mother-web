---
title: House Of Me
description: >-
  With influences from the middle-east mythologies to the western post-modern
  occult organizations.
date: 2020-07-21 11:18:00
author: set
category: releases
album: the-house-of-set
tags:
  - House of Set
  - Digital
  - LP
  - Simio
image: /assets/img/posts/templeofsetcoverart-inv.jpg
download:
link:
check_this_if_cannot_be_bought: true
stores:
  - name:
    url:
    icon:
    download: false
---

Too catchy for being IDM, too harsh for the mainstream, the aim of this release is to make you dance and reflect in the same instant. The title is a homage to my own belief system. At some point in time i discovered The tempe of Set, which caught my interest. I don't identify as a Setian, but I'm definitely inclined towards the left-hand path of things and the power of the verb.

This records was published fall of 2012 under a Creative Commons. Attribution Non-Commercial ShareAlike 3.0 Unported License. (BY-NC-SA)<br>See http://creativecommons.org/licenses/by-nc-sa/3.0/legalcode for details.