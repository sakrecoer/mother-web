---
title: Santas Audioserver
description: This is the first official release of reSet Sakrecoer. It exists on Vinyl and CD
date: 2004-12-23 00:00:00
author: set
category: releases
album: santas-audioserver
tags:
  - santas audioserver
  - reSet
  - Album
  - CD
  - Vinyl
  - Digital
image: /assets/img/albums/santas-audioserver/10-santa-simulator.jpeg
download:
link:
check_this_if_cannot_be_bought: false
stores:
  - name: Bandcamp
    url: 'https://villamagica.bandcamp.com/album/santas-audioserver'
    icon: fa-bandcamp
    download: true
---
My first release with a record label. Villamagica in Geneva offered me a really good deal. I consider myself very lucky to have had the chance to work with them. Especially Stephan Armleder AKA the Geneva Heathen. But they are all awesome!
### Credits

* Artwork By – reSet Sakrecoer
* Lyrics By – [reSet Sakrecoer](https://www.discogs.com/artist/404853-reSet-Sakrecoer){: target="_blank"} (tracks: 1 to 5, 7, 10, 12)
* Mastered By – [Adam Rourke](https://www.discogs.com/artist/250555-Adam-Rourke){: target="_blank"}
* Mixed By – [Kid Rolex](https://www.discogs.com/artist/326881-Kid-Rolex){: target="_blank"}
* Vocals – [reSet Sakrecoer](https://www.discogs.com/artist/404853-reSet-Sakrecoer){: target="_blank"} (tracks: 1 to 4, 7, 8, 10, 12)
* Vocals – [Muji Ghostape](https://www.discogs.com/artist/404852-Muji-Ghostape) (track: 5. Backing vocals on track: 1)
* Lyrics By \[French Lyrics\] – Mansour (track: 8)

### Notes

This is the first album of reSet Sakrecoer. All tracks written and produced by him. The album do not mention the complete name of the artist, only reset.