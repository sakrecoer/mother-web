---
title: Psykedelisk Good Karma Pop
description: >-
  Originally released by Good Karma Records, my boyish Piaf journey on a
  pounding kick-ride.
date: 2010-09-06 00:00:00
author: set
category: releases
album: psykedelisk-pop
tags:
  - Psykedelisk Pop
  - Good Karma Records
  - Digital
  - LP
image: /assets/img/posts/06.jpg
download:
link:
check_this_if_cannot_be_bought: false
stores:
  - name: Bandcamp
    url: 'https://shop.basspistol.com/album/psykedelisk-pop'
    icon: fa-bandcamp
    download: true
  - name: Spotify
    url: 'https://open.spotify.com/album/4E3qH53ckzrDzyuGCF8GhZ'
    icon: fa-spotify
    download: false
  - name: Apple Music
    url: 'https://music.apple.com/album/psykedelisk-pop-album/417962741'
    icon: fa-apple
    download: false
  - name: Google Play
    url: 'https://play.google.com/store/music/album/reSet_Sakrecoer_Psykedelisk_Pop?id=Bf5t6hsyrerf3m7vnuon3yae2ry'
    icon: fa-google-play
    download: false
  - name: Deezer
    url: 'https://www.deezer.com/us/album/898513'
    icon: fa-creative-commons-sampling
    download: false
---

I had a bit of psychedelic experience due to lack of sleep and burnout. My good Friend [Dr.Tikov](http://www.tikov.com/){: target="_blank"} turned it around for me and offered me to harvest and channel it into something productive. Out came this album.

### Credits
All Music Written, performed, programmed, recorded, mixed, and arranged by reSet Sakrecoer.

Except:<br>
Track 3: Featuring Gregoire Iwaniec On Guitar<br>Track 4: Featuring Don Miguel De Suecia On Lead Vocals & Lyrics<br>Track 9: Feat Soulfed On Guitar & Dr.Tikov on Synths.

Mastered by Gregoire Iwaniec, Except “Step Up”: Mastered by reSet Sakrecoer.<br>Cover Art by Trans Ritarn and reSet Sakrecoer

This project was recorded between 2005 and 2010. It was made with love, for no particular reason. It is involving and dedicated to people. I hope you recognize yourself.<br>/reSet Sakrecoer

&copy;2010 - Some Rights Reserved. - http://sakrecoer.com Creative minded right holders.