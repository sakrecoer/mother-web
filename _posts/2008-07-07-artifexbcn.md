---
title: ArtifexBCN
description: >-
  3 years after I moved to Barcelona, i got signed by ArtifexBCN, a stricktly
  vinyl label based in Barcelona
date: 2008-07-07 00:00:00
author: set
category: releases
album: party-single
tags:
  - ArtifexBCN
  - reSet
  - Vinyl
  - Single
image: /assets/img/posts/artifex.jpg
download:
link:
check_this_if_cannot_be_bought: true
stores:
  - name: ''
    url:
    icon:
    download: false
---

I met one of my role models in Barcelona, Jack The Cutter is a french Catalan living in Barcelona who runs a very cool label of DIY vinyl records of premium quality. He literally took me under his wings, and helped me when i needed it the most. Not only by releasing my music, but on a more personal level too. Releasing with ArtifexBCN is part of the greatest pride and gratitude in my life.

The records was printed in a set of 20 copies that where sold out pretty quickly. All the vinyls on ArtifexBCN are hand 100% crafted, and so because of this artistic dimension we were able to push them for a decent price despite the music industry agonizing.