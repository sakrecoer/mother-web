---
title: Lonely Hearts Club Techno Band
description: 'Hand crafted Full length vinyl, proudly the 1 LP on ArtifexBCN'
date: 2008-10-17 00:00:00
author: set
category: releases
album: the-lonely-hearts-club-techno-band
tags:
  - ArtifexBCN
  - Vinyl
  - reSet
image: /assets/img/posts/img-20120514-214237.jpg
download:
link:
check_this_if_cannot_be_bought: true
stores:
  - name:
    url:
    icon:
    download: false
---

Not long after the Single, ArtifexBCN offered me to release a full length vinyl. A beautiful piece of handcrafted artwork. Transparent vinyl. Because ArtifexBCN cuts the vinyls, instead of molding them, it takes the length of the record just to produce the actual late. The cover was hand painted by me with spray using a stencil For this reason only 10 copies where made.

![](/assets/img/posts/img-20200720-143126-867.jpg){: width="1440" height="1920"}

This is by far my most profitable record because of the rarity and the hand made nature, i manage to sell my 5 copies for ~100€ each.

I am for ever thankful for Jack The Cutter's belief in me.