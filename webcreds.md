---
title: Webcreds
description: >-
  Information and credit regarding authors of tools, content and code used to
  build this website.
image: /assets/img/kaur-kristjan-cppf4w5pb1c-unsplash.jpg
layout: page
---

## CMS

Build with [Jekyll](https://jekyllrb.com/){: target="_blank"}<br>Using [Publikator](https://github.com/terminalnetwork/publikator){: target="_blank"} the rusty metal heart of the Basspistol release machine developed by [aengl.](https://github.com/terminalnetwork/publikator/commits?author=aengl){: target="_blank"}<br>Facilitated with [CloudCannon, the cloud CMS for Jekyll](https://cloudcannon.com/){: target="_blank"}<br>Hosted and served by [Basspistol](https://basspistol.com){: target="_blank"} and [Alsenet](https://www.alsenet.com){: target="_blank"}

## Framework

Based on *Story* by [html5up.net](https://html5up.net){: target="_blank"} \| @ajlkn Free for personal and commercial use under the CCA 3.0 [license](https://html5up.net/license){: target="_blank"}

## Artwork

* 404 Photo by [Kaur Kristjan](https://unsplash.com/@badgerblack?utm_source=unsplash&amp;utm_medium=referral&amp;utm_content=creditCopyText){: target="_blank"}