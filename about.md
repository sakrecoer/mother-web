---
title: About
layout: page
description: What is Sakrecoer about and what does its music stand for?
image: /assets/img/cs-vid-3.jpg
---

## Sakrecoer Uncorporated Stands for independent fun-loving Activism, Art & Music.

I am a 1980 born robot. If [Monica Zetterlund](https://en.wikipedia.org/wiki/Monica_Zetterlund){: target="_blank"} was a cyberpunk making electronic music, I would probably be Her. I aim for questions rather than answers and at pushing your creativity. I move by feet in the streets and value my integrity.

![](/assets/img/cs-vid-1.jpg){: width="350" height="227"}

I go by many aliases that you will find if you scroll down this page.

![](/assets/img/cs-vid-2.jpg){: width="270" height="180"}

This website was designed and executed by [me](http://set.hallstrom.ch){: target="_blank"} On these pages I push sounds, images, thoughts and passion for cool robots like you, thanks to the good [Alsenet.com](https://alsenet.com){: target="_blank"} people and the Helevtico-Russian-Swedo-Geramno-Spanglish Linux Lovers Circle.

![](/assets/img/cs-vid-4.jpg){: width="270" height="180"}

The past gig list is to long times and for forgotten times\! Received with enthusiasm by several venues, big and small such as:

* Apolo Nitsa - Barcelona
* Dachkantine - Zürich
* Center Of Contemporary Art - Geneva
* Alcazar - Stockholm
* Microdisco Festival - Berlin
* Button Factory - Dublin
* And many many other really really nice ones :) delivering good mood, beat, base and bass\!

![](/assets/img/sakrecoerLIVE.png){: width="639" height="799"}

&nbsp;