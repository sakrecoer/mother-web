---
layout: track
path: >-
  /mnt/usb32gb/organized-sakrecoer.media/assets/albums/qy70/6-inner-city-blues.mp3
audio: /assets/albums/qy70/6-inner-city-blues.mp3
slug: qy70/6-inner-city-blues
albumSlug: qy70
trackSlug: 6-inner-city-blues
coverPath: >-
  /mnt/usb32gb/organized-sakrecoer.media/assets/albums/qy70/6-inner-city-blues.jpeg
cover: /assets/albums/qy70/6-inner-city-blues.jpeg
format:
  tagTypes:
    - ID3v2.4
  trackInfo: []
  lossless: false
  container: MPEG
  codec: MPEG 1 Layer 3
  sampleRate: 44100
  numberOfChannels: 2
  bitrate: 160000
  codecProfile: CBR
  numberOfSamples: 7013376
  duration: 159.0334693877551
native:
  ID3v2.4:
    - id: TALB
      value: QY70
    - id: TDRC
      value: '2003'
    - id: TCMP
      value: ''
    - id: TCON
      value: Alternative Pop
    - id: TCOM
      value: reSet Sakrecoer
    - id: TIT2
      value: Inner City Blues
    - id: TRCK
      value: '6'
    - id: COMM
      value: &ref_0
        language: eng
        description: ''
        text: 'https://sakrecoer.com (cc) licensed http://creativecommon.org'
    - id: TPE1
      value: reSet Sakrecoer
quality:
  warnings: []
common:
  track:
    'no': 6
    of: null
  disk:
    'no': null
    of: null
  album: QY70
  year: 2003
  date: '2003'
  compilation: ''
  genre:
    - Alternative Pop
  composer:
    - reSet Sakrecoer
  title: Inner City Blues
  comment:
    - 'https://sakrecoer.com (cc) licensed http://creativecommon.org'
  artists:
    - reSet Sakrecoer
  artist: reSet Sakrecoer
transformed:
  ID3v2.4:
    TALB: QY70
    TDRC: '2003'
    TCMP: ''
    TCON: Alternative Pop
    TCOM: reSet Sakrecoer
    TIT2: Inner City Blues
    TRCK: '6'
    COMM: *ref_0
    TPE1: reSet Sakrecoer
all:
  TALB: QY70
  TDRC: '2003'
  TCMP: ''
  TCON: Alternative Pop
  TCOM: reSet Sakrecoer
  TIT2: Inner City Blues
  TRCK: '6'
  COMM: *ref_0
  TPE1: reSet Sakrecoer
nextTrack:
  path: >-
    /mnt/usb32gb/organized-sakrecoer.media/assets/albums/qy70/7-les-filles-des-quartiers-chics-feat-jonction-boys.mp3
  audio: /assets/albums/qy70/7-les-filles-des-quartiers-chics-feat-jonction-boys.mp3
  slug: qy70/7-les-filles-des-quartiers-chics-feat-jonction-boys
  albumSlug: qy70
  trackSlug: 7-les-filles-des-quartiers-chics-feat-jonction-boys
  coverPath: >-
    /mnt/usb32gb/organized-sakrecoer.media/assets/albums/qy70/7-les-filles-des-quartiers-chics-feat-jonction-boys.jpeg
  cover: /assets/albums/qy70/7-les-filles-des-quartiers-chics-feat-jonction-boys.jpeg
  format:
    tagTypes:
      - ID3v2.4
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 192000
    codecProfile: CBR
    numberOfSamples: 10674432
    duration: 242.05061224489796
  native:
    ID3v2.4:
      - id: TIT2
        value: Les Filles Des Quartiers Chics feat. Jonction Boys
      - id: TALB
        value: QY70
      - id: TDRC
        value: '2003'
      - id: TCON
        value: Alternative Pop
      - id: TCMP
        value: ''
      - id: TCOM
        value: reSet Sakrecoer
      - id: TRCK
        value: '7'
      - id: COMM
        value: &ref_1
          language: eng
          description: ''
          text: 'https://sakrecoer.com (cc) licensed http://creativecommon.org'
      - id: TPE1
        value: reSet Sakrecoer
  quality:
    warnings: []
  common:
    track:
      'no': 7
      of: null
    disk:
      'no': null
      of: null
    title: Les Filles Des Quartiers Chics feat. Jonction Boys
    album: QY70
    year: 2003
    date: '2003'
    genre:
      - Alternative Pop
    compilation: ''
    composer:
      - reSet Sakrecoer
    comment:
      - 'https://sakrecoer.com (cc) licensed http://creativecommon.org'
    artists:
      - reSet Sakrecoer
    artist: reSet Sakrecoer
  transformed:
    ID3v2.4:
      TIT2: Les Filles Des Quartiers Chics feat. Jonction Boys
      TALB: QY70
      TDRC: '2003'
      TCON: Alternative Pop
      TCMP: ''
      TCOM: reSet Sakrecoer
      TRCK: '7'
      COMM: *ref_1
      TPE1: reSet Sakrecoer
  all:
    TIT2: Les Filles Des Quartiers Chics feat. Jonction Boys
    TALB: QY70
    TDRC: '2003'
    TCON: Alternative Pop
    TCMP: ''
    TCOM: reSet Sakrecoer
    TRCK: '7'
    COMM: *ref_1
    TPE1: reSet Sakrecoer
previousTrack:
  path: >-
    /mnt/usb32gb/organized-sakrecoer.media/assets/albums/qy70/5-koetochblod-rubabdub.mp3
  audio: /assets/albums/qy70/5-koetochblod-rubabdub.mp3
  slug: qy70/5-koetochblod-rubabdub
  albumSlug: qy70
  trackSlug: 5-koetochblod-rubabdub
  coverPath: >-
    /mnt/usb32gb/organized-sakrecoer.media/assets/albums/qy70/5-koetochblod-rubabdub.jpeg
  cover: /assets/albums/qy70/5-koetochblod-rubabdub.jpeg
  format:
    tagTypes:
      - ID3v2.4
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 192000
    codecProfile: CBR
    numberOfSamples: 10585728
    duration: 240.03918367346938
  native:
    ID3v2.4:
      - id: TALB
        value: QY70
      - id: TDRC
        value: '2003'
      - id: TCMP
        value: ''
      - id: TIT2
        value: Koetochblod Rubabdub
      - id: TCON
        value: Alternative Pop
      - id: TCOM
        value: reSet Sakrecoer
      - id: TRCK
        value: '5'
      - id: COMM
        value: &ref_2
          language: eng
          description: ''
          text: 'https://sakrecoer.com (cc) licensed http://creativecommon.org'
      - id: TPE1
        value: reSet Sakrecoer
  quality:
    warnings: []
  common:
    track:
      'no': 5
      of: null
    disk:
      'no': null
      of: null
    album: QY70
    year: 2003
    date: '2003'
    compilation: ''
    title: Koetochblod Rubabdub
    genre:
      - Alternative Pop
    composer:
      - reSet Sakrecoer
    comment:
      - 'https://sakrecoer.com (cc) licensed http://creativecommon.org'
    artists:
      - reSet Sakrecoer
    artist: reSet Sakrecoer
  transformed:
    ID3v2.4:
      TALB: QY70
      TDRC: '2003'
      TCMP: ''
      TIT2: Koetochblod Rubabdub
      TCON: Alternative Pop
      TCOM: reSet Sakrecoer
      TRCK: '5'
      COMM: *ref_2
      TPE1: reSet Sakrecoer
  all:
    TALB: QY70
    TDRC: '2003'
    TCMP: ''
    TIT2: Koetochblod Rubabdub
    TCON: Alternative Pop
    TCOM: reSet Sakrecoer
    TRCK: '5'
    COMM: *ref_2
    TPE1: reSet Sakrecoer
---
