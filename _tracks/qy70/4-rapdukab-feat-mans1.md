---
layout: track
path: >-
  /mnt/usb32gb/organized-sakrecoer.media/assets/albums/qy70/4-rapdukab-feat-mans1.mp3
audio: /assets/albums/qy70/4-rapdukab-feat-mans1.mp3
slug: qy70/4-rapdukab-feat-mans1
albumSlug: qy70
trackSlug: 4-rapdukab-feat-mans1
coverPath: >-
  /mnt/usb32gb/organized-sakrecoer.media/assets/albums/qy70/4-rapdukab-feat-mans1.jpeg
cover: /assets/albums/qy70/4-rapdukab-feat-mans1.jpeg
format:
  tagTypes:
    - ID3v2.4
  trackInfo: []
  lossless: false
  container: MPEG
  codec: MPEG 1 Layer 3
  sampleRate: 44100
  numberOfChannels: 2
  bitrate: 192000
  codecProfile: CBR
  numberOfSamples: 9965952
  duration: 225.98530612244897
native:
  ID3v2.4:
    - id: TALB
      value: QY70
    - id: TDRC
      value: '2003'
    - id: TCON
      value: Alternative Pop
    - id: TIT2
      value: Rapdukab feat. Mans1
    - id: TCMP
      value: ''
    - id: TCOM
      value: reSet Sakrecoer
    - id: TRCK
      value: '4'
    - id: COMM
      value: &ref_0
        language: eng
        description: ''
        text: 'https://sakrecoer.com (cc) licensed http://creativecommon.org'
    - id: TPE1
      value: reSet Sakrecoer
quality:
  warnings: []
common:
  track:
    'no': 4
    of: null
  disk:
    'no': null
    of: null
  album: QY70
  year: 2003
  date: '2003'
  genre:
    - Alternative Pop
  title: Rapdukab feat. Mans1
  compilation: ''
  composer:
    - reSet Sakrecoer
  comment:
    - 'https://sakrecoer.com (cc) licensed http://creativecommon.org'
  artists:
    - reSet Sakrecoer
  artist: reSet Sakrecoer
transformed:
  ID3v2.4:
    TALB: QY70
    TDRC: '2003'
    TCON: Alternative Pop
    TIT2: Rapdukab feat. Mans1
    TCMP: ''
    TCOM: reSet Sakrecoer
    TRCK: '4'
    COMM: *ref_0
    TPE1: reSet Sakrecoer
all:
  TALB: QY70
  TDRC: '2003'
  TCON: Alternative Pop
  TIT2: Rapdukab feat. Mans1
  TCMP: ''
  TCOM: reSet Sakrecoer
  TRCK: '4'
  COMM: *ref_0
  TPE1: reSet Sakrecoer
nextTrack:
  path: >-
    /mnt/usb32gb/organized-sakrecoer.media/assets/albums/qy70/5-koetochblod-rubabdub.mp3
  audio: /assets/albums/qy70/5-koetochblod-rubabdub.mp3
  slug: qy70/5-koetochblod-rubabdub
  albumSlug: qy70
  trackSlug: 5-koetochblod-rubabdub
  coverPath: >-
    /mnt/usb32gb/organized-sakrecoer.media/assets/albums/qy70/5-koetochblod-rubabdub.jpeg
  cover: /assets/albums/qy70/5-koetochblod-rubabdub.jpeg
  format:
    tagTypes:
      - ID3v2.4
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 192000
    codecProfile: CBR
    numberOfSamples: 10585728
    duration: 240.03918367346938
  native:
    ID3v2.4:
      - id: TALB
        value: QY70
      - id: TDRC
        value: '2003'
      - id: TCMP
        value: ''
      - id: TIT2
        value: Koetochblod Rubabdub
      - id: TCON
        value: Alternative Pop
      - id: TCOM
        value: reSet Sakrecoer
      - id: TRCK
        value: '5'
      - id: COMM
        value: &ref_1
          language: eng
          description: ''
          text: 'https://sakrecoer.com (cc) licensed http://creativecommon.org'
      - id: TPE1
        value: reSet Sakrecoer
  quality:
    warnings: []
  common:
    track:
      'no': 5
      of: null
    disk:
      'no': null
      of: null
    album: QY70
    year: 2003
    date: '2003'
    compilation: ''
    title: Koetochblod Rubabdub
    genre:
      - Alternative Pop
    composer:
      - reSet Sakrecoer
    comment:
      - 'https://sakrecoer.com (cc) licensed http://creativecommon.org'
    artists:
      - reSet Sakrecoer
    artist: reSet Sakrecoer
  transformed:
    ID3v2.4:
      TALB: QY70
      TDRC: '2003'
      TCMP: ''
      TIT2: Koetochblod Rubabdub
      TCON: Alternative Pop
      TCOM: reSet Sakrecoer
      TRCK: '5'
      COMM: *ref_1
      TPE1: reSet Sakrecoer
  all:
    TALB: QY70
    TDRC: '2003'
    TCMP: ''
    TIT2: Koetochblod Rubabdub
    TCON: Alternative Pop
    TCOM: reSet Sakrecoer
    TRCK: '5'
    COMM: *ref_1
    TPE1: reSet Sakrecoer
previousTrack:
  path: >-
    /mnt/usb32gb/organized-sakrecoer.media/assets/albums/qy70/3-teknoidskalle.mp3
  audio: /assets/albums/qy70/3-teknoidskalle.mp3
  slug: qy70/3-teknoidskalle
  albumSlug: qy70
  trackSlug: 3-teknoidskalle
  coverPath: >-
    /mnt/usb32gb/organized-sakrecoer.media/assets/albums/qy70/3-teknoidskalle.jpeg
  cover: /assets/albums/qy70/3-teknoidskalle.jpeg
  format:
    tagTypes:
      - ID3v2.4
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 192000
    codecProfile: CBR
    numberOfSamples: 9130752
    duration: 207.0465306122449
  native:
    ID3v2.4:
      - id: TDRC
        value: '2003'
      - id: TIT2
        value: Teknoidskalle
      - id: TALB
        value: QY70
      - id: TCON
        value: Alternative Pop
      - id: TCMP
        value: ''
      - id: TCOM
        value: reSet Sakrecoer
      - id: TRCK
        value: '3'
      - id: COMM
        value: &ref_2
          language: eng
          description: ''
          text: 'https://sakrecoer.com (cc) licensed http://creativecommon.org'
      - id: TPE1
        value: reSet Sakrecoer
  quality:
    warnings: []
  common:
    track:
      'no': 3
      of: null
    disk:
      'no': null
      of: null
    year: 2003
    date: '2003'
    title: Teknoidskalle
    album: QY70
    genre:
      - Alternative Pop
    compilation: ''
    composer:
      - reSet Sakrecoer
    comment:
      - 'https://sakrecoer.com (cc) licensed http://creativecommon.org'
    artists:
      - reSet Sakrecoer
    artist: reSet Sakrecoer
  transformed:
    ID3v2.4:
      TDRC: '2003'
      TIT2: Teknoidskalle
      TALB: QY70
      TCON: Alternative Pop
      TCMP: ''
      TCOM: reSet Sakrecoer
      TRCK: '3'
      COMM: *ref_2
      TPE1: reSet Sakrecoer
  all:
    TDRC: '2003'
    TIT2: Teknoidskalle
    TALB: QY70
    TCON: Alternative Pop
    TCMP: ''
    TCOM: reSet Sakrecoer
    TRCK: '3'
    COMM: *ref_2
    TPE1: reSet Sakrecoer
---
