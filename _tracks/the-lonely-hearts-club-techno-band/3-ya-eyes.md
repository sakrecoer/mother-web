---
layout: track
path: >-
  /mnt/usb32gb/organized-sakrecoer.media/assets/albums/the-lonely-hearts-club-techno-band/3-ya-eyes.mp3
audio: /assets/albums/the-lonely-hearts-club-techno-band/3-ya-eyes.mp3
slug: the-lonely-hearts-club-techno-band/3-ya-eyes
albumSlug: the-lonely-hearts-club-techno-band
trackSlug: 3-ya-eyes
coverPath: >-
  /mnt/usb32gb/organized-sakrecoer.media/assets/albums/the-lonely-hearts-club-techno-band/3-ya-eyes.jpeg
cover: /assets/albums/the-lonely-hearts-club-techno-band/3-ya-eyes.jpeg
format:
  tagTypes:
    - ID3v2.3
  trackInfo: []
  lossless: false
  container: MPEG
  codec: MPEG 1 Layer 3
  sampleRate: 44100
  numberOfChannels: 2
  bitrate: 128000
  codecProfile: CBR
  numberOfSamples: 11113344
  duration: 252.00326530612244
native:
  ID3v2.3:
    - id: TPE1
      value: reSet Sakrecoer
    - id: TIT2
      value: Ya Eyes
    - id: COMM
      value: &ref_0
        language: eng
        description: ''
        text: Released on ArtifexBCN
    - id: TALB
      value: The Lonely Hearts Club Techno Band
    - id: TRCK
      value: '3'
    - id: TCON
      value: Electro
    - id: TYER
      value: '2008'
quality:
  warnings: []
common:
  track:
    'no': 3
    of: null
  disk:
    'no': null
    of: null
  artists:
    - reSet Sakrecoer
  artist: reSet Sakrecoer
  title: Ya Eyes
  comment:
    - Released on ArtifexBCN
  album: The Lonely Hearts Club Techno Band
  genre:
    - Electro
  year: 2008
transformed:
  ID3v2.3:
    TPE1: reSet Sakrecoer
    TIT2: Ya Eyes
    COMM: *ref_0
    TALB: The Lonely Hearts Club Techno Band
    TRCK: '3'
    TCON: Electro
    TYER: '2008'
all:
  TPE1: reSet Sakrecoer
  TIT2: Ya Eyes
  COMM: *ref_0
  TALB: The Lonely Hearts Club Techno Band
  TRCK: '3'
  TCON: Electro
  TYER: '2008'
nextTrack:
  path: >-
    /mnt/usb32gb/organized-sakrecoer.media/assets/albums/the-lonely-hearts-club-techno-band/4-love-de-ma-life.mp3
  audio: /assets/albums/the-lonely-hearts-club-techno-band/4-love-de-ma-life.mp3
  slug: the-lonely-hearts-club-techno-band/4-love-de-ma-life
  albumSlug: the-lonely-hearts-club-techno-band
  trackSlug: 4-love-de-ma-life
  coverPath: >-
    /mnt/usb32gb/organized-sakrecoer.media/assets/albums/the-lonely-hearts-club-techno-band/4-love-de-ma-life.jpeg
  cover: /assets/albums/the-lonely-hearts-club-techno-band/4-love-de-ma-life.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 128000
    codecProfile: CBR
    numberOfSamples: 10584576
    duration: 240.0130612244898
  native:
    ID3v2.3:
      - id: TPE1
        value: reSet Sakrecoer
      - id: TCON
        value: Disco
      - id: TIT2
        value: Love De Ma Life
      - id: COMM
        value: &ref_1
          language: eng
          description: ''
          text: Released on ArtifexBCN
      - id: TALB
        value: The Lonely Hearts Club Techno Band
      - id: TRCK
        value: '4'
      - id: TYER
        value: '2008'
  quality:
    warnings: []
  common:
    track:
      'no': 4
      of: null
    disk:
      'no': null
      of: null
    artists:
      - reSet Sakrecoer
    artist: reSet Sakrecoer
    genre:
      - Disco
    title: Love De Ma Life
    comment:
      - Released on ArtifexBCN
    album: The Lonely Hearts Club Techno Band
    year: 2008
  transformed:
    ID3v2.3:
      TPE1: reSet Sakrecoer
      TCON: Disco
      TIT2: Love De Ma Life
      COMM: *ref_1
      TALB: The Lonely Hearts Club Techno Band
      TRCK: '4'
      TYER: '2008'
  all:
    TPE1: reSet Sakrecoer
    TCON: Disco
    TIT2: Love De Ma Life
    COMM: *ref_1
    TALB: The Lonely Hearts Club Techno Band
    TRCK: '4'
    TYER: '2008'
previousTrack:
  path: >-
    /mnt/usb32gb/organized-sakrecoer.media/assets/albums/the-lonely-hearts-club-techno-band/2-birthday-voltures.mp3
  audio: /assets/albums/the-lonely-hearts-club-techno-band/2-birthday-voltures.mp3
  slug: the-lonely-hearts-club-techno-band/2-birthday-voltures
  albumSlug: the-lonely-hearts-club-techno-band
  trackSlug: 2-birthday-voltures
  coverPath: >-
    /mnt/usb32gb/organized-sakrecoer.media/assets/albums/the-lonely-hearts-club-techno-band/2-birthday-voltures.jpeg
  cover: /assets/albums/the-lonely-hearts-club-techno-band/2-birthday-voltures.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 128000
    codecProfile: CBR
    numberOfSamples: 12245760
    duration: 277.68163265306123
  native:
    ID3v2.3:
      - id: TIT2
        value: Birthday Voltures
      - id: TPE1
        value: reSet Sakrecoer
      - id: TCON
        value: SynthPop
      - id: COMM
        value: &ref_2
          language: eng
          description: ''
          text: Released on ArtifexBCN
      - id: TALB
        value: The Lonely Hearts Club Techno Band
      - id: TRCK
        value: '2'
      - id: TYER
        value: '2008'
  quality:
    warnings: []
  common:
    track:
      'no': 2
      of: null
    disk:
      'no': null
      of: null
    title: Birthday Voltures
    artists:
      - reSet Sakrecoer
    artist: reSet Sakrecoer
    genre:
      - SynthPop
    comment:
      - Released on ArtifexBCN
    album: The Lonely Hearts Club Techno Band
    year: 2008
  transformed:
    ID3v2.3:
      TIT2: Birthday Voltures
      TPE1: reSet Sakrecoer
      TCON: SynthPop
      COMM: *ref_2
      TALB: The Lonely Hearts Club Techno Band
      TRCK: '2'
      TYER: '2008'
  all:
    TIT2: Birthday Voltures
    TPE1: reSet Sakrecoer
    TCON: SynthPop
    COMM: *ref_2
    TALB: The Lonely Hearts Club Techno Band
    TRCK: '2'
    TYER: '2008'
---
