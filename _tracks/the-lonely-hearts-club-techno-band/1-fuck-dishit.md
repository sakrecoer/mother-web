---
layout: track
path: >-
  /mnt/usb32gb/organized-sakrecoer.media/assets/albums/the-lonely-hearts-club-techno-band/1-fuck-dishit.mp3
audio: /assets/albums/the-lonely-hearts-club-techno-band/1-fuck-dishit.mp3
slug: the-lonely-hearts-club-techno-band/1-fuck-dishit
albumSlug: the-lonely-hearts-club-techno-band
trackSlug: 1-fuck-dishit
coverPath: >-
  /mnt/usb32gb/organized-sakrecoer.media/assets/albums/the-lonely-hearts-club-techno-band/1-fuck-dishit.jpeg
cover: /assets/albums/the-lonely-hearts-club-techno-band/1-fuck-dishit.jpeg
format:
  tagTypes:
    - ID3v2.3
  trackInfo: []
  lossless: false
  container: MPEG
  codec: MPEG 1 Layer 3
  sampleRate: 44100
  numberOfChannels: 2
  bitrate: 128000
  tool: LAME 3.100U
  codecProfile: CBR
  numberOfSamples: 15525504
  duration: 352.05224489795916
native:
  ID3v2.3:
    - id: TPE1
      value: reSet Sakrecoer
    - id: TCON
      value: Techno
    - id: TIT2
      value: Fuck Dishit
    - id: COMM
      value: &ref_0
        language: eng
        description: ''
        text: Released on ArtifexBCN
    - id: TALB
      value: The Lonely Hearts Club Techno Band
    - id: TRCK
      value: '1'
    - id: TYER
      value: '2008'
quality:
  warnings: []
common:
  track:
    'no': 1
    of: null
  disk:
    'no': null
    of: null
  artists:
    - reSet Sakrecoer
  artist: reSet Sakrecoer
  genre:
    - Techno
  title: Fuck Dishit
  comment:
    - Released on ArtifexBCN
  album: The Lonely Hearts Club Techno Band
  year: 2008
transformed:
  ID3v2.3:
    TPE1: reSet Sakrecoer
    TCON: Techno
    TIT2: Fuck Dishit
    COMM: *ref_0
    TALB: The Lonely Hearts Club Techno Band
    TRCK: '1'
    TYER: '2008'
all:
  TPE1: reSet Sakrecoer
  TCON: Techno
  TIT2: Fuck Dishit
  COMM: *ref_0
  TALB: The Lonely Hearts Club Techno Band
  TRCK: '1'
  TYER: '2008'
nextTrack:
  path: >-
    /mnt/usb32gb/organized-sakrecoer.media/assets/albums/the-lonely-hearts-club-techno-band/2-birthday-voltures.mp3
  audio: /assets/albums/the-lonely-hearts-club-techno-band/2-birthday-voltures.mp3
  slug: the-lonely-hearts-club-techno-band/2-birthday-voltures
  albumSlug: the-lonely-hearts-club-techno-band
  trackSlug: 2-birthday-voltures
  coverPath: >-
    /mnt/usb32gb/organized-sakrecoer.media/assets/albums/the-lonely-hearts-club-techno-band/2-birthday-voltures.jpeg
  cover: /assets/albums/the-lonely-hearts-club-techno-band/2-birthday-voltures.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 128000
    codecProfile: CBR
    numberOfSamples: 12245760
    duration: 277.68163265306123
  native:
    ID3v2.3:
      - id: TIT2
        value: Birthday Voltures
      - id: TPE1
        value: reSet Sakrecoer
      - id: TCON
        value: SynthPop
      - id: COMM
        value: &ref_1
          language: eng
          description: ''
          text: Released on ArtifexBCN
      - id: TALB
        value: The Lonely Hearts Club Techno Band
      - id: TRCK
        value: '2'
      - id: TYER
        value: '2008'
  quality:
    warnings: []
  common:
    track:
      'no': 2
      of: null
    disk:
      'no': null
      of: null
    title: Birthday Voltures
    artists:
      - reSet Sakrecoer
    artist: reSet Sakrecoer
    genre:
      - SynthPop
    comment:
      - Released on ArtifexBCN
    album: The Lonely Hearts Club Techno Band
    year: 2008
  transformed:
    ID3v2.3:
      TIT2: Birthday Voltures
      TPE1: reSet Sakrecoer
      TCON: SynthPop
      COMM: *ref_1
      TALB: The Lonely Hearts Club Techno Band
      TRCK: '2'
      TYER: '2008'
  all:
    TIT2: Birthday Voltures
    TPE1: reSet Sakrecoer
    TCON: SynthPop
    COMM: *ref_1
    TALB: The Lonely Hearts Club Techno Band
    TRCK: '2'
    TYER: '2008'
previousTrack:
  path: >-
    /mnt/usb32gb/organized-sakrecoer.media/assets/albums/the-lonely-hearts-club-techno-band/8-bizarfeeling.mp3
  audio: /assets/albums/the-lonely-hearts-club-techno-band/8-bizarfeeling.mp3
  slug: the-lonely-hearts-club-techno-band/8-bizarfeeling
  albumSlug: the-lonely-hearts-club-techno-band
  trackSlug: 8-bizarfeeling
  coverPath: >-
    /mnt/usb32gb/organized-sakrecoer.media/assets/albums/the-lonely-hearts-club-techno-band/8-bizarfeeling.jpeg
  cover: /assets/albums/the-lonely-hearts-club-techno-band/8-bizarfeeling.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 128000
    tool: LAME 3.100U
    codecProfile: CBR
    numberOfSamples: 9283968
    duration: 210.52081632653062
  native:
    ID3v2.3:
      - id: TPE1
        value: reSet Sakrecoer
      - id: TIT2
        value: Bizarfeeling
      - id: COMM
        value: &ref_2
          language: eng
          description: ''
          text: Released on ArtifexBCN
      - id: TALB
        value: The Lonely Hearts Club Techno Band
      - id: TRCK
        value: '8'
      - id: TCON
        value: Electro
      - id: TYER
        value: '2008'
  quality:
    warnings: []
  common:
    track:
      'no': 8
      of: null
    disk:
      'no': null
      of: null
    artists:
      - reSet Sakrecoer
    artist: reSet Sakrecoer
    title: Bizarfeeling
    comment:
      - Released on ArtifexBCN
    album: The Lonely Hearts Club Techno Band
    genre:
      - Electro
    year: 2008
  transformed:
    ID3v2.3:
      TPE1: reSet Sakrecoer
      TIT2: Bizarfeeling
      COMM: *ref_2
      TALB: The Lonely Hearts Club Techno Band
      TRCK: '8'
      TCON: Electro
      TYER: '2008'
  all:
    TPE1: reSet Sakrecoer
    TIT2: Bizarfeeling
    COMM: *ref_2
    TALB: The Lonely Hearts Club Techno Band
    TRCK: '8'
    TCON: Electro
    TYER: '2008'
---
