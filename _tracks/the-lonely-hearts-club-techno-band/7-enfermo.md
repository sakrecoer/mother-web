---
layout: track
path: >-
  /mnt/usb32gb/organized-sakrecoer.media/assets/albums/the-lonely-hearts-club-techno-band/7-enfermo.mp3
audio: /assets/albums/the-lonely-hearts-club-techno-band/7-enfermo.mp3
slug: the-lonely-hearts-club-techno-band/7-enfermo
albumSlug: the-lonely-hearts-club-techno-band
trackSlug: 7-enfermo
coverPath: >-
  /mnt/usb32gb/organized-sakrecoer.media/assets/albums/the-lonely-hearts-club-techno-band/7-enfermo.jpeg
cover: /assets/albums/the-lonely-hearts-club-techno-band/7-enfermo.jpeg
format:
  tagTypes:
    - ID3v2.3
  trackInfo: []
  lossless: false
  container: MPEG
  codec: MPEG 1 Layer 3
  sampleRate: 44100
  numberOfChannels: 2
  bitrate: 128000
  codecProfile: CBR
  numberOfSamples: 14224896
  duration: 322.56
native:
  ID3v2.3:
    - id: TPE1
      value: reSet Sakrecoer
    - id: TIT2
      value: Enfermo
    - id: COMM
      value: &ref_0
        language: eng
        description: ''
        text: Released on ArtifexBCN
    - id: TALB
      value: The Lonely Hearts Club Techno Band
    - id: TRCK
      value: '7'
    - id: TCON
      value: Electro
    - id: TYER
      value: '2008'
quality:
  warnings: []
common:
  track:
    'no': 7
    of: null
  disk:
    'no': null
    of: null
  artists:
    - reSet Sakrecoer
  artist: reSet Sakrecoer
  title: Enfermo
  comment:
    - Released on ArtifexBCN
  album: The Lonely Hearts Club Techno Band
  genre:
    - Electro
  year: 2008
transformed:
  ID3v2.3:
    TPE1: reSet Sakrecoer
    TIT2: Enfermo
    COMM: *ref_0
    TALB: The Lonely Hearts Club Techno Band
    TRCK: '7'
    TCON: Electro
    TYER: '2008'
all:
  TPE1: reSet Sakrecoer
  TIT2: Enfermo
  COMM: *ref_0
  TALB: The Lonely Hearts Club Techno Band
  TRCK: '7'
  TCON: Electro
  TYER: '2008'
nextTrack:
  path: >-
    /mnt/usb32gb/organized-sakrecoer.media/assets/albums/the-lonely-hearts-club-techno-band/8-bizarfeeling.mp3
  audio: /assets/albums/the-lonely-hearts-club-techno-band/8-bizarfeeling.mp3
  slug: the-lonely-hearts-club-techno-band/8-bizarfeeling
  albumSlug: the-lonely-hearts-club-techno-band
  trackSlug: 8-bizarfeeling
  coverPath: >-
    /mnt/usb32gb/organized-sakrecoer.media/assets/albums/the-lonely-hearts-club-techno-band/8-bizarfeeling.jpeg
  cover: /assets/albums/the-lonely-hearts-club-techno-band/8-bizarfeeling.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 128000
    tool: LAME 3.100U
    codecProfile: CBR
    numberOfSamples: 9283968
    duration: 210.52081632653062
  native:
    ID3v2.3:
      - id: TPE1
        value: reSet Sakrecoer
      - id: TIT2
        value: Bizarfeeling
      - id: COMM
        value: &ref_1
          language: eng
          description: ''
          text: Released on ArtifexBCN
      - id: TALB
        value: The Lonely Hearts Club Techno Band
      - id: TRCK
        value: '8'
      - id: TCON
        value: Electro
      - id: TYER
        value: '2008'
  quality:
    warnings: []
  common:
    track:
      'no': 8
      of: null
    disk:
      'no': null
      of: null
    artists:
      - reSet Sakrecoer
    artist: reSet Sakrecoer
    title: Bizarfeeling
    comment:
      - Released on ArtifexBCN
    album: The Lonely Hearts Club Techno Band
    genre:
      - Electro
    year: 2008
  transformed:
    ID3v2.3:
      TPE1: reSet Sakrecoer
      TIT2: Bizarfeeling
      COMM: *ref_1
      TALB: The Lonely Hearts Club Techno Band
      TRCK: '8'
      TCON: Electro
      TYER: '2008'
  all:
    TPE1: reSet Sakrecoer
    TIT2: Bizarfeeling
    COMM: *ref_1
    TALB: The Lonely Hearts Club Techno Band
    TRCK: '8'
    TCON: Electro
    TYER: '2008'
previousTrack:
  path: >-
    /mnt/usb32gb/organized-sakrecoer.media/assets/albums/the-lonely-hearts-club-techno-band/6-weed-buddy.mp3
  audio: /assets/albums/the-lonely-hearts-club-techno-band/6-weed-buddy.mp3
  slug: the-lonely-hearts-club-techno-band/6-weed-buddy
  albumSlug: the-lonely-hearts-club-techno-band
  trackSlug: 6-weed-buddy
  coverPath: >-
    /mnt/usb32gb/organized-sakrecoer.media/assets/albums/the-lonely-hearts-club-techno-band/6-weed-buddy.jpeg
  cover: /assets/albums/the-lonely-hearts-club-techno-band/6-weed-buddy.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 128000
    codecProfile: CBR
    numberOfSamples: 9970560
    duration: 226.08979591836734
  native:
    ID3v2.3:
      - id: TPE1
        value: reSet Sakrecoer
      - id: TCON
        value: Disco
      - id: TIT2
        value: Weed Buddy
      - id: COMM
        value: &ref_2
          language: eng
          description: ''
          text: Released on ArtifexBCN
      - id: TALB
        value: The Lonely Hearts Club Techno Band
      - id: TRCK
        value: '6'
      - id: TYER
        value: '2008'
  quality:
    warnings: []
  common:
    track:
      'no': 6
      of: null
    disk:
      'no': null
      of: null
    artists:
      - reSet Sakrecoer
    artist: reSet Sakrecoer
    genre:
      - Disco
    title: Weed Buddy
    comment:
      - Released on ArtifexBCN
    album: The Lonely Hearts Club Techno Band
    year: 2008
  transformed:
    ID3v2.3:
      TPE1: reSet Sakrecoer
      TCON: Disco
      TIT2: Weed Buddy
      COMM: *ref_2
      TALB: The Lonely Hearts Club Techno Band
      TRCK: '6'
      TYER: '2008'
  all:
    TPE1: reSet Sakrecoer
    TCON: Disco
    TIT2: Weed Buddy
    COMM: *ref_2
    TALB: The Lonely Hearts Club Techno Band
    TRCK: '6'
    TYER: '2008'
---
