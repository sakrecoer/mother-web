---
layout: track
path: >-
  /mnt/usb32gb/organized-sakrecoer.media/assets/albums/party-single/2-seducers-party.mp3
audio: /assets/albums/party-single/2-seducers-party.mp3
slug: party-single/2-seducers-party
albumSlug: party-single
trackSlug: 2-seducers-party
coverPath: >-
  /mnt/usb32gb/organized-sakrecoer.media/assets/albums/party-single/2-seducers-party.jpeg
cover: /assets/albums/party-single/2-seducers-party.jpeg
format:
  tagTypes:
    - ID3v2.3
  trackInfo: []
  lossless: false
  container: MPEG
  codec: MPEG 1 Layer 3
  sampleRate: 44100
  numberOfChannels: 2
  bitrate: 128000
  codecProfile: CBR
  numberOfSamples: 8112384
  duration: 183.9542857142857
native:
  ID3v2.3:
    - id: TIT2
      value: Seducers Party
    - id: TPE1
      value: reSet Sakrecoer
    - id: TALB
      value: Party Single
    - id: COMM
      value: &ref_0
        language: eng
        description: ''
        text: Released on ArtifexBCN
    - id: TRCK
      value: '2'
    - id: TCON
      value: Electro Funk
    - id: TCOM
      value: reSet Sakrecoer
    - id: TYER
      value: '2008'
quality:
  warnings: []
common:
  track:
    'no': 2
    of: null
  disk:
    'no': null
    of: null
  title: Seducers Party
  artists:
    - reSet Sakrecoer
  artist: reSet Sakrecoer
  album: Party Single
  comment:
    - Released on ArtifexBCN
  genre:
    - Electro Funk
  composer:
    - reSet Sakrecoer
  year: 2008
transformed:
  ID3v2.3:
    TIT2: Seducers Party
    TPE1: reSet Sakrecoer
    TALB: Party Single
    COMM: *ref_0
    TRCK: '2'
    TCON: Electro Funk
    TCOM: reSet Sakrecoer
    TYER: '2008'
all:
  TIT2: Seducers Party
  TPE1: reSet Sakrecoer
  TALB: Party Single
  COMM: *ref_0
  TRCK: '2'
  TCON: Electro Funk
  TCOM: reSet Sakrecoer
  TYER: '2008'
nextTrack: &ref_2
  path: >-
    /mnt/usb32gb/organized-sakrecoer.media/assets/albums/party-single/1-cafar-du-sonar.mp3
  audio: /assets/albums/party-single/1-cafar-du-sonar.mp3
  slug: party-single/1-cafar-du-sonar
  albumSlug: party-single
  trackSlug: 1-cafar-du-sonar
  coverPath: >-
    /mnt/usb32gb/organized-sakrecoer.media/assets/albums/party-single/1-cafar-du-sonar.jpeg
  cover: /assets/albums/party-single/1-cafar-du-sonar.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 128000
    codecProfile: CBR
    numberOfSamples: 5081472
    duration: 115.2261224489796
  native:
    ID3v2.3:
      - id: TPE1
        value: reSet Sakrecoer
      - id: TCOM
        value: reSet Sakrecoer
      - id: TALB
        value: Party Single
      - id: TIT2
        value: Cafar Du Sonar
      - id: COMM
        value: &ref_1
          language: eng
          description: ''
          text: Released on ArtifexBCN
      - id: TRCK
        value: '1'
      - id: TCON
        value: Electro Blues
      - id: TYER
        value: '2008'
  quality:
    warnings: []
  common:
    track:
      'no': 1
      of: null
    disk:
      'no': null
      of: null
    artists:
      - reSet Sakrecoer
    artist: reSet Sakrecoer
    composer:
      - reSet Sakrecoer
    album: Party Single
    title: Cafar Du Sonar
    comment:
      - Released on ArtifexBCN
    genre:
      - Electro Blues
    year: 2008
  transformed:
    ID3v2.3:
      TPE1: reSet Sakrecoer
      TCOM: reSet Sakrecoer
      TALB: Party Single
      TIT2: Cafar Du Sonar
      COMM: *ref_1
      TRCK: '1'
      TCON: Electro Blues
      TYER: '2008'
  all:
    TPE1: reSet Sakrecoer
    TCOM: reSet Sakrecoer
    TALB: Party Single
    TIT2: Cafar Du Sonar
    COMM: *ref_1
    TRCK: '1'
    TCON: Electro Blues
    TYER: '2008'
previousTrack: *ref_2
---
