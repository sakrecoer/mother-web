---
layout: track
path: >-
  /mnt/usb32gb/organized-sakrecoer.media/assets/albums/psykedelisk-pop/3-roevhjaelmen.mp3
audio: /assets/albums/psykedelisk-pop/3-roevhjaelmen.mp3
slug: psykedelisk-pop/3-roevhjaelmen
albumSlug: psykedelisk-pop
trackSlug: 3-roevhjaelmen
coverPath: >-
  /mnt/usb32gb/organized-sakrecoer.media/assets/albums/psykedelisk-pop/3-roevhjaelmen.jpeg
cover: /assets/albums/psykedelisk-pop/3-roevhjaelmen.jpeg
format:
  tagTypes:
    - ID3v2.3
  trackInfo: []
  lossless: false
  container: MPEG
  codec: MPEG 1 Layer 3
  sampleRate: 44100
  numberOfChannels: 2
  bitrate: 128000
  codecProfile: CBR
  numberOfSamples: 11242368
  duration: 254.92897959183674
native:
  ID3v2.3:
    - id: TIT2
      value: Roevhjaelmen
    - id: TPE1
      value: reSet Sakrecoer
    - id: TPE2
      value: reSet Sakrecoer
    - id: TCOM
      value: reSet Sakrecoer
    - id: TALB
      value: Psykedelisk Pop
    - id: TRCK
      value: 3/10
    - id: TCON
      value: House
    - id: COMM
      value:
        language: XXX
        description: Comment
        text: 'https://sakrecoer.com'
    - id: TYER
      value: '2010'
quality:
  warnings: []
common:
  track:
    'no': 3
    of: 10
  disk:
    'no': null
    of: null
  title: Roevhjaelmen
  artists:
    - reSet Sakrecoer
  artist: reSet Sakrecoer
  albumartist: reSet Sakrecoer
  composer:
    - reSet Sakrecoer
  album: Psykedelisk Pop
  genre:
    - House
  comment:
    - 'https://sakrecoer.com'
  year: 2010
transformed:
  ID3v2.3:
    TIT2: Roevhjaelmen
    TPE1: reSet Sakrecoer
    TPE2: reSet Sakrecoer
    TCOM: reSet Sakrecoer
    TALB: Psykedelisk Pop
    TRCK: 3/10
    TCON: House
    Comment: 'https://sakrecoer.com'
    TYER: '2010'
all:
  TIT2: Roevhjaelmen
  TPE1: reSet Sakrecoer
  TPE2: reSet Sakrecoer
  TCOM: reSet Sakrecoer
  TALB: Psykedelisk Pop
  TRCK: 3/10
  TCON: House
  Comment: 'https://sakrecoer.com'
  TYER: '2010'
nextTrack:
  path: >-
    /mnt/usb32gb/organized-sakrecoer.media/assets/albums/psykedelisk-pop/4-it-s-all-over.mp3
  audio: /assets/albums/psykedelisk-pop/4-it-s-all-over.mp3
  slug: psykedelisk-pop/4-it-s-all-over
  albumSlug: psykedelisk-pop
  trackSlug: 4-it-s-all-over
  coverPath: >-
    /mnt/usb32gb/organized-sakrecoer.media/assets/albums/psykedelisk-pop/4-it-s-all-over.jpeg
  cover: /assets/albums/psykedelisk-pop/4-it-s-all-over.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 128000
    codecProfile: CBR
    numberOfSamples: 11700864
    duration: 265.3257142857143
  native:
    ID3v2.3:
      - id: TIT2
        value: It's All Over
      - id: TPE1
        value: reSet Sakrecoer
      - id: TPE2
        value: reSet Sakrecoer
      - id: TCOM
        value: reSet Sakrecoer
      - id: TALB
        value: Psykedelisk Pop
      - id: TRCK
        value: 4/10
      - id: TCON
        value: Emo
      - id: COMM
        value:
          language: XXX
          description: Comment
          text: 'https://sakrecoer.com'
      - id: TYER
        value: '2010'
  quality:
    warnings: []
  common:
    track:
      'no': 4
      of: 10
    disk:
      'no': null
      of: null
    title: It's All Over
    artists:
      - reSet Sakrecoer
    artist: reSet Sakrecoer
    albumartist: reSet Sakrecoer
    composer:
      - reSet Sakrecoer
    album: Psykedelisk Pop
    genre:
      - Emo
    comment:
      - 'https://sakrecoer.com'
    year: 2010
  transformed:
    ID3v2.3:
      TIT2: It's All Over
      TPE1: reSet Sakrecoer
      TPE2: reSet Sakrecoer
      TCOM: reSet Sakrecoer
      TALB: Psykedelisk Pop
      TRCK: 4/10
      TCON: Emo
      Comment: 'https://sakrecoer.com'
      TYER: '2010'
  all:
    TIT2: It's All Over
    TPE1: reSet Sakrecoer
    TPE2: reSet Sakrecoer
    TCOM: reSet Sakrecoer
    TALB: Psykedelisk Pop
    TRCK: 4/10
    TCON: Emo
    Comment: 'https://sakrecoer.com'
    TYER: '2010'
previousTrack:
  path: >-
    /mnt/usb32gb/organized-sakrecoer.media/assets/albums/psykedelisk-pop/2-step-up.mp3
  audio: /assets/albums/psykedelisk-pop/2-step-up.mp3
  slug: psykedelisk-pop/2-step-up
  albumSlug: psykedelisk-pop
  trackSlug: 2-step-up
  coverPath: >-
    /mnt/usb32gb/organized-sakrecoer.media/assets/albums/psykedelisk-pop/2-step-up.jpeg
  cover: /assets/albums/psykedelisk-pop/2-step-up.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 128000
    codecProfile: CBR
    numberOfSamples: 8176896
    duration: 185.41714285714286
  native:
    ID3v2.3:
      - id: TIT2
        value: Step Up
      - id: TPE1
        value: reSet Sakrecoer
      - id: TPE2
        value: reSet Sakrecoer
      - id: TCOM
        value: reSet Sakrecoer
      - id: TALB
        value: Psykedelisk Pop
      - id: TRCK
        value: 2/10
      - id: TCON
        value: Hip-Hop
      - id: COMM
        value:
          language: XXX
          description: Comment
          text: 'https://sakrecoer.com'
      - id: TYER
        value: '2010'
  quality:
    warnings: []
  common:
    track:
      'no': 2
      of: 10
    disk:
      'no': null
      of: null
    title: Step Up
    artists:
      - reSet Sakrecoer
    artist: reSet Sakrecoer
    albumartist: reSet Sakrecoer
    composer:
      - reSet Sakrecoer
    album: Psykedelisk Pop
    genre:
      - Hip-Hop
    comment:
      - 'https://sakrecoer.com'
    year: 2010
  transformed:
    ID3v2.3:
      TIT2: Step Up
      TPE1: reSet Sakrecoer
      TPE2: reSet Sakrecoer
      TCOM: reSet Sakrecoer
      TALB: Psykedelisk Pop
      TRCK: 2/10
      TCON: Hip-Hop
      Comment: 'https://sakrecoer.com'
      TYER: '2010'
  all:
    TIT2: Step Up
    TPE1: reSet Sakrecoer
    TPE2: reSet Sakrecoer
    TCOM: reSet Sakrecoer
    TALB: Psykedelisk Pop
    TRCK: 2/10
    TCON: Hip-Hop
    Comment: 'https://sakrecoer.com'
    TYER: '2010'
---
