---
layout: track
path: >-
  /mnt/usb32gb/organized-sakrecoer.media/assets/albums/psykedelisk-pop/10-hidden-track.mp3
audio: /assets/albums/psykedelisk-pop/10-hidden-track.mp3
slug: psykedelisk-pop/10-hidden-track
albumSlug: psykedelisk-pop
trackSlug: 10-hidden-track
coverPath: >-
  /mnt/usb32gb/organized-sakrecoer.media/assets/albums/psykedelisk-pop/10-hidden-track.jpeg
cover: /assets/albums/psykedelisk-pop/10-hidden-track.jpeg
format:
  tagTypes:
    - ID3v2.3
  trackInfo: []
  lossless: false
  container: MPEG
  codec: MPEG 1 Layer 3
  sampleRate: 44100
  numberOfChannels: 2
  bitrate: 128000
  codecProfile: CBR
  numberOfSamples: 6984576
  duration: 158.38040816326532
native:
  ID3v2.3:
    - id: TIT2
      value: HIdden Track
    - id: TPE1
      value: reSet Sakrecoer
    - id: TPE2
      value: reSet Sakrecoer
    - id: TCOM
      value: reSet Sakrecoer
    - id: TALB
      value: Psykedelisk Pop
    - id: TRCK
      value: 10/10
    - id: TBPM
      value: '127'
    - id: TCON
      value: Punk
    - id: COMM
      value:
        language: XXX
        description: Comment
        text: 'https://sakrecoer.com'
    - id: TYER
      value: '2010'
quality:
  warnings: []
common:
  track:
    'no': 10
    of: 10
  disk:
    'no': null
    of: null
  title: HIdden Track
  artists:
    - reSet Sakrecoer
  artist: reSet Sakrecoer
  albumartist: reSet Sakrecoer
  composer:
    - reSet Sakrecoer
  album: Psykedelisk Pop
  bpm: '127'
  genre:
    - Punk
  comment:
    - 'https://sakrecoer.com'
  year: 2010
transformed:
  ID3v2.3:
    TIT2: HIdden Track
    TPE1: reSet Sakrecoer
    TPE2: reSet Sakrecoer
    TCOM: reSet Sakrecoer
    TALB: Psykedelisk Pop
    TRCK: 10/10
    TBPM: '127'
    TCON: Punk
    Comment: 'https://sakrecoer.com'
    TYER: '2010'
all:
  TIT2: HIdden Track
  TPE1: reSet Sakrecoer
  TPE2: reSet Sakrecoer
  TCOM: reSet Sakrecoer
  TALB: Psykedelisk Pop
  TRCK: 10/10
  TBPM: '127'
  TCON: Punk
  Comment: 'https://sakrecoer.com'
  TYER: '2010'
nextTrack:
  path: >-
    /mnt/usb32gb/organized-sakrecoer.media/assets/albums/psykedelisk-pop/1-janne-i-min-hj-rna.mp3
  audio: /assets/albums/psykedelisk-pop/1-janne-i-min-hj-rna.mp3
  slug: psykedelisk-pop/1-janne-i-min-hj-rna
  albumSlug: psykedelisk-pop
  trackSlug: 1-janne-i-min-hj-rna
  coverPath: >-
    /mnt/usb32gb/organized-sakrecoer.media/assets/albums/psykedelisk-pop/1-janne-i-min-hj-rna.jpeg
  cover: /assets/albums/psykedelisk-pop/1-janne-i-min-hj-rna.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 128000
    codecProfile: CBR
    numberOfSamples: 9080064
    duration: 205.89714285714285
  native:
    ID3v2.3:
      - id: TIT2
        value: Janne I Min Hjärna
      - id: TPE1
        value: reSet Sakrecoer
      - id: TPE2
        value: reSet Sakrecoer
      - id: TCOM
        value: reSet Sakrecoer
      - id: TALB
        value: Psykedelisk Pop
      - id: TRCK
        value: 1/10
      - id: TCON
        value: Pop
      - id: COMM
        value:
          language: XXX
          description: Comment
          text: 'https://sakrecoer.com'
      - id: TYER
        value: '2010'
  quality:
    warnings: []
  common:
    track:
      'no': 1
      of: 10
    disk:
      'no': null
      of: null
    title: Janne I Min Hjärna
    artists:
      - reSet Sakrecoer
    artist: reSet Sakrecoer
    albumartist: reSet Sakrecoer
    composer:
      - reSet Sakrecoer
    album: Psykedelisk Pop
    genre:
      - Pop
    comment:
      - 'https://sakrecoer.com'
    year: 2010
  transformed:
    ID3v2.3:
      TIT2: Janne I Min Hjärna
      TPE1: reSet Sakrecoer
      TPE2: reSet Sakrecoer
      TCOM: reSet Sakrecoer
      TALB: Psykedelisk Pop
      TRCK: 1/10
      TCON: Pop
      Comment: 'https://sakrecoer.com'
      TYER: '2010'
  all:
    TIT2: Janne I Min Hjärna
    TPE1: reSet Sakrecoer
    TPE2: reSet Sakrecoer
    TCOM: reSet Sakrecoer
    TALB: Psykedelisk Pop
    TRCK: 1/10
    TCON: Pop
    Comment: 'https://sakrecoer.com'
    TYER: '2010'
previousTrack:
  path: >-
    /mnt/usb32gb/organized-sakrecoer.media/assets/albums/psykedelisk-pop/9-drugs.mp3
  audio: /assets/albums/psykedelisk-pop/9-drugs.mp3
  slug: psykedelisk-pop/9-drugs
  albumSlug: psykedelisk-pop
  trackSlug: 9-drugs
  coverPath: >-
    /mnt/usb32gb/organized-sakrecoer.media/assets/albums/psykedelisk-pop/9-drugs.jpeg
  cover: /assets/albums/psykedelisk-pop/9-drugs.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 128000
    codecProfile: CBR
    numberOfSamples: 10166400
    duration: 230.53061224489795
  native:
    ID3v2.3:
      - id: TIT2
        value: Drugs
      - id: TPE1
        value: reSet Sakrecoer
      - id: TPE2
        value: reSet Sakrecoer
      - id: TCOM
        value: reSet Sakrecoer
      - id: TALB
        value: Psykedelisk Pop
      - id: TRCK
        value: 9/10
      - id: TBPM
        value: '127'
      - id: TCON
        value: Alternative Pop
      - id: COMM
        value:
          language: XXX
          description: Comment
          text: 'https://sakrecoer.com'
      - id: TYER
        value: '2010'
  quality:
    warnings: []
  common:
    track:
      'no': 9
      of: 10
    disk:
      'no': null
      of: null
    title: Drugs
    artists:
      - reSet Sakrecoer
    artist: reSet Sakrecoer
    albumartist: reSet Sakrecoer
    composer:
      - reSet Sakrecoer
    album: Psykedelisk Pop
    bpm: '127'
    genre:
      - Alternative Pop
    comment:
      - 'https://sakrecoer.com'
    year: 2010
  transformed:
    ID3v2.3:
      TIT2: Drugs
      TPE1: reSet Sakrecoer
      TPE2: reSet Sakrecoer
      TCOM: reSet Sakrecoer
      TALB: Psykedelisk Pop
      TRCK: 9/10
      TBPM: '127'
      TCON: Alternative Pop
      Comment: 'https://sakrecoer.com'
      TYER: '2010'
  all:
    TIT2: Drugs
    TPE1: reSet Sakrecoer
    TPE2: reSet Sakrecoer
    TCOM: reSet Sakrecoer
    TALB: Psykedelisk Pop
    TRCK: 9/10
    TBPM: '127'
    TCON: Alternative Pop
    Comment: 'https://sakrecoer.com'
    TYER: '2010'
---
