---
layout: track
path: >-
  /mnt/usb32gb/organized-sakrecoer.media/assets/albums/psykedelisk-pop/5-luftboejning.mp3
audio: /assets/albums/psykedelisk-pop/5-luftboejning.mp3
slug: psykedelisk-pop/5-luftboejning
albumSlug: psykedelisk-pop
trackSlug: 5-luftboejning
coverPath: >-
  /mnt/usb32gb/organized-sakrecoer.media/assets/albums/psykedelisk-pop/5-luftboejning.jpeg
cover: /assets/albums/psykedelisk-pop/5-luftboejning.jpeg
format:
  tagTypes:
    - ID3v2.3
  trackInfo: []
  lossless: false
  container: MPEG
  codec: MPEG 1 Layer 3
  sampleRate: 44100
  numberOfChannels: 2
  bitrate: 128000
  codecProfile: CBR
  numberOfSamples: 19203840
  duration: 435.46122448979594
native:
  ID3v2.3:
    - id: TIT2
      value: Luftboejning
    - id: TPE1
      value: reSet Sakrecoer
    - id: TPE2
      value: reSet Sakrecoer
    - id: TCOM
      value: reSet Sakrecoer
    - id: TALB
      value: Psykedelisk Pop
    - id: TRCK
      value: 5/10
    - id: TCON
      value: Alternative Pop
    - id: COMM
      value:
        language: XXX
        description: Comment
        text: 'https://sakrecoer.com'
    - id: TYER
      value: '2010'
quality:
  warnings: []
common:
  track:
    'no': 5
    of: 10
  disk:
    'no': null
    of: null
  title: Luftboejning
  artists:
    - reSet Sakrecoer
  artist: reSet Sakrecoer
  albumartist: reSet Sakrecoer
  composer:
    - reSet Sakrecoer
  album: Psykedelisk Pop
  genre:
    - Alternative Pop
  comment:
    - 'https://sakrecoer.com'
  year: 2010
transformed:
  ID3v2.3:
    TIT2: Luftboejning
    TPE1: reSet Sakrecoer
    TPE2: reSet Sakrecoer
    TCOM: reSet Sakrecoer
    TALB: Psykedelisk Pop
    TRCK: 5/10
    TCON: Alternative Pop
    Comment: 'https://sakrecoer.com'
    TYER: '2010'
all:
  TIT2: Luftboejning
  TPE1: reSet Sakrecoer
  TPE2: reSet Sakrecoer
  TCOM: reSet Sakrecoer
  TALB: Psykedelisk Pop
  TRCK: 5/10
  TCON: Alternative Pop
  Comment: 'https://sakrecoer.com'
  TYER: '2010'
nextTrack:
  path: >-
    /mnt/usb32gb/organized-sakrecoer.media/assets/albums/psykedelisk-pop/6-itelligent-musik.mp3
  audio: /assets/albums/psykedelisk-pop/6-itelligent-musik.mp3
  slug: psykedelisk-pop/6-itelligent-musik
  albumSlug: psykedelisk-pop
  trackSlug: 6-itelligent-musik
  coverPath: >-
    /mnt/usb32gb/organized-sakrecoer.media/assets/albums/psykedelisk-pop/6-itelligent-musik.jpeg
  cover: /assets/albums/psykedelisk-pop/6-itelligent-musik.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 128000
    codecProfile: CBR
    numberOfSamples: 10062720
    duration: 228.1795918367347
  native:
    ID3v2.3:
      - id: TIT2
        value: Itelligent Musik
      - id: TPE1
        value: reSet Sakrecoer
      - id: TPE2
        value: reSet Sakrecoer
      - id: TCOM
        value: reSet Sakrecoer
      - id: TALB
        value: Psykedelisk Pop
      - id: TRCK
        value: 6/10
      - id: TBPM
        value: '127'
      - id: TCON
        value: Alternative Pop
      - id: COMM
        value:
          language: XXX
          description: Comment
          text: 'https://sakrecoer.com'
      - id: TYER
        value: '2010'
  quality:
    warnings: []
  common:
    track:
      'no': 6
      of: 10
    disk:
      'no': null
      of: null
    title: Itelligent Musik
    artists:
      - reSet Sakrecoer
    artist: reSet Sakrecoer
    albumartist: reSet Sakrecoer
    composer:
      - reSet Sakrecoer
    album: Psykedelisk Pop
    bpm: '127'
    genre:
      - Alternative Pop
    comment:
      - 'https://sakrecoer.com'
    year: 2010
  transformed:
    ID3v2.3:
      TIT2: Itelligent Musik
      TPE1: reSet Sakrecoer
      TPE2: reSet Sakrecoer
      TCOM: reSet Sakrecoer
      TALB: Psykedelisk Pop
      TRCK: 6/10
      TBPM: '127'
      TCON: Alternative Pop
      Comment: 'https://sakrecoer.com'
      TYER: '2010'
  all:
    TIT2: Itelligent Musik
    TPE1: reSet Sakrecoer
    TPE2: reSet Sakrecoer
    TCOM: reSet Sakrecoer
    TALB: Psykedelisk Pop
    TRCK: 6/10
    TBPM: '127'
    TCON: Alternative Pop
    Comment: 'https://sakrecoer.com'
    TYER: '2010'
previousTrack:
  path: >-
    /mnt/usb32gb/organized-sakrecoer.media/assets/albums/psykedelisk-pop/4-it-s-all-over.mp3
  audio: /assets/albums/psykedelisk-pop/4-it-s-all-over.mp3
  slug: psykedelisk-pop/4-it-s-all-over
  albumSlug: psykedelisk-pop
  trackSlug: 4-it-s-all-over
  coverPath: >-
    /mnt/usb32gb/organized-sakrecoer.media/assets/albums/psykedelisk-pop/4-it-s-all-over.jpeg
  cover: /assets/albums/psykedelisk-pop/4-it-s-all-over.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 128000
    codecProfile: CBR
    numberOfSamples: 11700864
    duration: 265.3257142857143
  native:
    ID3v2.3:
      - id: TIT2
        value: It's All Over
      - id: TPE1
        value: reSet Sakrecoer
      - id: TPE2
        value: reSet Sakrecoer
      - id: TCOM
        value: reSet Sakrecoer
      - id: TALB
        value: Psykedelisk Pop
      - id: TRCK
        value: 4/10
      - id: TCON
        value: Emo
      - id: COMM
        value:
          language: XXX
          description: Comment
          text: 'https://sakrecoer.com'
      - id: TYER
        value: '2010'
  quality:
    warnings: []
  common:
    track:
      'no': 4
      of: 10
    disk:
      'no': null
      of: null
    title: It's All Over
    artists:
      - reSet Sakrecoer
    artist: reSet Sakrecoer
    albumartist: reSet Sakrecoer
    composer:
      - reSet Sakrecoer
    album: Psykedelisk Pop
    genre:
      - Emo
    comment:
      - 'https://sakrecoer.com'
    year: 2010
  transformed:
    ID3v2.3:
      TIT2: It's All Over
      TPE1: reSet Sakrecoer
      TPE2: reSet Sakrecoer
      TCOM: reSet Sakrecoer
      TALB: Psykedelisk Pop
      TRCK: 4/10
      TCON: Emo
      Comment: 'https://sakrecoer.com'
      TYER: '2010'
  all:
    TIT2: It's All Over
    TPE1: reSet Sakrecoer
    TPE2: reSet Sakrecoer
    TCOM: reSet Sakrecoer
    TALB: Psykedelisk Pop
    TRCK: 4/10
    TCON: Emo
    Comment: 'https://sakrecoer.com'
    TYER: '2010'
---
