---
layout: track
path: >-
  /mnt/usb32gb/organized-sakrecoer.media/assets/albums/coolometrics/8-dr-mmar.mp3
audio: /assets/albums/coolometrics/8-dr-mmar.mp3
slug: coolometrics/8-dr-mmar
albumSlug: coolometrics
trackSlug: 8-dr-mmar
coverPath: >-
  /mnt/usb32gb/organized-sakrecoer.media/assets/albums/coolometrics/8-dr-mmar.jpeg
cover: /assets/albums/coolometrics/8-dr-mmar.jpeg
format:
  tagTypes:
    - ID3v2.3
  trackInfo: []
  lossless: false
  container: MPEG
  codec: MPEG 1 Layer 3
  sampleRate: 44100
  numberOfChannels: 2
  bitrate: 128000
  codecProfile: CBR
  numberOfSamples: 4983552
  duration: 113.00571428571429
native:
  ID3v2.3:
    - id: TIT2
      value: Drömmar
    - id: TPE1
      value: reSet Sakrecoer
    - id: TALB
      value: Coolometrics
    - id: TRCK
      value: '8'
    - id: TCON
      value: Lo-Fi
    - id: TYER
      value: '2005'
quality:
  warnings: []
common:
  track:
    'no': 8
    of: null
  disk:
    'no': null
    of: null
  title: Drömmar
  artists:
    - reSet Sakrecoer
  artist: reSet Sakrecoer
  album: Coolometrics
  genre:
    - Lo-Fi
  year: 2005
transformed:
  ID3v2.3:
    TIT2: Drömmar
    TPE1: reSet Sakrecoer
    TALB: Coolometrics
    TRCK: '8'
    TCON: Lo-Fi
    TYER: '2005'
all:
  TIT2: Drömmar
  TPE1: reSet Sakrecoer
  TALB: Coolometrics
  TRCK: '8'
  TCON: Lo-Fi
  TYER: '2005'
nextTrack:
  path: >-
    /mnt/usb32gb/organized-sakrecoer.media/assets/albums/coolometrics/9-ordfilter.mp3
  audio: /assets/albums/coolometrics/9-ordfilter.mp3
  slug: coolometrics/9-ordfilter
  albumSlug: coolometrics
  trackSlug: 9-ordfilter
  coverPath: >-
    /mnt/usb32gb/organized-sakrecoer.media/assets/albums/coolometrics/9-ordfilter.jpeg
  cover: /assets/albums/coolometrics/9-ordfilter.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 128000
    codecProfile: CBR
    numberOfSamples: 5109120
    duration: 115.85306122448979
  native:
    ID3v2.3:
      - id: TIT2
        value: Ordfilter
      - id: TPE1
        value: reSet Sakrecoer
      - id: TCON
        value: Swedish låfink
      - id: TALB
        value: Coolometrics
      - id: TRCK
        value: '9'
      - id: TYER
        value: '2005'
  quality:
    warnings: []
  common:
    track:
      'no': 9
      of: null
    disk:
      'no': null
      of: null
    title: Ordfilter
    artists:
      - reSet Sakrecoer
    artist: reSet Sakrecoer
    genre:
      - Swedish låfink
    album: Coolometrics
    year: 2005
  transformed:
    ID3v2.3:
      TIT2: Ordfilter
      TPE1: reSet Sakrecoer
      TCON: Swedish låfink
      TALB: Coolometrics
      TRCK: '9'
      TYER: '2005'
  all:
    TIT2: Ordfilter
    TPE1: reSet Sakrecoer
    TCON: Swedish låfink
    TALB: Coolometrics
    TRCK: '9'
    TYER: '2005'
previousTrack:
  path: >-
    /mnt/usb32gb/organized-sakrecoer.media/assets/albums/coolometrics/7-i-hate-kidz-dieanna-remixed-.mp3
  audio: /assets/albums/coolometrics/7-i-hate-kidz-dieanna-remixed-.mp3
  slug: coolometrics/7-i-hate-kidz-dieanna-remixed-
  albumSlug: coolometrics
  trackSlug: 7-i-hate-kidz-dieanna-remixed-
  coverPath: >-
    /mnt/usb32gb/organized-sakrecoer.media/assets/albums/coolometrics/7-i-hate-kidz-dieanna-remixed-.jpeg
  cover: /assets/albums/coolometrics/7-i-hate-kidz-dieanna-remixed-.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 128000
    tool: LAME 3.100U
    codecProfile: CBR
    numberOfSamples: 7803648
    duration: 176.9534693877551
  native:
    ID3v2.3:
      - id: TIT2
        value: I hate Kidz (DieAnna Remixed)
      - id: TPE1
        value: reSet Sakrecoer
      - id: TALB
        value: Coolometrics
      - id: TRCK
        value: '7'
      - id: TCON
        value: Punk
      - id: TYER
        value: '2005'
  quality:
    warnings: []
  common:
    track:
      'no': 7
      of: null
    disk:
      'no': null
      of: null
    title: I hate Kidz (DieAnna Remixed)
    artists:
      - reSet Sakrecoer
    artist: reSet Sakrecoer
    album: Coolometrics
    genre:
      - Punk
    year: 2005
  transformed:
    ID3v2.3:
      TIT2: I hate Kidz (DieAnna Remixed)
      TPE1: reSet Sakrecoer
      TALB: Coolometrics
      TRCK: '7'
      TCON: Punk
      TYER: '2005'
  all:
    TIT2: I hate Kidz (DieAnna Remixed)
    TPE1: reSet Sakrecoer
    TALB: Coolometrics
    TRCK: '7'
    TCON: Punk
    TYER: '2005'
---
