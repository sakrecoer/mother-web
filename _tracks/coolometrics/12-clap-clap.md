---
layout: track
path: >-
  /mnt/usb32gb/organized-sakrecoer.media/assets/albums/coolometrics/12-clap-clap.mp3
audio: /assets/albums/coolometrics/12-clap-clap.mp3
slug: coolometrics/12-clap-clap
albumSlug: coolometrics
trackSlug: 12-clap-clap
coverPath: >-
  /mnt/usb32gb/organized-sakrecoer.media/assets/albums/coolometrics/12-clap-clap.jpeg
cover: /assets/albums/coolometrics/12-clap-clap.jpeg
format:
  tagTypes:
    - ID3v2.3
  trackInfo: []
  lossless: false
  container: MPEG
  codec: MPEG 1 Layer 3
  sampleRate: 44100
  numberOfChannels: 2
  bitrate: 128000
  codecProfile: CBR
  numberOfSamples: 5874048
  duration: 133.19836734693877
native:
  ID3v2.3:
    - id: TIT2
      value: Clap Clap
    - id: TPE1
      value: reSet Sakrecoer
    - id: TALB
      value: Coolometrics
    - id: TRCK
      value: '12'
    - id: TCON
      value: Lo-Fi
    - id: TYER
      value: '2005'
quality:
  warnings: []
common:
  track:
    'no': 12
    of: null
  disk:
    'no': null
    of: null
  title: Clap Clap
  artists:
    - reSet Sakrecoer
  artist: reSet Sakrecoer
  album: Coolometrics
  genre:
    - Lo-Fi
  year: 2005
transformed:
  ID3v2.3:
    TIT2: Clap Clap
    TPE1: reSet Sakrecoer
    TALB: Coolometrics
    TRCK: '12'
    TCON: Lo-Fi
    TYER: '2005'
all:
  TIT2: Clap Clap
  TPE1: reSet Sakrecoer
  TALB: Coolometrics
  TRCK: '12'
  TCON: Lo-Fi
  TYER: '2005'
nextTrack:
  path: >-
    /mnt/usb32gb/organized-sakrecoer.media/assets/albums/coolometrics/13-mes-sentiments-pour-toi.mp3
  audio: /assets/albums/coolometrics/13-mes-sentiments-pour-toi.mp3
  slug: coolometrics/13-mes-sentiments-pour-toi
  albumSlug: coolometrics
  trackSlug: 13-mes-sentiments-pour-toi
  coverPath: >-
    /mnt/usb32gb/organized-sakrecoer.media/assets/albums/coolometrics/13-mes-sentiments-pour-toi.jpeg
  cover: /assets/albums/coolometrics/13-mes-sentiments-pour-toi.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 128000
    codecProfile: CBR
    numberOfSamples: 8839296
    duration: 200.43755102040816
  native:
    ID3v2.3:
      - id: TPE1
        value: reSet Sakrecoer
      - id: TALB
        value: Coolometrics
      - id: TCON
        value: Pop
      - id: COMM
        value:
          language: XXX
          description: Comment
          text: creative common licensed - gnashed records
      - id: TIT2
        value: Mes Sentiments Pour Toi
      - id: TRCK
        value: '13'
      - id: TYER
        value: '2005'
  quality:
    warnings: []
  common:
    track:
      'no': 13
      of: null
    disk:
      'no': null
      of: null
    artists:
      - reSet Sakrecoer
    artist: reSet Sakrecoer
    album: Coolometrics
    genre:
      - Pop
    comment:
      - creative common licensed - gnashed records
    title: Mes Sentiments Pour Toi
    year: 2005
  transformed:
    ID3v2.3:
      TPE1: reSet Sakrecoer
      TALB: Coolometrics
      TCON: Pop
      Comment: creative common licensed - gnashed records
      TIT2: Mes Sentiments Pour Toi
      TRCK: '13'
      TYER: '2005'
  all:
    TPE1: reSet Sakrecoer
    TALB: Coolometrics
    TCON: Pop
    Comment: creative common licensed - gnashed records
    TIT2: Mes Sentiments Pour Toi
    TRCK: '13'
    TYER: '2005'
previousTrack:
  path: >-
    /mnt/usb32gb/organized-sakrecoer.media/assets/albums/coolometrics/11-morning-scooper.mp3
  audio: /assets/albums/coolometrics/11-morning-scooper.mp3
  slug: coolometrics/11-morning-scooper
  albumSlug: coolometrics
  trackSlug: 11-morning-scooper
  coverPath: >-
    /mnt/usb32gb/organized-sakrecoer.media/assets/albums/coolometrics/11-morning-scooper.jpeg
  cover: /assets/albums/coolometrics/11-morning-scooper.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 128000
    codecProfile: CBR
    numberOfSamples: 9517824
    duration: 215.82367346938776
  native:
    ID3v2.3:
      - id: TPE1
        value: reSet Sakrecoer
      - id: TIT2
        value: Morning Scooper
      - id: TALB
        value: Coolometrics
      - id: TRCK
        value: '11'
      - id: TCON
        value: Pop
      - id: TYER
        value: '2005'
  quality:
    warnings: []
  common:
    track:
      'no': 11
      of: null
    disk:
      'no': null
      of: null
    artists:
      - reSet Sakrecoer
    artist: reSet Sakrecoer
    title: Morning Scooper
    album: Coolometrics
    genre:
      - Pop
    year: 2005
  transformed:
    ID3v2.3:
      TPE1: reSet Sakrecoer
      TIT2: Morning Scooper
      TALB: Coolometrics
      TRCK: '11'
      TCON: Pop
      TYER: '2005'
  all:
    TPE1: reSet Sakrecoer
    TIT2: Morning Scooper
    TALB: Coolometrics
    TRCK: '11'
    TCON: Pop
    TYER: '2005'
---
