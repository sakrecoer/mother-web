---
layout: track
path: >-
  /mnt/usb32gb/organized-sakrecoer.media/assets/albums/coolometrics/15-exclamation-feat-arete.mp3
audio: /assets/albums/coolometrics/15-exclamation-feat-arete.mp3
slug: coolometrics/15-exclamation-feat-arete
albumSlug: coolometrics
trackSlug: 15-exclamation-feat-arete
coverPath: >-
  /mnt/usb32gb/organized-sakrecoer.media/assets/albums/coolometrics/15-exclamation-feat-arete.jpeg
cover: /assets/albums/coolometrics/15-exclamation-feat-arete.jpeg
format:
  tagTypes:
    - ID3v2.3
  trackInfo: []
  lossless: false
  container: MPEG
  codec: MPEG 1 Layer 3
  sampleRate: 44100
  numberOfChannels: 2
  bitrate: 128000
  codecProfile: CBR
  numberOfSamples: 7413120
  duration: 168.09795918367348
native:
  ID3v2.3:
    - id: TIT2
      value: Exclamation Feat. Arete
    - id: TPE1
      value: reSet Sakrecoer
    - id: TALB
      value: Coolometrics
    - id: TRCK
      value: '15'
    - id: TCON
      value: Hippie Shit
    - id: TYER
      value: '2005'
quality:
  warnings: []
common:
  track:
    'no': 15
    of: null
  disk:
    'no': null
    of: null
  title: Exclamation Feat. Arete
  artists:
    - reSet Sakrecoer
  artist: reSet Sakrecoer
  album: Coolometrics
  genre:
    - Hippie Shit
  year: 2005
transformed:
  ID3v2.3:
    TIT2: Exclamation Feat. Arete
    TPE1: reSet Sakrecoer
    TALB: Coolometrics
    TRCK: '15'
    TCON: Hippie Shit
    TYER: '2005'
all:
  TIT2: Exclamation Feat. Arete
  TPE1: reSet Sakrecoer
  TALB: Coolometrics
  TRCK: '15'
  TCON: Hippie Shit
  TYER: '2005'
nextTrack:
  path: >-
    /mnt/usb32gb/organized-sakrecoer.media/assets/albums/coolometrics/16-tap-tap.mp3
  audio: /assets/albums/coolometrics/16-tap-tap.mp3
  slug: coolometrics/16-tap-tap
  albumSlug: coolometrics
  trackSlug: 16-tap-tap
  coverPath: >-
    /mnt/usb32gb/organized-sakrecoer.media/assets/albums/coolometrics/16-tap-tap.jpeg
  cover: /assets/albums/coolometrics/16-tap-tap.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 128000
    codecProfile: CBR
    numberOfSamples: 4846464
    duration: 109.89714285714285
  native:
    ID3v2.3:
      - id: TIT2
        value: Tap Tap
      - id: TPE1
        value: reSet Sakrecoer
      - id: TALB
        value: Coolometrics
      - id: TRCK
        value: '16'
      - id: TCON
        value: Lo-Fi
      - id: TYER
        value: '2005'
  quality:
    warnings: []
  common:
    track:
      'no': 16
      of: null
    disk:
      'no': null
      of: null
    title: Tap Tap
    artists:
      - reSet Sakrecoer
    artist: reSet Sakrecoer
    album: Coolometrics
    genre:
      - Lo-Fi
    year: 2005
  transformed:
    ID3v2.3:
      TIT2: Tap Tap
      TPE1: reSet Sakrecoer
      TALB: Coolometrics
      TRCK: '16'
      TCON: Lo-Fi
      TYER: '2005'
  all:
    TIT2: Tap Tap
    TPE1: reSet Sakrecoer
    TALB: Coolometrics
    TRCK: '16'
    TCON: Lo-Fi
    TYER: '2005'
previousTrack:
  path: >-
    /mnt/usb32gb/organized-sakrecoer.media/assets/albums/coolometrics/14-sn-lla-se-mig-feat-thickdick-arete-and-enapa.mp3
  audio: >-
    /assets/albums/coolometrics/14-sn-lla-se-mig-feat-thickdick-arete-and-enapa.mp3
  slug: coolometrics/14-sn-lla-se-mig-feat-thickdick-arete-and-enapa
  albumSlug: coolometrics
  trackSlug: 14-sn-lla-se-mig-feat-thickdick-arete-and-enapa
  coverPath: >-
    /mnt/usb32gb/organized-sakrecoer.media/assets/albums/coolometrics/14-sn-lla-se-mig-feat-thickdick-arete-and-enapa.jpeg
  cover: >-
    /assets/albums/coolometrics/14-sn-lla-se-mig-feat-thickdick-arete-and-enapa.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 128000
    codecProfile: CBR
    numberOfSamples: 7226496
    duration: 163.86612244897958
  native:
    ID3v2.3:
      - id: TPE1
        value: reSet Sakrecoer
      - id: TBPM
        value: '126'
      - id: COMM
        value:
          language: XXX
          description: Comment
          text: 808 drum by thickdick various samples from Arete and EnApa
      - id: TCON
        value: Alternative Pop
      - id: TIT2
        value: 'Snälla Se Mig Feat. Thickdick, Arete and EnApa'
      - id: TALB
        value: Coolometrics
      - id: TRCK
        value: '14'
      - id: TYER
        value: '2005'
  quality:
    warnings: []
  common:
    track:
      'no': 14
      of: null
    disk:
      'no': null
      of: null
    artists:
      - reSet Sakrecoer
    artist: reSet Sakrecoer
    bpm: '126'
    comment:
      - 808 drum by thickdick various samples from Arete and EnApa
    genre:
      - Alternative Pop
    title: 'Snälla Se Mig Feat. Thickdick, Arete and EnApa'
    album: Coolometrics
    year: 2005
  transformed:
    ID3v2.3:
      TPE1: reSet Sakrecoer
      TBPM: '126'
      Comment: 808 drum by thickdick various samples from Arete and EnApa
      TCON: Alternative Pop
      TIT2: 'Snälla Se Mig Feat. Thickdick, Arete and EnApa'
      TALB: Coolometrics
      TRCK: '14'
      TYER: '2005'
  all:
    TPE1: reSet Sakrecoer
    TBPM: '126'
    Comment: 808 drum by thickdick various samples from Arete and EnApa
    TCON: Alternative Pop
    TIT2: 'Snälla Se Mig Feat. Thickdick, Arete and EnApa'
    TALB: Coolometrics
    TRCK: '14'
    TYER: '2005'
---
