---
layout: track
path: >-
  /mnt/usb32gb/organized-sakrecoer.media/assets/albums/coolometrics/16-tap-tap.mp3
audio: /assets/albums/coolometrics/16-tap-tap.mp3
slug: coolometrics/16-tap-tap
albumSlug: coolometrics
trackSlug: 16-tap-tap
coverPath: >-
  /mnt/usb32gb/organized-sakrecoer.media/assets/albums/coolometrics/16-tap-tap.jpeg
cover: /assets/albums/coolometrics/16-tap-tap.jpeg
format:
  tagTypes:
    - ID3v2.3
  trackInfo: []
  lossless: false
  container: MPEG
  codec: MPEG 1 Layer 3
  sampleRate: 44100
  numberOfChannels: 2
  bitrate: 128000
  codecProfile: CBR
  numberOfSamples: 4846464
  duration: 109.89714285714285
native:
  ID3v2.3:
    - id: TIT2
      value: Tap Tap
    - id: TPE1
      value: reSet Sakrecoer
    - id: TALB
      value: Coolometrics
    - id: TRCK
      value: '16'
    - id: TCON
      value: Lo-Fi
    - id: TYER
      value: '2005'
quality:
  warnings: []
common:
  track:
    'no': 16
    of: null
  disk:
    'no': null
    of: null
  title: Tap Tap
  artists:
    - reSet Sakrecoer
  artist: reSet Sakrecoer
  album: Coolometrics
  genre:
    - Lo-Fi
  year: 2005
transformed:
  ID3v2.3:
    TIT2: Tap Tap
    TPE1: reSet Sakrecoer
    TALB: Coolometrics
    TRCK: '16'
    TCON: Lo-Fi
    TYER: '2005'
all:
  TIT2: Tap Tap
  TPE1: reSet Sakrecoer
  TALB: Coolometrics
  TRCK: '16'
  TCON: Lo-Fi
  TYER: '2005'
nextTrack:
  path: >-
    /mnt/usb32gb/organized-sakrecoer.media/assets/albums/coolometrics/17-battlezoo.mp3
  audio: /assets/albums/coolometrics/17-battlezoo.mp3
  slug: coolometrics/17-battlezoo
  albumSlug: coolometrics
  trackSlug: 17-battlezoo
  coverPath: >-
    /mnt/usb32gb/organized-sakrecoer.media/assets/albums/coolometrics/17-battlezoo.jpeg
  cover: /assets/albums/coolometrics/17-battlezoo.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 128000
    codecProfile: CBR
    numberOfSamples: 10275840
    duration: 233.0122448979592
  native:
    ID3v2.3:
      - id: TIT2
        value: Battlezoo
      - id: TPE1
        value: reSet Sakrecoer
      - id: TALB
        value: Coolometrics
      - id: TCON
        value: Emo
      - id: TRCK
        value: '17'
      - id: TYER
        value: '2005'
  quality:
    warnings: []
  common:
    track:
      'no': 17
      of: null
    disk:
      'no': null
      of: null
    title: Battlezoo
    artists:
      - reSet Sakrecoer
    artist: reSet Sakrecoer
    album: Coolometrics
    genre:
      - Emo
    year: 2005
  transformed:
    ID3v2.3:
      TIT2: Battlezoo
      TPE1: reSet Sakrecoer
      TALB: Coolometrics
      TCON: Emo
      TRCK: '17'
      TYER: '2005'
  all:
    TIT2: Battlezoo
    TPE1: reSet Sakrecoer
    TALB: Coolometrics
    TCON: Emo
    TRCK: '17'
    TYER: '2005'
previousTrack:
  path: >-
    /mnt/usb32gb/organized-sakrecoer.media/assets/albums/coolometrics/15-exclamation-feat-arete.mp3
  audio: /assets/albums/coolometrics/15-exclamation-feat-arete.mp3
  slug: coolometrics/15-exclamation-feat-arete
  albumSlug: coolometrics
  trackSlug: 15-exclamation-feat-arete
  coverPath: >-
    /mnt/usb32gb/organized-sakrecoer.media/assets/albums/coolometrics/15-exclamation-feat-arete.jpeg
  cover: /assets/albums/coolometrics/15-exclamation-feat-arete.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 128000
    codecProfile: CBR
    numberOfSamples: 7413120
    duration: 168.09795918367348
  native:
    ID3v2.3:
      - id: TIT2
        value: Exclamation Feat. Arete
      - id: TPE1
        value: reSet Sakrecoer
      - id: TALB
        value: Coolometrics
      - id: TRCK
        value: '15'
      - id: TCON
        value: Hippie Shit
      - id: TYER
        value: '2005'
  quality:
    warnings: []
  common:
    track:
      'no': 15
      of: null
    disk:
      'no': null
      of: null
    title: Exclamation Feat. Arete
    artists:
      - reSet Sakrecoer
    artist: reSet Sakrecoer
    album: Coolometrics
    genre:
      - Hippie Shit
    year: 2005
  transformed:
    ID3v2.3:
      TIT2: Exclamation Feat. Arete
      TPE1: reSet Sakrecoer
      TALB: Coolometrics
      TRCK: '15'
      TCON: Hippie Shit
      TYER: '2005'
  all:
    TIT2: Exclamation Feat. Arete
    TPE1: reSet Sakrecoer
    TALB: Coolometrics
    TRCK: '15'
    TCON: Hippie Shit
    TYER: '2005'
---
