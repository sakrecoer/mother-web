---
layout: track
path: >-
  /mnt/usb32gb/organized-sakrecoer.media/assets/albums/coolometrics/10-sms-poet.mp3
audio: /assets/albums/coolometrics/10-sms-poet.mp3
slug: coolometrics/10-sms-poet
albumSlug: coolometrics
trackSlug: 10-sms-poet
coverPath: >-
  /mnt/usb32gb/organized-sakrecoer.media/assets/albums/coolometrics/10-sms-poet.jpeg
cover: /assets/albums/coolometrics/10-sms-poet.jpeg
format:
  tagTypes:
    - ID3v2.3
  trackInfo: []
  lossless: false
  container: MPEG
  codec: MPEG 1 Layer 3
  sampleRate: 44100
  numberOfChannels: 2
  bitrate: 128000
  codecProfile: CBR
  numberOfSamples: 10012032
  duration: 227.03020408163266
native:
  ID3v2.3:
    - id: TIT2
      value: SMS Poet
    - id: TPE1
      value: reSet Sakrecoer
    - id: TALB
      value: Coolometrics
    - id: TRCK
      value: '10'
    - id: TCON
      value: Pop
    - id: TYER
      value: '2005'
quality:
  warnings: []
common:
  track:
    'no': 10
    of: null
  disk:
    'no': null
    of: null
  title: SMS Poet
  artists:
    - reSet Sakrecoer
  artist: reSet Sakrecoer
  album: Coolometrics
  genre:
    - Pop
  year: 2005
transformed:
  ID3v2.3:
    TIT2: SMS Poet
    TPE1: reSet Sakrecoer
    TALB: Coolometrics
    TRCK: '10'
    TCON: Pop
    TYER: '2005'
all:
  TIT2: SMS Poet
  TPE1: reSet Sakrecoer
  TALB: Coolometrics
  TRCK: '10'
  TCON: Pop
  TYER: '2005'
nextTrack:
  path: >-
    /mnt/usb32gb/organized-sakrecoer.media/assets/albums/coolometrics/11-morning-scooper.mp3
  audio: /assets/albums/coolometrics/11-morning-scooper.mp3
  slug: coolometrics/11-morning-scooper
  albumSlug: coolometrics
  trackSlug: 11-morning-scooper
  coverPath: >-
    /mnt/usb32gb/organized-sakrecoer.media/assets/albums/coolometrics/11-morning-scooper.jpeg
  cover: /assets/albums/coolometrics/11-morning-scooper.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 128000
    codecProfile: CBR
    numberOfSamples: 9517824
    duration: 215.82367346938776
  native:
    ID3v2.3:
      - id: TPE1
        value: reSet Sakrecoer
      - id: TIT2
        value: Morning Scooper
      - id: TALB
        value: Coolometrics
      - id: TRCK
        value: '11'
      - id: TCON
        value: Pop
      - id: TYER
        value: '2005'
  quality:
    warnings: []
  common:
    track:
      'no': 11
      of: null
    disk:
      'no': null
      of: null
    artists:
      - reSet Sakrecoer
    artist: reSet Sakrecoer
    title: Morning Scooper
    album: Coolometrics
    genre:
      - Pop
    year: 2005
  transformed:
    ID3v2.3:
      TPE1: reSet Sakrecoer
      TIT2: Morning Scooper
      TALB: Coolometrics
      TRCK: '11'
      TCON: Pop
      TYER: '2005'
  all:
    TPE1: reSet Sakrecoer
    TIT2: Morning Scooper
    TALB: Coolometrics
    TRCK: '11'
    TCON: Pop
    TYER: '2005'
previousTrack:
  path: >-
    /mnt/usb32gb/organized-sakrecoer.media/assets/albums/coolometrics/9-ordfilter.mp3
  audio: /assets/albums/coolometrics/9-ordfilter.mp3
  slug: coolometrics/9-ordfilter
  albumSlug: coolometrics
  trackSlug: 9-ordfilter
  coverPath: >-
    /mnt/usb32gb/organized-sakrecoer.media/assets/albums/coolometrics/9-ordfilter.jpeg
  cover: /assets/albums/coolometrics/9-ordfilter.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 128000
    codecProfile: CBR
    numberOfSamples: 5109120
    duration: 115.85306122448979
  native:
    ID3v2.3:
      - id: TIT2
        value: Ordfilter
      - id: TPE1
        value: reSet Sakrecoer
      - id: TCON
        value: Swedish låfink
      - id: TALB
        value: Coolometrics
      - id: TRCK
        value: '9'
      - id: TYER
        value: '2005'
  quality:
    warnings: []
  common:
    track:
      'no': 9
      of: null
    disk:
      'no': null
      of: null
    title: Ordfilter
    artists:
      - reSet Sakrecoer
    artist: reSet Sakrecoer
    genre:
      - Swedish låfink
    album: Coolometrics
    year: 2005
  transformed:
    ID3v2.3:
      TIT2: Ordfilter
      TPE1: reSet Sakrecoer
      TCON: Swedish låfink
      TALB: Coolometrics
      TRCK: '9'
      TYER: '2005'
  all:
    TIT2: Ordfilter
    TPE1: reSet Sakrecoer
    TCON: Swedish låfink
    TALB: Coolometrics
    TRCK: '9'
    TYER: '2005'
---
