---
layout: track
path: >-
  /mnt/usb32gb/organized-sakrecoer.media/assets/albums/coolometrics/5-donwanastay-feat-peter-hageus.mp3
audio: /assets/albums/coolometrics/5-donwanastay-feat-peter-hageus.mp3
slug: coolometrics/5-donwanastay-feat-peter-hageus
albumSlug: coolometrics
trackSlug: 5-donwanastay-feat-peter-hageus
coverPath: >-
  /mnt/usb32gb/organized-sakrecoer.media/assets/albums/coolometrics/5-donwanastay-feat-peter-hageus.jpeg
cover: /assets/albums/coolometrics/5-donwanastay-feat-peter-hageus.jpeg
format:
  tagTypes:
    - ID3v2.3
  trackInfo: []
  lossless: false
  container: MPEG
  codec: MPEG 1 Layer 3
  sampleRate: 44100
  numberOfChannels: 2
  bitrate: 128000
  codecProfile: CBR
  numberOfSamples: 7940736
  duration: 180.06204081632654
native:
  ID3v2.3:
    - id: TIT2
      value: Donwanastay Feat. Peter Hageus
    - id: TPE1
      value: reSet Sakrecoer
    - id: TCON
      value: Drakpop
    - id: TALB
      value: Coolometrics
    - id: TRCK
      value: '5'
    - id: TYER
      value: '2005'
quality:
  warnings: []
common:
  track:
    'no': 5
    of: null
  disk:
    'no': null
    of: null
  title: Donwanastay Feat. Peter Hageus
  artists:
    - reSet Sakrecoer
  artist: reSet Sakrecoer
  genre:
    - Drakpop
  album: Coolometrics
  year: 2005
transformed:
  ID3v2.3:
    TIT2: Donwanastay Feat. Peter Hageus
    TPE1: reSet Sakrecoer
    TCON: Drakpop
    TALB: Coolometrics
    TRCK: '5'
    TYER: '2005'
all:
  TIT2: Donwanastay Feat. Peter Hageus
  TPE1: reSet Sakrecoer
  TCON: Drakpop
  TALB: Coolometrics
  TRCK: '5'
  TYER: '2005'
nextTrack:
  path: >-
    /mnt/usb32gb/organized-sakrecoer.media/assets/albums/coolometrics/6-sthlm-rave.mp3
  audio: /assets/albums/coolometrics/6-sthlm-rave.mp3
  slug: coolometrics/6-sthlm-rave
  albumSlug: coolometrics
  trackSlug: 6-sthlm-rave
  coverPath: >-
    /mnt/usb32gb/organized-sakrecoer.media/assets/albums/coolometrics/6-sthlm-rave.jpeg
  cover: /assets/albums/coolometrics/6-sthlm-rave.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 128000
    codecProfile: CBR
    numberOfSamples: 7827840
    duration: 177.50204081632654
  native:
    ID3v2.3:
      - id: TIT2
        value: STHLM Rave
      - id: TPE1
        value: reSet Sakrecoer
      - id: TALB
        value: Coolometrics
      - id: TRCK
        value: '6'
      - id: TCON
        value: Lo-Fi
      - id: TYER
        value: '2005'
  quality:
    warnings: []
  common:
    track:
      'no': 6
      of: null
    disk:
      'no': null
      of: null
    title: STHLM Rave
    artists:
      - reSet Sakrecoer
    artist: reSet Sakrecoer
    album: Coolometrics
    genre:
      - Lo-Fi
    year: 2005
  transformed:
    ID3v2.3:
      TIT2: STHLM Rave
      TPE1: reSet Sakrecoer
      TALB: Coolometrics
      TRCK: '6'
      TCON: Lo-Fi
      TYER: '2005'
  all:
    TIT2: STHLM Rave
    TPE1: reSet Sakrecoer
    TALB: Coolometrics
    TRCK: '6'
    TCON: Lo-Fi
    TYER: '2005'
previousTrack:
  path: >-
    /mnt/usb32gb/organized-sakrecoer.media/assets/albums/coolometrics/4-abject.mp3
  audio: /assets/albums/coolometrics/4-abject.mp3
  slug: coolometrics/4-abject
  albumSlug: coolometrics
  trackSlug: 4-abject
  coverPath: >-
    /mnt/usb32gb/organized-sakrecoer.media/assets/albums/coolometrics/4-abject.jpeg
  cover: /assets/albums/coolometrics/4-abject.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 128000
    tool: LAME 3.100U
    codecProfile: CBR
    numberOfSamples: 13418496
    duration: 304.2742857142857
  native:
    ID3v2.3:
      - id: TPE1
        value: reSet Sakrecoer
      - id: TIT2
        value: Abject
      - id: TALB
        value: Coolometrics
      - id: TRCK
        value: '4'
      - id: TCON
        value: Disco
      - id: TYER
        value: '2005'
  quality:
    warnings: []
  common:
    track:
      'no': 4
      of: null
    disk:
      'no': null
      of: null
    artists:
      - reSet Sakrecoer
    artist: reSet Sakrecoer
    title: Abject
    album: Coolometrics
    genre:
      - Disco
    year: 2005
  transformed:
    ID3v2.3:
      TPE1: reSet Sakrecoer
      TIT2: Abject
      TALB: Coolometrics
      TRCK: '4'
      TCON: Disco
      TYER: '2005'
  all:
    TPE1: reSet Sakrecoer
    TIT2: Abject
    TALB: Coolometrics
    TRCK: '4'
    TCON: Disco
    TYER: '2005'
---
