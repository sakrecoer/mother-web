---
layout: track
path: >-
  /mnt/usb32gb/organized-sakrecoer.media/assets/albums/coolometrics/3-truning-thing-feat-muji.mp3
audio: /assets/albums/coolometrics/3-truning-thing-feat-muji.mp3
slug: coolometrics/3-truning-thing-feat-muji
albumSlug: coolometrics
trackSlug: 3-truning-thing-feat-muji
coverPath: >-
  /mnt/usb32gb/organized-sakrecoer.media/assets/albums/coolometrics/3-truning-thing-feat-muji.jpeg
cover: /assets/albums/coolometrics/3-truning-thing-feat-muji.jpeg
format:
  tagTypes:
    - ID3v2.3
  trackInfo: []
  lossless: false
  container: MPEG
  codec: MPEG 1 Layer 3
  sampleRate: 44100
  numberOfChannels: 2
  bitrate: 128000
  codecProfile: CBR
  numberOfSamples: 8704512
  duration: 197.38122448979593
native:
  ID3v2.3:
    - id: TIT2
      value: Truning Thing Feat. Muji
    - id: TPE1
      value: reSet Sakrecoer
    - id: TALB
      value: Coolometrics
    - id: TRCK
      value: '3'
    - id: TCON
      value: Lo-Fi
    - id: TYER
      value: '2005'
quality:
  warnings: []
common:
  track:
    'no': 3
    of: null
  disk:
    'no': null
    of: null
  title: Truning Thing Feat. Muji
  artists:
    - reSet Sakrecoer
  artist: reSet Sakrecoer
  album: Coolometrics
  genre:
    - Lo-Fi
  year: 2005
transformed:
  ID3v2.3:
    TIT2: Truning Thing Feat. Muji
    TPE1: reSet Sakrecoer
    TALB: Coolometrics
    TRCK: '3'
    TCON: Lo-Fi
    TYER: '2005'
all:
  TIT2: Truning Thing Feat. Muji
  TPE1: reSet Sakrecoer
  TALB: Coolometrics
  TRCK: '3'
  TCON: Lo-Fi
  TYER: '2005'
nextTrack:
  path: >-
    /mnt/usb32gb/organized-sakrecoer.media/assets/albums/coolometrics/4-abject.mp3
  audio: /assets/albums/coolometrics/4-abject.mp3
  slug: coolometrics/4-abject
  albumSlug: coolometrics
  trackSlug: 4-abject
  coverPath: >-
    /mnt/usb32gb/organized-sakrecoer.media/assets/albums/coolometrics/4-abject.jpeg
  cover: /assets/albums/coolometrics/4-abject.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 128000
    tool: LAME 3.100U
    codecProfile: CBR
    numberOfSamples: 13418496
    duration: 304.2742857142857
  native:
    ID3v2.3:
      - id: TPE1
        value: reSet Sakrecoer
      - id: TIT2
        value: Abject
      - id: TALB
        value: Coolometrics
      - id: TRCK
        value: '4'
      - id: TCON
        value: Disco
      - id: TYER
        value: '2005'
  quality:
    warnings: []
  common:
    track:
      'no': 4
      of: null
    disk:
      'no': null
      of: null
    artists:
      - reSet Sakrecoer
    artist: reSet Sakrecoer
    title: Abject
    album: Coolometrics
    genre:
      - Disco
    year: 2005
  transformed:
    ID3v2.3:
      TPE1: reSet Sakrecoer
      TIT2: Abject
      TALB: Coolometrics
      TRCK: '4'
      TCON: Disco
      TYER: '2005'
  all:
    TPE1: reSet Sakrecoer
    TIT2: Abject
    TALB: Coolometrics
    TRCK: '4'
    TCON: Disco
    TYER: '2005'
previousTrack:
  path: >-
    /mnt/usb32gb/organized-sakrecoer.media/assets/albums/coolometrics/2-computers-kill.mp3
  audio: /assets/albums/coolometrics/2-computers-kill.mp3
  slug: coolometrics/2-computers-kill
  albumSlug: coolometrics
  trackSlug: 2-computers-kill
  coverPath: >-
    /mnt/usb32gb/organized-sakrecoer.media/assets/albums/coolometrics/2-computers-kill.jpeg
  cover: /assets/albums/coolometrics/2-computers-kill.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 128000
    codecProfile: CBR
    numberOfSamples: 9440640
    duration: 214.0734693877551
  native:
    ID3v2.3:
      - id: TPE1
        value: reSet Sakrecoer
      - id: TALB
        value: Coolometrics
      - id: TCON
        value: Disco
      - id: COMM
        value:
          language: XXX
          description: Comment
          text: creative common licensed
      - id: TIT2
        value: Computers Kill
      - id: TRCK
        value: '2'
      - id: TYER
        value: '2005'
  quality:
    warnings: []
  common:
    track:
      'no': 2
      of: null
    disk:
      'no': null
      of: null
    artists:
      - reSet Sakrecoer
    artist: reSet Sakrecoer
    album: Coolometrics
    genre:
      - Disco
    comment:
      - creative common licensed
    title: Computers Kill
    year: 2005
  transformed:
    ID3v2.3:
      TPE1: reSet Sakrecoer
      TALB: Coolometrics
      TCON: Disco
      Comment: creative common licensed
      TIT2: Computers Kill
      TRCK: '2'
      TYER: '2005'
  all:
    TPE1: reSet Sakrecoer
    TALB: Coolometrics
    TCON: Disco
    Comment: creative common licensed
    TIT2: Computers Kill
    TRCK: '2'
    TYER: '2005'
---
