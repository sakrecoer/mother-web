---
layout: track
path: >-
  /mnt/usb32gb/organized-sakrecoer.media/assets/albums/coolometrics/19-la-douce-chanson-du-robot.mp3
audio: /assets/albums/coolometrics/19-la-douce-chanson-du-robot.mp3
slug: coolometrics/19-la-douce-chanson-du-robot
albumSlug: coolometrics
trackSlug: 19-la-douce-chanson-du-robot
coverPath: >-
  /mnt/usb32gb/organized-sakrecoer.media/assets/albums/coolometrics/19-la-douce-chanson-du-robot.jpeg
cover: /assets/albums/coolometrics/19-la-douce-chanson-du-robot.jpeg
format:
  tagTypes:
    - ID3v2.3
  trackInfo: []
  lossless: false
  container: MPEG
  codec: MPEG 1 Layer 3
  sampleRate: 44100
  numberOfChannels: 2
  bitrate: 128000
  codecProfile: CBR
  numberOfSamples: 7853184
  duration: 178.07673469387754
native:
  ID3v2.3:
    - id: TIT2
      value: La Douce Chanson Du Robot
    - id: TPE1
      value: reSet Sakrecoer
    - id: TALB
      value: Coolometrics
    - id: TCON
      value: Crooner
    - id: TRCK
      value: '19'
    - id: TYER
      value: '2005'
quality:
  warnings: []
common:
  track:
    'no': 19
    of: null
  disk:
    'no': null
    of: null
  title: La Douce Chanson Du Robot
  artists:
    - reSet Sakrecoer
  artist: reSet Sakrecoer
  album: Coolometrics
  genre:
    - Crooner
  year: 2005
transformed:
  ID3v2.3:
    TIT2: La Douce Chanson Du Robot
    TPE1: reSet Sakrecoer
    TALB: Coolometrics
    TCON: Crooner
    TRCK: '19'
    TYER: '2005'
all:
  TIT2: La Douce Chanson Du Robot
  TPE1: reSet Sakrecoer
  TALB: Coolometrics
  TCON: Crooner
  TRCK: '19'
  TYER: '2005'
nextTrack:
  path: >-
    /mnt/usb32gb/organized-sakrecoer.media/assets/albums/coolometrics/1-sis-n-pussy.mp3
  audio: /assets/albums/coolometrics/1-sis-n-pussy.mp3
  slug: coolometrics/1-sis-n-pussy
  albumSlug: coolometrics
  trackSlug: 1-sis-n-pussy
  coverPath: >-
    /mnt/usb32gb/organized-sakrecoer.media/assets/albums/coolometrics/1-sis-n-pussy.jpeg
  cover: /assets/albums/coolometrics/1-sis-n-pussy.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 128000
    codecProfile: CBR
    numberOfSamples: 6271488
    duration: 142.21061224489796
  native:
    ID3v2.3:
      - id: TIT2
        value: Sis'n'Pussy
      - id: TPE1
        value: reSet Sakrecoer
      - id: TALB
        value: Coolometrics
      - id: TRCK
        value: '1'
      - id: TCON
        value: Pop
      - id: TYER
        value: '2005'
  quality:
    warnings: []
  common:
    track:
      'no': 1
      of: null
    disk:
      'no': null
      of: null
    title: Sis'n'Pussy
    artists:
      - reSet Sakrecoer
    artist: reSet Sakrecoer
    album: Coolometrics
    genre:
      - Pop
    year: 2005
  transformed:
    ID3v2.3:
      TIT2: Sis'n'Pussy
      TPE1: reSet Sakrecoer
      TALB: Coolometrics
      TRCK: '1'
      TCON: Pop
      TYER: '2005'
  all:
    TIT2: Sis'n'Pussy
    TPE1: reSet Sakrecoer
    TALB: Coolometrics
    TRCK: '1'
    TCON: Pop
    TYER: '2005'
previousTrack:
  path: >-
    /mnt/usb32gb/organized-sakrecoer.media/assets/albums/coolometrics/18-love-for-fame.mp3
  audio: /assets/albums/coolometrics/18-love-for-fame.mp3
  slug: coolometrics/18-love-for-fame
  albumSlug: coolometrics
  trackSlug: 18-love-for-fame
  coverPath: >-
    /mnt/usb32gb/organized-sakrecoer.media/assets/albums/coolometrics/18-love-for-fame.jpeg
  cover: /assets/albums/coolometrics/18-love-for-fame.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 128000
    codecProfile: CBR
    numberOfSamples: 13549824
    duration: 307.2522448979592
  native:
    ID3v2.3:
      - id: TPE1
        value: reSet Sakrecoer
      - id: TALB
        value: Coolometrics
      - id: TIT2
        value: Love For Fame
      - id: TRCK
        value: '18'
      - id: TCON
        value: Emo
      - id: TYER
        value: '2005'
  quality:
    warnings: []
  common:
    track:
      'no': 18
      of: null
    disk:
      'no': null
      of: null
    artists:
      - reSet Sakrecoer
    artist: reSet Sakrecoer
    album: Coolometrics
    title: Love For Fame
    genre:
      - Emo
    year: 2005
  transformed:
    ID3v2.3:
      TPE1: reSet Sakrecoer
      TALB: Coolometrics
      TIT2: Love For Fame
      TRCK: '18'
      TCON: Emo
      TYER: '2005'
  all:
    TPE1: reSet Sakrecoer
    TALB: Coolometrics
    TIT2: Love For Fame
    TRCK: '18'
    TCON: Emo
    TYER: '2005'
---
