---
layout: track
path: >-
  /mnt/usb32gb/organized-sakrecoer.media/assets/albums/coolometrics/18-love-for-fame.mp3
audio: /assets/albums/coolometrics/18-love-for-fame.mp3
slug: coolometrics/18-love-for-fame
albumSlug: coolometrics
trackSlug: 18-love-for-fame
coverPath: >-
  /mnt/usb32gb/organized-sakrecoer.media/assets/albums/coolometrics/18-love-for-fame.jpeg
cover: /assets/albums/coolometrics/18-love-for-fame.jpeg
format:
  tagTypes:
    - ID3v2.3
  trackInfo: []
  lossless: false
  container: MPEG
  codec: MPEG 1 Layer 3
  sampleRate: 44100
  numberOfChannels: 2
  bitrate: 128000
  codecProfile: CBR
  numberOfSamples: 13549824
  duration: 307.2522448979592
native:
  ID3v2.3:
    - id: TPE1
      value: reSet Sakrecoer
    - id: TALB
      value: Coolometrics
    - id: TIT2
      value: Love For Fame
    - id: TRCK
      value: '18'
    - id: TCON
      value: Emo
    - id: TYER
      value: '2005'
quality:
  warnings: []
common:
  track:
    'no': 18
    of: null
  disk:
    'no': null
    of: null
  artists:
    - reSet Sakrecoer
  artist: reSet Sakrecoer
  album: Coolometrics
  title: Love For Fame
  genre:
    - Emo
  year: 2005
transformed:
  ID3v2.3:
    TPE1: reSet Sakrecoer
    TALB: Coolometrics
    TIT2: Love For Fame
    TRCK: '18'
    TCON: Emo
    TYER: '2005'
all:
  TPE1: reSet Sakrecoer
  TALB: Coolometrics
  TIT2: Love For Fame
  TRCK: '18'
  TCON: Emo
  TYER: '2005'
nextTrack:
  path: >-
    /mnt/usb32gb/organized-sakrecoer.media/assets/albums/coolometrics/19-la-douce-chanson-du-robot.mp3
  audio: /assets/albums/coolometrics/19-la-douce-chanson-du-robot.mp3
  slug: coolometrics/19-la-douce-chanson-du-robot
  albumSlug: coolometrics
  trackSlug: 19-la-douce-chanson-du-robot
  coverPath: >-
    /mnt/usb32gb/organized-sakrecoer.media/assets/albums/coolometrics/19-la-douce-chanson-du-robot.jpeg
  cover: /assets/albums/coolometrics/19-la-douce-chanson-du-robot.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 128000
    codecProfile: CBR
    numberOfSamples: 7853184
    duration: 178.07673469387754
  native:
    ID3v2.3:
      - id: TIT2
        value: La Douce Chanson Du Robot
      - id: TPE1
        value: reSet Sakrecoer
      - id: TALB
        value: Coolometrics
      - id: TCON
        value: Crooner
      - id: TRCK
        value: '19'
      - id: TYER
        value: '2005'
  quality:
    warnings: []
  common:
    track:
      'no': 19
      of: null
    disk:
      'no': null
      of: null
    title: La Douce Chanson Du Robot
    artists:
      - reSet Sakrecoer
    artist: reSet Sakrecoer
    album: Coolometrics
    genre:
      - Crooner
    year: 2005
  transformed:
    ID3v2.3:
      TIT2: La Douce Chanson Du Robot
      TPE1: reSet Sakrecoer
      TALB: Coolometrics
      TCON: Crooner
      TRCK: '19'
      TYER: '2005'
  all:
    TIT2: La Douce Chanson Du Robot
    TPE1: reSet Sakrecoer
    TALB: Coolometrics
    TCON: Crooner
    TRCK: '19'
    TYER: '2005'
previousTrack:
  path: >-
    /mnt/usb32gb/organized-sakrecoer.media/assets/albums/coolometrics/17-battlezoo.mp3
  audio: /assets/albums/coolometrics/17-battlezoo.mp3
  slug: coolometrics/17-battlezoo
  albumSlug: coolometrics
  trackSlug: 17-battlezoo
  coverPath: >-
    /mnt/usb32gb/organized-sakrecoer.media/assets/albums/coolometrics/17-battlezoo.jpeg
  cover: /assets/albums/coolometrics/17-battlezoo.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 128000
    codecProfile: CBR
    numberOfSamples: 10275840
    duration: 233.0122448979592
  native:
    ID3v2.3:
      - id: TIT2
        value: Battlezoo
      - id: TPE1
        value: reSet Sakrecoer
      - id: TALB
        value: Coolometrics
      - id: TCON
        value: Emo
      - id: TRCK
        value: '17'
      - id: TYER
        value: '2005'
  quality:
    warnings: []
  common:
    track:
      'no': 17
      of: null
    disk:
      'no': null
      of: null
    title: Battlezoo
    artists:
      - reSet Sakrecoer
    artist: reSet Sakrecoer
    album: Coolometrics
    genre:
      - Emo
    year: 2005
  transformed:
    ID3v2.3:
      TIT2: Battlezoo
      TPE1: reSet Sakrecoer
      TALB: Coolometrics
      TCON: Emo
      TRCK: '17'
      TYER: '2005'
  all:
    TIT2: Battlezoo
    TPE1: reSet Sakrecoer
    TALB: Coolometrics
    TCON: Emo
    TRCK: '17'
    TYER: '2005'
---
