---
layout: track
path: >-
  /mnt/usb32gb/organized-sakrecoer.media/assets/albums/the-house-of-set/5-acid-forest.mp3
audio: /assets/albums/the-house-of-set/5-acid-forest.mp3
slug: the-house-of-set/5-acid-forest
albumSlug: the-house-of-set
trackSlug: 5-acid-forest
coverPath: >-
  /mnt/usb32gb/organized-sakrecoer.media/assets/albums/the-house-of-set/5-acid-forest.jpeg
cover: /assets/albums/the-house-of-set/5-acid-forest.jpeg
format:
  tagTypes:
    - ID3v2.3
  trackInfo: []
  lossless: false
  container: MPEG
  codec: MPEG 1 Layer 3
  sampleRate: 44100
  numberOfChannels: 2
  bitrate: 128000
  codecProfile: CBR
  numberOfSamples: 20156544
  duration: 457.06448979591835
native:
  ID3v2.3:
    - id: TIT2
      value: Acid Forest
    - id: TPE1
      value: Simio Sakrecoer
    - id: TALB
      value: The House Of Set
    - id: TYER
      value: '2012'
    - id: TRCK
      value: '5'
    - id: TCON
      value: Electronic
    - id: COMM
      value:
        language: XXX
        description: Comment
        text: sakrecoer.com/simio
quality:
  warnings: []
common:
  track:
    'no': 5
    of: null
  disk:
    'no': null
    of: null
  title: Acid Forest
  artists:
    - Simio Sakrecoer
  artist: Simio Sakrecoer
  album: The House Of Set
  year: 2012
  genre:
    - Electronic
  comment:
    - sakrecoer.com/simio
transformed:
  ID3v2.3:
    TIT2: Acid Forest
    TPE1: Simio Sakrecoer
    TALB: The House Of Set
    TYER: '2012'
    TRCK: '5'
    TCON: Electronic
    Comment: sakrecoer.com/simio
all:
  TIT2: Acid Forest
  TPE1: Simio Sakrecoer
  TALB: The House Of Set
  TYER: '2012'
  TRCK: '5'
  TCON: Electronic
  Comment: sakrecoer.com/simio
nextTrack:
  path: >-
    /mnt/usb32gb/organized-sakrecoer.media/assets/albums/the-house-of-set/6-ask-me.mp3
  audio: /assets/albums/the-house-of-set/6-ask-me.mp3
  slug: the-house-of-set/6-ask-me
  albumSlug: the-house-of-set
  trackSlug: 6-ask-me
  coverPath: >-
    /mnt/usb32gb/organized-sakrecoer.media/assets/albums/the-house-of-set/6-ask-me.jpeg
  cover: /assets/albums/the-house-of-set/6-ask-me.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 128000
    codecProfile: CBR
    numberOfSamples: 15260544
    duration: 346.04408163265305
  native:
    ID3v2.3:
      - id: TIT2
        value: Ask Me
      - id: TPE1
        value: Simio Sakrecoer
      - id: TALB
        value: The House Of Set
      - id: TRCK
        value: '6'
      - id: TCON
        value: Electronic
      - id: COMM
        value:
          language: XXX
          description: Comment
          text: sakrecoer.com/simio
      - id: TYER
        value: '2012'
  quality:
    warnings: []
  common:
    track:
      'no': 6
      of: null
    disk:
      'no': null
      of: null
    title: Ask Me
    artists:
      - Simio Sakrecoer
    artist: Simio Sakrecoer
    album: The House Of Set
    genre:
      - Electronic
    comment:
      - sakrecoer.com/simio
    year: 2012
  transformed:
    ID3v2.3:
      TIT2: Ask Me
      TPE1: Simio Sakrecoer
      TALB: The House Of Set
      TRCK: '6'
      TCON: Electronic
      Comment: sakrecoer.com/simio
      TYER: '2012'
  all:
    TIT2: Ask Me
    TPE1: Simio Sakrecoer
    TALB: The House Of Set
    TRCK: '6'
    TCON: Electronic
    Comment: sakrecoer.com/simio
    TYER: '2012'
previousTrack:
  path: >-
    /mnt/usb32gb/organized-sakrecoer.media/assets/albums/the-house-of-set/4-quasi-lucid.mp3
  audio: /assets/albums/the-house-of-set/4-quasi-lucid.mp3
  slug: the-house-of-set/4-quasi-lucid
  albumSlug: the-house-of-set
  trackSlug: 4-quasi-lucid
  coverPath: >-
    /mnt/usb32gb/organized-sakrecoer.media/assets/albums/the-house-of-set/4-quasi-lucid.jpeg
  cover: /assets/albums/the-house-of-set/4-quasi-lucid.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 128000
    codecProfile: CBR
    numberOfSamples: 16237440
    duration: 368.19591836734696
  native:
    ID3v2.3:
      - id: TIT2
        value: Quasi Lucid
      - id: TPE1
        value: Simio Sakrecoer
      - id: TALB
        value: The House Of Set
      - id: TRCK
        value: '4'
      - id: TCON
        value: Electronic
      - id: COMM
        value:
          language: XXX
          description: Comment
          text: sakrecoer.com/simio
      - id: TYER
        value: '2012'
  quality:
    warnings: []
  common:
    track:
      'no': 4
      of: null
    disk:
      'no': null
      of: null
    title: Quasi Lucid
    artists:
      - Simio Sakrecoer
    artist: Simio Sakrecoer
    album: The House Of Set
    genre:
      - Electronic
    comment:
      - sakrecoer.com/simio
    year: 2012
  transformed:
    ID3v2.3:
      TIT2: Quasi Lucid
      TPE1: Simio Sakrecoer
      TALB: The House Of Set
      TRCK: '4'
      TCON: Electronic
      Comment: sakrecoer.com/simio
      TYER: '2012'
  all:
    TIT2: Quasi Lucid
    TPE1: Simio Sakrecoer
    TALB: The House Of Set
    TRCK: '4'
    TCON: Electronic
    Comment: sakrecoer.com/simio
    TYER: '2012'
---
