---
layout: track
path: >-
  /mnt/usb32gb/organized-sakrecoer.media/assets/albums/the-house-of-set/1-subway.mp3
audio: /assets/albums/the-house-of-set/1-subway.mp3
slug: the-house-of-set/1-subway
albumSlug: the-house-of-set
trackSlug: 1-subway
coverPath: >-
  /mnt/usb32gb/organized-sakrecoer.media/assets/albums/the-house-of-set/1-subway.jpeg
cover: /assets/albums/the-house-of-set/1-subway.jpeg
format:
  tagTypes:
    - ID3v2.3
  trackInfo: []
  lossless: false
  container: MPEG
  codec: MPEG 1 Layer 3
  sampleRate: 44100
  numberOfChannels: 2
  bitrate: 128000
  codecProfile: CBR
  numberOfSamples: 15352704
  duration: 348.1338775510204
native:
  ID3v2.3:
    - id: TIT2
      value: Subway
    - id: TPE1
      value: Simio Sakrecoer
    - id: TALB
      value: The House Of Set
    - id: TYER
      value: '2012'
    - id: TRCK
      value: '1'
    - id: TCON
      value: Electronic
    - id: COMM
      value:
        language: XXX
        description: Comment
        text: sakrecoer.com/simio
quality:
  warnings: []
common:
  track:
    'no': 1
    of: null
  disk:
    'no': null
    of: null
  title: Subway
  artists:
    - Simio Sakrecoer
  artist: Simio Sakrecoer
  album: The House Of Set
  year: 2012
  genre:
    - Electronic
  comment:
    - sakrecoer.com/simio
transformed:
  ID3v2.3:
    TIT2: Subway
    TPE1: Simio Sakrecoer
    TALB: The House Of Set
    TYER: '2012'
    TRCK: '1'
    TCON: Electronic
    Comment: sakrecoer.com/simio
all:
  TIT2: Subway
  TPE1: Simio Sakrecoer
  TALB: The House Of Set
  TYER: '2012'
  TRCK: '1'
  TCON: Electronic
  Comment: sakrecoer.com/simio
nextTrack:
  path: >-
    /mnt/usb32gb/organized-sakrecoer.media/assets/albums/the-house-of-set/2-everyday-is-a-life.mp3
  audio: /assets/albums/the-house-of-set/2-everyday-is-a-life.mp3
  slug: the-house-of-set/2-everyday-is-a-life
  albumSlug: the-house-of-set
  trackSlug: 2-everyday-is-a-life
  coverPath: >-
    /mnt/usb32gb/organized-sakrecoer.media/assets/albums/the-house-of-set/2-everyday-is-a-life.jpeg
  cover: /assets/albums/the-house-of-set/2-everyday-is-a-life.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 128000
    codecProfile: CBR
    numberOfSamples: 14555520
    duration: 330.0571428571429
  native:
    ID3v2.3:
      - id: TIT2
        value: Everyday Is A Life
      - id: TPE1
        value: Simio Sakrecoer
      - id: TALB
        value: The House Of Set
      - id: TRCK
        value: '2'
      - id: TCON
        value: Electronic
      - id: COMM
        value:
          language: XXX
          description: Comment
          text: sakrecoer.com/simio
      - id: TYER
        value: '2012'
  quality:
    warnings: []
  common:
    track:
      'no': 2
      of: null
    disk:
      'no': null
      of: null
    title: Everyday Is A Life
    artists:
      - Simio Sakrecoer
    artist: Simio Sakrecoer
    album: The House Of Set
    genre:
      - Electronic
    comment:
      - sakrecoer.com/simio
    year: 2012
  transformed:
    ID3v2.3:
      TIT2: Everyday Is A Life
      TPE1: Simio Sakrecoer
      TALB: The House Of Set
      TRCK: '2'
      TCON: Electronic
      Comment: sakrecoer.com/simio
      TYER: '2012'
  all:
    TIT2: Everyday Is A Life
    TPE1: Simio Sakrecoer
    TALB: The House Of Set
    TRCK: '2'
    TCON: Electronic
    Comment: sakrecoer.com/simio
    TYER: '2012'
previousTrack:
  path: >-
    /mnt/usb32gb/organized-sakrecoer.media/assets/albums/the-house-of-set/8-saint-fragle-day.mp3
  audio: /assets/albums/the-house-of-set/8-saint-fragle-day.mp3
  slug: the-house-of-set/8-saint-fragle-day
  albumSlug: the-house-of-set
  trackSlug: 8-saint-fragle-day
  coverPath: >-
    /mnt/usb32gb/organized-sakrecoer.media/assets/albums/the-house-of-set/8-saint-fragle-day.jpeg
  cover: /assets/albums/the-house-of-set/8-saint-fragle-day.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 128000
    codecProfile: CBR
    numberOfSamples: 14337792
    duration: 325.12
  native:
    ID3v2.3:
      - id: TIT2
        value: Saint Fragle Day
      - id: TPE1
        value: Simio Sakrecoer
      - id: TALB
        value: The House Of Set
      - id: TRCK
        value: '8'
      - id: TCON
        value: Electronic
      - id: COMM
        value:
          language: XXX
          description: Comment
          text: sakrecoer.com/simio
      - id: TYER
        value: '2012'
  quality:
    warnings: []
  common:
    track:
      'no': 8
      of: null
    disk:
      'no': null
      of: null
    title: Saint Fragle Day
    artists:
      - Simio Sakrecoer
    artist: Simio Sakrecoer
    album: The House Of Set
    genre:
      - Electronic
    comment:
      - sakrecoer.com/simio
    year: 2012
  transformed:
    ID3v2.3:
      TIT2: Saint Fragle Day
      TPE1: Simio Sakrecoer
      TALB: The House Of Set
      TRCK: '8'
      TCON: Electronic
      Comment: sakrecoer.com/simio
      TYER: '2012'
  all:
    TIT2: Saint Fragle Day
    TPE1: Simio Sakrecoer
    TALB: The House Of Set
    TRCK: '8'
    TCON: Electronic
    Comment: sakrecoer.com/simio
    TYER: '2012'
---
