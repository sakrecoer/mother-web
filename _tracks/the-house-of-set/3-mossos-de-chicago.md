---
layout: track
path: >-
  /mnt/usb32gb/organized-sakrecoer.media/assets/albums/the-house-of-set/3-mossos-de-chicago.mp3
audio: /assets/albums/the-house-of-set/3-mossos-de-chicago.mp3
slug: the-house-of-set/3-mossos-de-chicago
albumSlug: the-house-of-set
trackSlug: 3-mossos-de-chicago
coverPath: >-
  /mnt/usb32gb/organized-sakrecoer.media/assets/albums/the-house-of-set/3-mossos-de-chicago.jpeg
cover: /assets/albums/the-house-of-set/3-mossos-de-chicago.jpeg
format:
  tagTypes:
    - ID3v2.3
  trackInfo: []
  lossless: false
  container: MPEG
  codec: MPEG 1 Layer 3
  sampleRate: 44100
  numberOfChannels: 2
  bitrate: 128000
  codecProfile: CBR
  numberOfSamples: 15466752
  duration: 350.72
native:
  ID3v2.3:
    - id: TIT2
      value: Mossos De Chicago
    - id: TPE1
      value: Simio Sakrecoer
    - id: TALB
      value: The House Of Set
    - id: TYER
      value: '2012'
    - id: TRCK
      value: '3'
    - id: TCON
      value: Electronic
    - id: COMM
      value:
        language: XXX
        description: Comment
        text: sakrecoer.com/simio
quality:
  warnings: []
common:
  track:
    'no': 3
    of: null
  disk:
    'no': null
    of: null
  title: Mossos De Chicago
  artists:
    - Simio Sakrecoer
  artist: Simio Sakrecoer
  album: The House Of Set
  year: 2012
  genre:
    - Electronic
  comment:
    - sakrecoer.com/simio
transformed:
  ID3v2.3:
    TIT2: Mossos De Chicago
    TPE1: Simio Sakrecoer
    TALB: The House Of Set
    TYER: '2012'
    TRCK: '3'
    TCON: Electronic
    Comment: sakrecoer.com/simio
all:
  TIT2: Mossos De Chicago
  TPE1: Simio Sakrecoer
  TALB: The House Of Set
  TYER: '2012'
  TRCK: '3'
  TCON: Electronic
  Comment: sakrecoer.com/simio
nextTrack:
  path: >-
    /mnt/usb32gb/organized-sakrecoer.media/assets/albums/the-house-of-set/4-quasi-lucid.mp3
  audio: /assets/albums/the-house-of-set/4-quasi-lucid.mp3
  slug: the-house-of-set/4-quasi-lucid
  albumSlug: the-house-of-set
  trackSlug: 4-quasi-lucid
  coverPath: >-
    /mnt/usb32gb/organized-sakrecoer.media/assets/albums/the-house-of-set/4-quasi-lucid.jpeg
  cover: /assets/albums/the-house-of-set/4-quasi-lucid.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 128000
    codecProfile: CBR
    numberOfSamples: 16237440
    duration: 368.19591836734696
  native:
    ID3v2.3:
      - id: TIT2
        value: Quasi Lucid
      - id: TPE1
        value: Simio Sakrecoer
      - id: TALB
        value: The House Of Set
      - id: TRCK
        value: '4'
      - id: TCON
        value: Electronic
      - id: COMM
        value:
          language: XXX
          description: Comment
          text: sakrecoer.com/simio
      - id: TYER
        value: '2012'
  quality:
    warnings: []
  common:
    track:
      'no': 4
      of: null
    disk:
      'no': null
      of: null
    title: Quasi Lucid
    artists:
      - Simio Sakrecoer
    artist: Simio Sakrecoer
    album: The House Of Set
    genre:
      - Electronic
    comment:
      - sakrecoer.com/simio
    year: 2012
  transformed:
    ID3v2.3:
      TIT2: Quasi Lucid
      TPE1: Simio Sakrecoer
      TALB: The House Of Set
      TRCK: '4'
      TCON: Electronic
      Comment: sakrecoer.com/simio
      TYER: '2012'
  all:
    TIT2: Quasi Lucid
    TPE1: Simio Sakrecoer
    TALB: The House Of Set
    TRCK: '4'
    TCON: Electronic
    Comment: sakrecoer.com/simio
    TYER: '2012'
previousTrack:
  path: >-
    /mnt/usb32gb/organized-sakrecoer.media/assets/albums/the-house-of-set/2-everyday-is-a-life.mp3
  audio: /assets/albums/the-house-of-set/2-everyday-is-a-life.mp3
  slug: the-house-of-set/2-everyday-is-a-life
  albumSlug: the-house-of-set
  trackSlug: 2-everyday-is-a-life
  coverPath: >-
    /mnt/usb32gb/organized-sakrecoer.media/assets/albums/the-house-of-set/2-everyday-is-a-life.jpeg
  cover: /assets/albums/the-house-of-set/2-everyday-is-a-life.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 128000
    codecProfile: CBR
    numberOfSamples: 14555520
    duration: 330.0571428571429
  native:
    ID3v2.3:
      - id: TIT2
        value: Everyday Is A Life
      - id: TPE1
        value: Simio Sakrecoer
      - id: TALB
        value: The House Of Set
      - id: TRCK
        value: '2'
      - id: TCON
        value: Electronic
      - id: COMM
        value:
          language: XXX
          description: Comment
          text: sakrecoer.com/simio
      - id: TYER
        value: '2012'
  quality:
    warnings: []
  common:
    track:
      'no': 2
      of: null
    disk:
      'no': null
      of: null
    title: Everyday Is A Life
    artists:
      - Simio Sakrecoer
    artist: Simio Sakrecoer
    album: The House Of Set
    genre:
      - Electronic
    comment:
      - sakrecoer.com/simio
    year: 2012
  transformed:
    ID3v2.3:
      TIT2: Everyday Is A Life
      TPE1: Simio Sakrecoer
      TALB: The House Of Set
      TRCK: '2'
      TCON: Electronic
      Comment: sakrecoer.com/simio
      TYER: '2012'
  all:
    TIT2: Everyday Is A Life
    TPE1: Simio Sakrecoer
    TALB: The House Of Set
    TRCK: '2'
    TCON: Electronic
    Comment: sakrecoer.com/simio
    TYER: '2012'
---
