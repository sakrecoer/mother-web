---
layout: track
path: >-
  /mnt/usb32gb/organized-sakrecoer.media/assets/albums/santas-audioserver/2-allein-in-das-all.mp3
audio: /assets/albums/santas-audioserver/2-allein-in-das-all.mp3
slug: santas-audioserver/2-allein-in-das-all
albumSlug: santas-audioserver
trackSlug: 2-allein-in-das-all
coverPath: >-
  /mnt/usb32gb/organized-sakrecoer.media/assets/albums/santas-audioserver/2-allein-in-das-all.jpeg
cover: /assets/albums/santas-audioserver/2-allein-in-das-all.jpeg
format:
  tagTypes:
    - ID3v2.3
  trackInfo: []
  lossless: false
  container: MPEG
  codec: MPEG 1 Layer 3
  sampleRate: 44100
  numberOfChannels: 2
  bitrate: 128000
  codecProfile: CBR
  numberOfSamples: 7455744
  duration: 169.06448979591838
native:
  ID3v2.3:
    - id: TIT2
      value: Allein In Das All
    - id: TPE1
      value: reSet
    - id: TALB
      value: Santas Audioserver
    - id: TRCK
      value: '2'
    - id: TCON
      value: Electro
    - id: TPUB
      value: Villa Magica Records
    - id: TYER
      value: '2004'
quality:
  warnings: []
common:
  track:
    'no': 2
    of: null
  disk:
    'no': null
    of: null
  title: Allein In Das All
  artists:
    - reSet
  artist: reSet
  album: Santas Audioserver
  genre:
    - Electro
  label:
    - Villa Magica Records
  year: 2004
transformed:
  ID3v2.3:
    TIT2: Allein In Das All
    TPE1: reSet
    TALB: Santas Audioserver
    TRCK: '2'
    TCON: Electro
    TPUB: Villa Magica Records
    TYER: '2004'
all:
  TIT2: Allein In Das All
  TPE1: reSet
  TALB: Santas Audioserver
  TRCK: '2'
  TCON: Electro
  TPUB: Villa Magica Records
  TYER: '2004'
nextTrack:
  path: >-
    /mnt/usb32gb/organized-sakrecoer.media/assets/albums/santas-audioserver/3-you-and-me.mp3
  audio: /assets/albums/santas-audioserver/3-you-and-me.mp3
  slug: santas-audioserver/3-you-and-me
  albumSlug: santas-audioserver
  trackSlug: 3-you-and-me
  coverPath: >-
    /mnt/usb32gb/organized-sakrecoer.media/assets/albums/santas-audioserver/3-you-and-me.jpeg
  cover: /assets/albums/santas-audioserver/3-you-and-me.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 128000
    codecProfile: CBR
    numberOfSamples: 10666368
    duration: 241.8677551020408
  native:
    ID3v2.3:
      - id: TIT2
        value: You And Me
      - id: TPE1
        value: reSet
      - id: TALB
        value: Santas Audioserver
      - id: TRCK
        value: '3'
      - id: TCON
        value: Electro
      - id: TPUB
        value: Villa Magica Records
      - id: TYER
        value: '2004'
  quality:
    warnings: []
  common:
    track:
      'no': 3
      of: null
    disk:
      'no': null
      of: null
    title: You And Me
    artists:
      - reSet
    artist: reSet
    album: Santas Audioserver
    genre:
      - Electro
    label:
      - Villa Magica Records
    year: 2004
  transformed:
    ID3v2.3:
      TIT2: You And Me
      TPE1: reSet
      TALB: Santas Audioserver
      TRCK: '3'
      TCON: Electro
      TPUB: Villa Magica Records
      TYER: '2004'
  all:
    TIT2: You And Me
    TPE1: reSet
    TALB: Santas Audioserver
    TRCK: '3'
    TCON: Electro
    TPUB: Villa Magica Records
    TYER: '2004'
previousTrack:
  path: >-
    /mnt/usb32gb/organized-sakrecoer.media/assets/albums/santas-audioserver/1-what-love-makes-me-do.mp3
  audio: /assets/albums/santas-audioserver/1-what-love-makes-me-do.mp3
  slug: santas-audioserver/1-what-love-makes-me-do
  albumSlug: santas-audioserver
  trackSlug: 1-what-love-makes-me-do
  coverPath: >-
    /mnt/usb32gb/organized-sakrecoer.media/assets/albums/santas-audioserver/1-what-love-makes-me-do.jpeg
  cover: /assets/albums/santas-audioserver/1-what-love-makes-me-do.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 128000
    codecProfile: CBR
    numberOfSamples: 8732160
    duration: 198.0081632653061
  native:
    ID3v2.3:
      - id: TIT2
        value: What Love Makes Me Do
      - id: TPE1
        value: reSet
      - id: TALB
        value: Santas Audioserver
      - id: TRCK
        value: '1'
      - id: TCON
        value: Electro
      - id: TPUB
        value: Villa Magica Records
      - id: TYER
        value: '2004'
  quality:
    warnings: []
  common:
    track:
      'no': 1
      of: null
    disk:
      'no': null
      of: null
    title: What Love Makes Me Do
    artists:
      - reSet
    artist: reSet
    album: Santas Audioserver
    genre:
      - Electro
    label:
      - Villa Magica Records
    year: 2004
  transformed:
    ID3v2.3:
      TIT2: What Love Makes Me Do
      TPE1: reSet
      TALB: Santas Audioserver
      TRCK: '1'
      TCON: Electro
      TPUB: Villa Magica Records
      TYER: '2004'
  all:
    TIT2: What Love Makes Me Do
    TPE1: reSet
    TALB: Santas Audioserver
    TRCK: '1'
    TCON: Electro
    TPUB: Villa Magica Records
    TYER: '2004'
---
