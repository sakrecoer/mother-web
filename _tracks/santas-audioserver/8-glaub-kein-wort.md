---
layout: track
path: >-
  /mnt/usb32gb/organized-sakrecoer.media/assets/albums/santas-audioserver/8-glaub-kein-wort.mp3
audio: /assets/albums/santas-audioserver/8-glaub-kein-wort.mp3
slug: santas-audioserver/8-glaub-kein-wort
albumSlug: santas-audioserver
trackSlug: 8-glaub-kein-wort
coverPath: >-
  /mnt/usb32gb/organized-sakrecoer.media/assets/albums/santas-audioserver/8-glaub-kein-wort.jpeg
cover: /assets/albums/santas-audioserver/8-glaub-kein-wort.jpeg
format:
  tagTypes:
    - ID3v2.3
  trackInfo: []
  lossless: false
  container: MPEG
  codec: MPEG 1 Layer 3
  sampleRate: 44100
  numberOfChannels: 2
  bitrate: 128000
  codecProfile: CBR
  numberOfSamples: 12769920
  duration: 289.56734693877553
native:
  ID3v2.3:
    - id: TIT2
      value: Glaub Kein Wort
    - id: TPE1
      value: reSet
    - id: TALB
      value: Santas Audioserver
    - id: TRCK
      value: '8'
    - id: TCON
      value: Electro
    - id: TPUB
      value: Villa Magica Records
    - id: TYER
      value: '2004'
quality:
  warnings: []
common:
  track:
    'no': 8
    of: null
  disk:
    'no': null
    of: null
  title: Glaub Kein Wort
  artists:
    - reSet
  artist: reSet
  album: Santas Audioserver
  genre:
    - Electro
  label:
    - Villa Magica Records
  year: 2004
transformed:
  ID3v2.3:
    TIT2: Glaub Kein Wort
    TPE1: reSet
    TALB: Santas Audioserver
    TRCK: '8'
    TCON: Electro
    TPUB: Villa Magica Records
    TYER: '2004'
all:
  TIT2: Glaub Kein Wort
  TPE1: reSet
  TALB: Santas Audioserver
  TRCK: '8'
  TCON: Electro
  TPUB: Villa Magica Records
  TYER: '2004'
nextTrack:
  path: >-
    /mnt/usb32gb/organized-sakrecoer.media/assets/albums/santas-audioserver/9-liar.mp3
  audio: /assets/albums/santas-audioserver/9-liar.mp3
  slug: santas-audioserver/9-liar
  albumSlug: santas-audioserver
  trackSlug: 9-liar
  coverPath: >-
    /mnt/usb32gb/organized-sakrecoer.media/assets/albums/santas-audioserver/9-liar.jpeg
  cover: /assets/albums/santas-audioserver/9-liar.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 128000
    codecProfile: CBR
    numberOfSamples: 12433536
    duration: 281.9395918367347
  native:
    ID3v2.3:
      - id: TIT2
        value: Liar
      - id: TPE1
        value: reSet
      - id: TALB
        value: Santas Audioserver
      - id: TRCK
        value: '9'
      - id: TCON
        value: Electro
      - id: TPUB
        value: Villa Magica Records
      - id: TYER
        value: '2004'
  quality:
    warnings: []
  common:
    track:
      'no': 9
      of: null
    disk:
      'no': null
      of: null
    title: Liar
    artists:
      - reSet
    artist: reSet
    album: Santas Audioserver
    genre:
      - Electro
    label:
      - Villa Magica Records
    year: 2004
  transformed:
    ID3v2.3:
      TIT2: Liar
      TPE1: reSet
      TALB: Santas Audioserver
      TRCK: '9'
      TCON: Electro
      TPUB: Villa Magica Records
      TYER: '2004'
  all:
    TIT2: Liar
    TPE1: reSet
    TALB: Santas Audioserver
    TRCK: '9'
    TCON: Electro
    TPUB: Villa Magica Records
    TYER: '2004'
previousTrack:
  path: >-
    /mnt/usb32gb/organized-sakrecoer.media/assets/albums/santas-audioserver/7-what-love-makes-me-do-bad-ass-mix-.mp3
  audio: /assets/albums/santas-audioserver/7-what-love-makes-me-do-bad-ass-mix-.mp3
  slug: santas-audioserver/7-what-love-makes-me-do-bad-ass-mix-
  albumSlug: santas-audioserver
  trackSlug: 7-what-love-makes-me-do-bad-ass-mix-
  coverPath: >-
    /mnt/usb32gb/organized-sakrecoer.media/assets/albums/santas-audioserver/7-what-love-makes-me-do-bad-ass-mix-.jpeg
  cover: /assets/albums/santas-audioserver/7-what-love-makes-me-do-bad-ass-mix-.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 128000
    codecProfile: CBR
    numberOfSamples: 12503808
    duration: 283.5330612244898
  native:
    ID3v2.3:
      - id: TIT2
        value: What Love Makes Me Do (Bad Ass Mix)
      - id: TPE1
        value: reSet
      - id: TALB
        value: Santas Audioserver
      - id: TRCK
        value: '7'
      - id: TCON
        value: Electro
      - id: TPUB
        value: Villa Magica Records
      - id: TYER
        value: '2004'
  quality:
    warnings: []
  common:
    track:
      'no': 7
      of: null
    disk:
      'no': null
      of: null
    title: What Love Makes Me Do (Bad Ass Mix)
    artists:
      - reSet
    artist: reSet
    album: Santas Audioserver
    genre:
      - Electro
    label:
      - Villa Magica Records
    year: 2004
  transformed:
    ID3v2.3:
      TIT2: What Love Makes Me Do (Bad Ass Mix)
      TPE1: reSet
      TALB: Santas Audioserver
      TRCK: '7'
      TCON: Electro
      TPUB: Villa Magica Records
      TYER: '2004'
  all:
    TIT2: What Love Makes Me Do (Bad Ass Mix)
    TPE1: reSet
    TALB: Santas Audioserver
    TRCK: '7'
    TCON: Electro
    TPUB: Villa Magica Records
    TYER: '2004'
---
