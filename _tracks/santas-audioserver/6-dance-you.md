---
layout: track
path: >-
  /mnt/usb32gb/organized-sakrecoer.media/assets/albums/santas-audioserver/6-dance-you.mp3
audio: /assets/albums/santas-audioserver/6-dance-you.mp3
slug: santas-audioserver/6-dance-you
albumSlug: santas-audioserver
trackSlug: 6-dance-you
coverPath: >-
  /mnt/usb32gb/organized-sakrecoer.media/assets/albums/santas-audioserver/6-dance-you.jpeg
cover: /assets/albums/santas-audioserver/6-dance-you.jpeg
format:
  tagTypes:
    - ID3v2.3
  trackInfo: []
  lossless: false
  container: MPEG
  codec: MPEG 1 Layer 3
  sampleRate: 44100
  numberOfChannels: 2
  bitrate: 128000
  codecProfile: CBR
  numberOfSamples: 7276032
  duration: 164.98938775510203
native:
  ID3v2.3:
    - id: TIT2
      value: Dance You
    - id: TPE1
      value: reSet
    - id: TALB
      value: Santas Audioserver
    - id: TRCK
      value: '6'
    - id: TCON
      value: Electro
    - id: TPUB
      value: Villa Magica Records
    - id: TYER
      value: '2004'
quality:
  warnings: []
common:
  track:
    'no': 6
    of: null
  disk:
    'no': null
    of: null
  title: Dance You
  artists:
    - reSet
  artist: reSet
  album: Santas Audioserver
  genre:
    - Electro
  label:
    - Villa Magica Records
  year: 2004
transformed:
  ID3v2.3:
    TIT2: Dance You
    TPE1: reSet
    TALB: Santas Audioserver
    TRCK: '6'
    TCON: Electro
    TPUB: Villa Magica Records
    TYER: '2004'
all:
  TIT2: Dance You
  TPE1: reSet
  TALB: Santas Audioserver
  TRCK: '6'
  TCON: Electro
  TPUB: Villa Magica Records
  TYER: '2004'
nextTrack:
  path: >-
    /mnt/usb32gb/organized-sakrecoer.media/assets/albums/santas-audioserver/7-what-love-makes-me-do-bad-ass-mix-.mp3
  audio: /assets/albums/santas-audioserver/7-what-love-makes-me-do-bad-ass-mix-.mp3
  slug: santas-audioserver/7-what-love-makes-me-do-bad-ass-mix-
  albumSlug: santas-audioserver
  trackSlug: 7-what-love-makes-me-do-bad-ass-mix-
  coverPath: >-
    /mnt/usb32gb/organized-sakrecoer.media/assets/albums/santas-audioserver/7-what-love-makes-me-do-bad-ass-mix-.jpeg
  cover: /assets/albums/santas-audioserver/7-what-love-makes-me-do-bad-ass-mix-.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 128000
    codecProfile: CBR
    numberOfSamples: 12503808
    duration: 283.5330612244898
  native:
    ID3v2.3:
      - id: TIT2
        value: What Love Makes Me Do (Bad Ass Mix)
      - id: TPE1
        value: reSet
      - id: TALB
        value: Santas Audioserver
      - id: TRCK
        value: '7'
      - id: TCON
        value: Electro
      - id: TPUB
        value: Villa Magica Records
      - id: TYER
        value: '2004'
  quality:
    warnings: []
  common:
    track:
      'no': 7
      of: null
    disk:
      'no': null
      of: null
    title: What Love Makes Me Do (Bad Ass Mix)
    artists:
      - reSet
    artist: reSet
    album: Santas Audioserver
    genre:
      - Electro
    label:
      - Villa Magica Records
    year: 2004
  transformed:
    ID3v2.3:
      TIT2: What Love Makes Me Do (Bad Ass Mix)
      TPE1: reSet
      TALB: Santas Audioserver
      TRCK: '7'
      TCON: Electro
      TPUB: Villa Magica Records
      TYER: '2004'
  all:
    TIT2: What Love Makes Me Do (Bad Ass Mix)
    TPE1: reSet
    TALB: Santas Audioserver
    TRCK: '7'
    TCON: Electro
    TPUB: Villa Magica Records
    TYER: '2004'
previousTrack:
  path: >-
    /mnt/usb32gb/organized-sakrecoer.media/assets/albums/santas-audioserver/5-not-pissed-happy.mp3
  audio: /assets/albums/santas-audioserver/5-not-pissed-happy.mp3
  slug: santas-audioserver/5-not-pissed-happy
  albumSlug: santas-audioserver
  trackSlug: 5-not-pissed-happy
  coverPath: >-
    /mnt/usb32gb/organized-sakrecoer.media/assets/albums/santas-audioserver/5-not-pissed-happy.jpeg
  cover: /assets/albums/santas-audioserver/5-not-pissed-happy.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 128000
    codecProfile: CBR
    numberOfSamples: 10251648
    duration: 232.46367346938774
  native:
    ID3v2.3:
      - id: TIT2
        value: 'Not Pissed, Happy'
      - id: TPE1
        value: reSet
      - id: TALB
        value: Santas Audioserver
      - id: TRCK
        value: '5'
      - id: TCON
        value: Electro
      - id: TPUB
        value: Villa Magica Records
      - id: TYER
        value: '2004'
  quality:
    warnings: []
  common:
    track:
      'no': 5
      of: null
    disk:
      'no': null
      of: null
    title: 'Not Pissed, Happy'
    artists:
      - reSet
    artist: reSet
    album: Santas Audioserver
    genre:
      - Electro
    label:
      - Villa Magica Records
    year: 2004
  transformed:
    ID3v2.3:
      TIT2: 'Not Pissed, Happy'
      TPE1: reSet
      TALB: Santas Audioserver
      TRCK: '5'
      TCON: Electro
      TPUB: Villa Magica Records
      TYER: '2004'
  all:
    TIT2: 'Not Pissed, Happy'
    TPE1: reSet
    TALB: Santas Audioserver
    TRCK: '5'
    TCON: Electro
    TPUB: Villa Magica Records
    TYER: '2004'
---
