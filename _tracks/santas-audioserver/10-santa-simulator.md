---
layout: track
path: >-
  /mnt/usb32gb/organized-sakrecoer.media/assets/albums/santas-audioserver/10-santa-simulator.mp3
audio: /assets/albums/santas-audioserver/10-santa-simulator.mp3
slug: santas-audioserver/10-santa-simulator
albumSlug: santas-audioserver
trackSlug: 10-santa-simulator
coverPath: >-
  /mnt/usb32gb/organized-sakrecoer.media/assets/albums/santas-audioserver/10-santa-simulator.jpeg
cover: /assets/albums/santas-audioserver/10-santa-simulator.jpeg
format:
  tagTypes:
    - ID3v2.3
  trackInfo: []
  lossless: false
  container: MPEG
  codec: MPEG 1 Layer 3
  sampleRate: 44100
  numberOfChannels: 2
  bitrate: 128000
  codecProfile: CBR
  numberOfSamples: 10356480
  duration: 234.84081632653061
native:
  ID3v2.3:
    - id: TIT2
      value: Santa Simulator
    - id: TPE1
      value: reSet
    - id: TALB
      value: Santas Audioserver
    - id: TRCK
      value: '10'
    - id: TCON
      value: Electro
    - id: TPUB
      value: Villa Magica Records
    - id: TYER
      value: '2004'
quality:
  warnings: []
common:
  track:
    'no': 10
    of: null
  disk:
    'no': null
    of: null
  title: Santa Simulator
  artists:
    - reSet
  artist: reSet
  album: Santas Audioserver
  genre:
    - Electro
  label:
    - Villa Magica Records
  year: 2004
transformed:
  ID3v2.3:
    TIT2: Santa Simulator
    TPE1: reSet
    TALB: Santas Audioserver
    TRCK: '10'
    TCON: Electro
    TPUB: Villa Magica Records
    TYER: '2004'
all:
  TIT2: Santa Simulator
  TPE1: reSet
  TALB: Santas Audioserver
  TRCK: '10'
  TCON: Electro
  TPUB: Villa Magica Records
  TYER: '2004'
nextTrack:
  path: >-
    /mnt/usb32gb/organized-sakrecoer.media/assets/albums/santas-audioserver/11-tracker-upfront.mp3
  audio: /assets/albums/santas-audioserver/11-tracker-upfront.mp3
  slug: santas-audioserver/11-tracker-upfront
  albumSlug: santas-audioserver
  trackSlug: 11-tracker-upfront
  coverPath: >-
    /mnt/usb32gb/organized-sakrecoer.media/assets/albums/santas-audioserver/11-tracker-upfront.jpeg
  cover: /assets/albums/santas-audioserver/11-tracker-upfront.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 128000
    codecProfile: CBR
    numberOfSamples: 20098944
    duration: 455.75836734693877
  native:
    ID3v2.3:
      - id: TIT2
        value: Tracker Upfront
      - id: TPE1
        value: reSet
      - id: TALB
        value: Santas Audioserver
      - id: TRCK
        value: '11'
      - id: TCON
        value: Electro
      - id: TPUB
        value: Villa Magica Records
      - id: TYER
        value: '2004'
  quality:
    warnings: []
  common:
    track:
      'no': 11
      of: null
    disk:
      'no': null
      of: null
    title: Tracker Upfront
    artists:
      - reSet
    artist: reSet
    album: Santas Audioserver
    genre:
      - Electro
    label:
      - Villa Magica Records
    year: 2004
  transformed:
    ID3v2.3:
      TIT2: Tracker Upfront
      TPE1: reSet
      TALB: Santas Audioserver
      TRCK: '11'
      TCON: Electro
      TPUB: Villa Magica Records
      TYER: '2004'
  all:
    TIT2: Tracker Upfront
    TPE1: reSet
    TALB: Santas Audioserver
    TRCK: '11'
    TCON: Electro
    TPUB: Villa Magica Records
    TYER: '2004'
previousTrack:
  path: >-
    /mnt/usb32gb/organized-sakrecoer.media/assets/albums/santas-audioserver/9-liar.mp3
  audio: /assets/albums/santas-audioserver/9-liar.mp3
  slug: santas-audioserver/9-liar
  albumSlug: santas-audioserver
  trackSlug: 9-liar
  coverPath: >-
    /mnt/usb32gb/organized-sakrecoer.media/assets/albums/santas-audioserver/9-liar.jpeg
  cover: /assets/albums/santas-audioserver/9-liar.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 128000
    codecProfile: CBR
    numberOfSamples: 12433536
    duration: 281.9395918367347
  native:
    ID3v2.3:
      - id: TIT2
        value: Liar
      - id: TPE1
        value: reSet
      - id: TALB
        value: Santas Audioserver
      - id: TRCK
        value: '9'
      - id: TCON
        value: Electro
      - id: TPUB
        value: Villa Magica Records
      - id: TYER
        value: '2004'
  quality:
    warnings: []
  common:
    track:
      'no': 9
      of: null
    disk:
      'no': null
      of: null
    title: Liar
    artists:
      - reSet
    artist: reSet
    album: Santas Audioserver
    genre:
      - Electro
    label:
      - Villa Magica Records
    year: 2004
  transformed:
    ID3v2.3:
      TIT2: Liar
      TPE1: reSet
      TALB: Santas Audioserver
      TRCK: '9'
      TCON: Electro
      TPUB: Villa Magica Records
      TYER: '2004'
  all:
    TIT2: Liar
    TPE1: reSet
    TALB: Santas Audioserver
    TRCK: '9'
    TCON: Electro
    TPUB: Villa Magica Records
    TYER: '2004'
---
