---
layout: album
slug: santas-audioserver
name: Santas Audioserver
artists: reSet
bitrate: 128000
trackCount: 12
cover: /assets/albums/santas-audioserver/1-what-love-makes-me-do.jpeg
date: 2004-1-1
tracks:
  - path: >-
      /mnt/usb32gb/organized-sakrecoer.media/assets/albums/santas-audioserver/1-what-love-makes-me-do.mp3
    audio: /assets/albums/santas-audioserver/1-what-love-makes-me-do.mp3
    slug: santas-audioserver/1-what-love-makes-me-do
    albumSlug: santas-audioserver
    trackSlug: 1-what-love-makes-me-do
    coverPath: >-
      /mnt/usb32gb/organized-sakrecoer.media/assets/albums/santas-audioserver/1-what-love-makes-me-do.jpeg
    cover: /assets/albums/santas-audioserver/1-what-love-makes-me-do.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 2
      bitrate: 128000
      codecProfile: CBR
      numberOfSamples: 8732160
      duration: 198.0081632653061
    native:
      ID3v2.3:
        - id: TIT2
          value: What Love Makes Me Do
        - id: TPE1
          value: reSet
        - id: TALB
          value: Santas Audioserver
        - id: TRCK
          value: '1'
        - id: TCON
          value: Electro
        - id: TPUB
          value: Villa Magica Records
        - id: TYER
          value: '2004'
    quality:
      warnings: []
    common:
      track:
        'no': 1
        of: null
      disk:
        'no': null
        of: null
      title: What Love Makes Me Do
      artists:
        - reSet
      artist: reSet
      album: Santas Audioserver
      genre:
        - Electro
      label:
        - Villa Magica Records
      year: 2004
    transformed:
      ID3v2.3:
        TIT2: What Love Makes Me Do
        TPE1: reSet
        TALB: Santas Audioserver
        TRCK: '1'
        TCON: Electro
        TPUB: Villa Magica Records
        TYER: '2004'
    all:
      TIT2: What Love Makes Me Do
      TPE1: reSet
      TALB: Santas Audioserver
      TRCK: '1'
      TCON: Electro
      TPUB: Villa Magica Records
      TYER: '2004'
  - path: >-
      /mnt/usb32gb/organized-sakrecoer.media/assets/albums/santas-audioserver/2-allein-in-das-all.mp3
    audio: /assets/albums/santas-audioserver/2-allein-in-das-all.mp3
    slug: santas-audioserver/2-allein-in-das-all
    albumSlug: santas-audioserver
    trackSlug: 2-allein-in-das-all
    coverPath: >-
      /mnt/usb32gb/organized-sakrecoer.media/assets/albums/santas-audioserver/2-allein-in-das-all.jpeg
    cover: /assets/albums/santas-audioserver/2-allein-in-das-all.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 2
      bitrate: 128000
      codecProfile: CBR
      numberOfSamples: 7455744
      duration: 169.06448979591838
    native:
      ID3v2.3:
        - id: TIT2
          value: Allein In Das All
        - id: TPE1
          value: reSet
        - id: TALB
          value: Santas Audioserver
        - id: TRCK
          value: '2'
        - id: TCON
          value: Electro
        - id: TPUB
          value: Villa Magica Records
        - id: TYER
          value: '2004'
    quality:
      warnings: []
    common:
      track:
        'no': 2
        of: null
      disk:
        'no': null
        of: null
      title: Allein In Das All
      artists:
        - reSet
      artist: reSet
      album: Santas Audioserver
      genre:
        - Electro
      label:
        - Villa Magica Records
      year: 2004
    transformed:
      ID3v2.3:
        TIT2: Allein In Das All
        TPE1: reSet
        TALB: Santas Audioserver
        TRCK: '2'
        TCON: Electro
        TPUB: Villa Magica Records
        TYER: '2004'
    all:
      TIT2: Allein In Das All
      TPE1: reSet
      TALB: Santas Audioserver
      TRCK: '2'
      TCON: Electro
      TPUB: Villa Magica Records
      TYER: '2004'
  - path: >-
      /mnt/usb32gb/organized-sakrecoer.media/assets/albums/santas-audioserver/3-you-and-me.mp3
    audio: /assets/albums/santas-audioserver/3-you-and-me.mp3
    slug: santas-audioserver/3-you-and-me
    albumSlug: santas-audioserver
    trackSlug: 3-you-and-me
    coverPath: >-
      /mnt/usb32gb/organized-sakrecoer.media/assets/albums/santas-audioserver/3-you-and-me.jpeg
    cover: /assets/albums/santas-audioserver/3-you-and-me.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 2
      bitrate: 128000
      codecProfile: CBR
      numberOfSamples: 10666368
      duration: 241.8677551020408
    native:
      ID3v2.3:
        - id: TIT2
          value: You And Me
        - id: TPE1
          value: reSet
        - id: TALB
          value: Santas Audioserver
        - id: TRCK
          value: '3'
        - id: TCON
          value: Electro
        - id: TPUB
          value: Villa Magica Records
        - id: TYER
          value: '2004'
    quality:
      warnings: []
    common:
      track:
        'no': 3
        of: null
      disk:
        'no': null
        of: null
      title: You And Me
      artists:
        - reSet
      artist: reSet
      album: Santas Audioserver
      genre:
        - Electro
      label:
        - Villa Magica Records
      year: 2004
    transformed:
      ID3v2.3:
        TIT2: You And Me
        TPE1: reSet
        TALB: Santas Audioserver
        TRCK: '3'
        TCON: Electro
        TPUB: Villa Magica Records
        TYER: '2004'
    all:
      TIT2: You And Me
      TPE1: reSet
      TALB: Santas Audioserver
      TRCK: '3'
      TCON: Electro
      TPUB: Villa Magica Records
      TYER: '2004'
  - path: >-
      /mnt/usb32gb/organized-sakrecoer.media/assets/albums/santas-audioserver/4-you-and-me-ii.mp3
    audio: /assets/albums/santas-audioserver/4-you-and-me-ii.mp3
    slug: santas-audioserver/4-you-and-me-ii
    albumSlug: santas-audioserver
    trackSlug: 4-you-and-me-ii
    coverPath: >-
      /mnt/usb32gb/organized-sakrecoer.media/assets/albums/santas-audioserver/4-you-and-me-ii.jpeg
    cover: /assets/albums/santas-audioserver/4-you-and-me-ii.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 2
      bitrate: 128000
      codecProfile: CBR
      numberOfSamples: 10688256
      duration: 242.36408163265307
    native:
      ID3v2.3:
        - id: TIT2
          value: You And Me II
        - id: TPE1
          value: reSet
        - id: TALB
          value: Santas Audioserver
        - id: TRCK
          value: '4'
        - id: TCON
          value: Electro
        - id: TPUB
          value: Villa Magica Records
        - id: TYER
          value: '2004'
    quality:
      warnings: []
    common:
      track:
        'no': 4
        of: null
      disk:
        'no': null
        of: null
      title: You And Me II
      artists:
        - reSet
      artist: reSet
      album: Santas Audioserver
      genre:
        - Electro
      label:
        - Villa Magica Records
      year: 2004
    transformed:
      ID3v2.3:
        TIT2: You And Me II
        TPE1: reSet
        TALB: Santas Audioserver
        TRCK: '4'
        TCON: Electro
        TPUB: Villa Magica Records
        TYER: '2004'
    all:
      TIT2: You And Me II
      TPE1: reSet
      TALB: Santas Audioserver
      TRCK: '4'
      TCON: Electro
      TPUB: Villa Magica Records
      TYER: '2004'
  - path: >-
      /mnt/usb32gb/organized-sakrecoer.media/assets/albums/santas-audioserver/5-not-pissed-happy.mp3
    audio: /assets/albums/santas-audioserver/5-not-pissed-happy.mp3
    slug: santas-audioserver/5-not-pissed-happy
    albumSlug: santas-audioserver
    trackSlug: 5-not-pissed-happy
    coverPath: >-
      /mnt/usb32gb/organized-sakrecoer.media/assets/albums/santas-audioserver/5-not-pissed-happy.jpeg
    cover: /assets/albums/santas-audioserver/5-not-pissed-happy.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 2
      bitrate: 128000
      codecProfile: CBR
      numberOfSamples: 10251648
      duration: 232.46367346938774
    native:
      ID3v2.3:
        - id: TIT2
          value: 'Not Pissed, Happy'
        - id: TPE1
          value: reSet
        - id: TALB
          value: Santas Audioserver
        - id: TRCK
          value: '5'
        - id: TCON
          value: Electro
        - id: TPUB
          value: Villa Magica Records
        - id: TYER
          value: '2004'
    quality:
      warnings: []
    common:
      track:
        'no': 5
        of: null
      disk:
        'no': null
        of: null
      title: 'Not Pissed, Happy'
      artists:
        - reSet
      artist: reSet
      album: Santas Audioserver
      genre:
        - Electro
      label:
        - Villa Magica Records
      year: 2004
    transformed:
      ID3v2.3:
        TIT2: 'Not Pissed, Happy'
        TPE1: reSet
        TALB: Santas Audioserver
        TRCK: '5'
        TCON: Electro
        TPUB: Villa Magica Records
        TYER: '2004'
    all:
      TIT2: 'Not Pissed, Happy'
      TPE1: reSet
      TALB: Santas Audioserver
      TRCK: '5'
      TCON: Electro
      TPUB: Villa Magica Records
      TYER: '2004'
  - path: >-
      /mnt/usb32gb/organized-sakrecoer.media/assets/albums/santas-audioserver/6-dance-you.mp3
    audio: /assets/albums/santas-audioserver/6-dance-you.mp3
    slug: santas-audioserver/6-dance-you
    albumSlug: santas-audioserver
    trackSlug: 6-dance-you
    coverPath: >-
      /mnt/usb32gb/organized-sakrecoer.media/assets/albums/santas-audioserver/6-dance-you.jpeg
    cover: /assets/albums/santas-audioserver/6-dance-you.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 2
      bitrate: 128000
      codecProfile: CBR
      numberOfSamples: 7276032
      duration: 164.98938775510203
    native:
      ID3v2.3:
        - id: TIT2
          value: Dance You
        - id: TPE1
          value: reSet
        - id: TALB
          value: Santas Audioserver
        - id: TRCK
          value: '6'
        - id: TCON
          value: Electro
        - id: TPUB
          value: Villa Magica Records
        - id: TYER
          value: '2004'
    quality:
      warnings: []
    common:
      track:
        'no': 6
        of: null
      disk:
        'no': null
        of: null
      title: Dance You
      artists:
        - reSet
      artist: reSet
      album: Santas Audioserver
      genre:
        - Electro
      label:
        - Villa Magica Records
      year: 2004
    transformed:
      ID3v2.3:
        TIT2: Dance You
        TPE1: reSet
        TALB: Santas Audioserver
        TRCK: '6'
        TCON: Electro
        TPUB: Villa Magica Records
        TYER: '2004'
    all:
      TIT2: Dance You
      TPE1: reSet
      TALB: Santas Audioserver
      TRCK: '6'
      TCON: Electro
      TPUB: Villa Magica Records
      TYER: '2004'
  - path: >-
      /mnt/usb32gb/organized-sakrecoer.media/assets/albums/santas-audioserver/7-what-love-makes-me-do-bad-ass-mix-.mp3
    audio: /assets/albums/santas-audioserver/7-what-love-makes-me-do-bad-ass-mix-.mp3
    slug: santas-audioserver/7-what-love-makes-me-do-bad-ass-mix-
    albumSlug: santas-audioserver
    trackSlug: 7-what-love-makes-me-do-bad-ass-mix-
    coverPath: >-
      /mnt/usb32gb/organized-sakrecoer.media/assets/albums/santas-audioserver/7-what-love-makes-me-do-bad-ass-mix-.jpeg
    cover: >-
      /assets/albums/santas-audioserver/7-what-love-makes-me-do-bad-ass-mix-.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 2
      bitrate: 128000
      codecProfile: CBR
      numberOfSamples: 12503808
      duration: 283.5330612244898
    native:
      ID3v2.3:
        - id: TIT2
          value: What Love Makes Me Do (Bad Ass Mix)
        - id: TPE1
          value: reSet
        - id: TALB
          value: Santas Audioserver
        - id: TRCK
          value: '7'
        - id: TCON
          value: Electro
        - id: TPUB
          value: Villa Magica Records
        - id: TYER
          value: '2004'
    quality:
      warnings: []
    common:
      track:
        'no': 7
        of: null
      disk:
        'no': null
        of: null
      title: What Love Makes Me Do (Bad Ass Mix)
      artists:
        - reSet
      artist: reSet
      album: Santas Audioserver
      genre:
        - Electro
      label:
        - Villa Magica Records
      year: 2004
    transformed:
      ID3v2.3:
        TIT2: What Love Makes Me Do (Bad Ass Mix)
        TPE1: reSet
        TALB: Santas Audioserver
        TRCK: '7'
        TCON: Electro
        TPUB: Villa Magica Records
        TYER: '2004'
    all:
      TIT2: What Love Makes Me Do (Bad Ass Mix)
      TPE1: reSet
      TALB: Santas Audioserver
      TRCK: '7'
      TCON: Electro
      TPUB: Villa Magica Records
      TYER: '2004'
  - path: >-
      /mnt/usb32gb/organized-sakrecoer.media/assets/albums/santas-audioserver/8-glaub-kein-wort.mp3
    audio: /assets/albums/santas-audioserver/8-glaub-kein-wort.mp3
    slug: santas-audioserver/8-glaub-kein-wort
    albumSlug: santas-audioserver
    trackSlug: 8-glaub-kein-wort
    coverPath: >-
      /mnt/usb32gb/organized-sakrecoer.media/assets/albums/santas-audioserver/8-glaub-kein-wort.jpeg
    cover: /assets/albums/santas-audioserver/8-glaub-kein-wort.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 2
      bitrate: 128000
      codecProfile: CBR
      numberOfSamples: 12769920
      duration: 289.56734693877553
    native:
      ID3v2.3:
        - id: TIT2
          value: Glaub Kein Wort
        - id: TPE1
          value: reSet
        - id: TALB
          value: Santas Audioserver
        - id: TRCK
          value: '8'
        - id: TCON
          value: Electro
        - id: TPUB
          value: Villa Magica Records
        - id: TYER
          value: '2004'
    quality:
      warnings: []
    common:
      track:
        'no': 8
        of: null
      disk:
        'no': null
        of: null
      title: Glaub Kein Wort
      artists:
        - reSet
      artist: reSet
      album: Santas Audioserver
      genre:
        - Electro
      label:
        - Villa Magica Records
      year: 2004
    transformed:
      ID3v2.3:
        TIT2: Glaub Kein Wort
        TPE1: reSet
        TALB: Santas Audioserver
        TRCK: '8'
        TCON: Electro
        TPUB: Villa Magica Records
        TYER: '2004'
    all:
      TIT2: Glaub Kein Wort
      TPE1: reSet
      TALB: Santas Audioserver
      TRCK: '8'
      TCON: Electro
      TPUB: Villa Magica Records
      TYER: '2004'
  - path: >-
      /mnt/usb32gb/organized-sakrecoer.media/assets/albums/santas-audioserver/9-liar.mp3
    audio: /assets/albums/santas-audioserver/9-liar.mp3
    slug: santas-audioserver/9-liar
    albumSlug: santas-audioserver
    trackSlug: 9-liar
    coverPath: >-
      /mnt/usb32gb/organized-sakrecoer.media/assets/albums/santas-audioserver/9-liar.jpeg
    cover: /assets/albums/santas-audioserver/9-liar.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 2
      bitrate: 128000
      codecProfile: CBR
      numberOfSamples: 12433536
      duration: 281.9395918367347
    native:
      ID3v2.3:
        - id: TIT2
          value: Liar
        - id: TPE1
          value: reSet
        - id: TALB
          value: Santas Audioserver
        - id: TRCK
          value: '9'
        - id: TCON
          value: Electro
        - id: TPUB
          value: Villa Magica Records
        - id: TYER
          value: '2004'
    quality:
      warnings: []
    common:
      track:
        'no': 9
        of: null
      disk:
        'no': null
        of: null
      title: Liar
      artists:
        - reSet
      artist: reSet
      album: Santas Audioserver
      genre:
        - Electro
      label:
        - Villa Magica Records
      year: 2004
    transformed:
      ID3v2.3:
        TIT2: Liar
        TPE1: reSet
        TALB: Santas Audioserver
        TRCK: '9'
        TCON: Electro
        TPUB: Villa Magica Records
        TYER: '2004'
    all:
      TIT2: Liar
      TPE1: reSet
      TALB: Santas Audioserver
      TRCK: '9'
      TCON: Electro
      TPUB: Villa Magica Records
      TYER: '2004'
  - path: >-
      /mnt/usb32gb/organized-sakrecoer.media/assets/albums/santas-audioserver/10-santa-simulator.mp3
    audio: /assets/albums/santas-audioserver/10-santa-simulator.mp3
    slug: santas-audioserver/10-santa-simulator
    albumSlug: santas-audioserver
    trackSlug: 10-santa-simulator
    coverPath: >-
      /mnt/usb32gb/organized-sakrecoer.media/assets/albums/santas-audioserver/10-santa-simulator.jpeg
    cover: /assets/albums/santas-audioserver/10-santa-simulator.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 2
      bitrate: 128000
      codecProfile: CBR
      numberOfSamples: 10356480
      duration: 234.84081632653061
    native:
      ID3v2.3:
        - id: TIT2
          value: Santa Simulator
        - id: TPE1
          value: reSet
        - id: TALB
          value: Santas Audioserver
        - id: TRCK
          value: '10'
        - id: TCON
          value: Electro
        - id: TPUB
          value: Villa Magica Records
        - id: TYER
          value: '2004'
    quality:
      warnings: []
    common:
      track:
        'no': 10
        of: null
      disk:
        'no': null
        of: null
      title: Santa Simulator
      artists:
        - reSet
      artist: reSet
      album: Santas Audioserver
      genre:
        - Electro
      label:
        - Villa Magica Records
      year: 2004
    transformed:
      ID3v2.3:
        TIT2: Santa Simulator
        TPE1: reSet
        TALB: Santas Audioserver
        TRCK: '10'
        TCON: Electro
        TPUB: Villa Magica Records
        TYER: '2004'
    all:
      TIT2: Santa Simulator
      TPE1: reSet
      TALB: Santas Audioserver
      TRCK: '10'
      TCON: Electro
      TPUB: Villa Magica Records
      TYER: '2004'
  - path: >-
      /mnt/usb32gb/organized-sakrecoer.media/assets/albums/santas-audioserver/11-tracker-upfront.mp3
    audio: /assets/albums/santas-audioserver/11-tracker-upfront.mp3
    slug: santas-audioserver/11-tracker-upfront
    albumSlug: santas-audioserver
    trackSlug: 11-tracker-upfront
    coverPath: >-
      /mnt/usb32gb/organized-sakrecoer.media/assets/albums/santas-audioserver/11-tracker-upfront.jpeg
    cover: /assets/albums/santas-audioserver/11-tracker-upfront.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 2
      bitrate: 128000
      codecProfile: CBR
      numberOfSamples: 20098944
      duration: 455.75836734693877
    native:
      ID3v2.3:
        - id: TIT2
          value: Tracker Upfront
        - id: TPE1
          value: reSet
        - id: TALB
          value: Santas Audioserver
        - id: TRCK
          value: '11'
        - id: TCON
          value: Electro
        - id: TPUB
          value: Villa Magica Records
        - id: TYER
          value: '2004'
    quality:
      warnings: []
    common:
      track:
        'no': 11
        of: null
      disk:
        'no': null
        of: null
      title: Tracker Upfront
      artists:
        - reSet
      artist: reSet
      album: Santas Audioserver
      genre:
        - Electro
      label:
        - Villa Magica Records
      year: 2004
    transformed:
      ID3v2.3:
        TIT2: Tracker Upfront
        TPE1: reSet
        TALB: Santas Audioserver
        TRCK: '11'
        TCON: Electro
        TPUB: Villa Magica Records
        TYER: '2004'
    all:
      TIT2: Tracker Upfront
      TPE1: reSet
      TALB: Santas Audioserver
      TRCK: '11'
      TCON: Electro
      TPUB: Villa Magica Records
      TYER: '2004'
  - path: >-
      /mnt/usb32gb/organized-sakrecoer.media/assets/albums/santas-audioserver/12-what-love-makes-me-do-codein-mix-.mp3
    audio: /assets/albums/santas-audioserver/12-what-love-makes-me-do-codein-mix-.mp3
    slug: santas-audioserver/12-what-love-makes-me-do-codein-mix-
    albumSlug: santas-audioserver
    trackSlug: 12-what-love-makes-me-do-codein-mix-
    coverPath: >-
      /mnt/usb32gb/organized-sakrecoer.media/assets/albums/santas-audioserver/12-what-love-makes-me-do-codein-mix-.jpeg
    cover: >-
      /assets/albums/santas-audioserver/12-what-love-makes-me-do-codein-mix-.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 2
      bitrate: 128000
      codecProfile: CBR
      numberOfSamples: 15485184
      duration: 351.13795918367344
    native:
      ID3v2.3:
        - id: TIT2
          value: What Love Makes Me Do (Codein Mix)
        - id: TPE1
          value: reSet
        - id: TALB
          value: Santas Audioserver
        - id: TRCK
          value: '12'
        - id: TCON
          value: Electro
        - id: TPUB
          value: Villa Magica Records
        - id: TYER
          value: '2004'
    quality:
      warnings: []
    common:
      track:
        'no': 12
        of: null
      disk:
        'no': null
        of: null
      title: What Love Makes Me Do (Codein Mix)
      artists:
        - reSet
      artist: reSet
      album: Santas Audioserver
      genre:
        - Electro
      label:
        - Villa Magica Records
      year: 2004
    transformed:
      ID3v2.3:
        TIT2: What Love Makes Me Do (Codein Mix)
        TPE1: reSet
        TALB: Santas Audioserver
        TRCK: '12'
        TCON: Electro
        TPUB: Villa Magica Records
        TYER: '2004'
    all:
      TIT2: What Love Makes Me Do (Codein Mix)
      TPE1: reSet
      TALB: Santas Audioserver
      TRCK: '12'
      TCON: Electro
      TPUB: Villa Magica Records
      TYER: '2004'
---
