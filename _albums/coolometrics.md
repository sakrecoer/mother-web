---
layout: album
slug: coolometrics
name: Coolometrics
artists: reSet Sakrecoer
bitrate: 128000
trackCount: 19
cover: /assets/albums/coolometrics/1-sis-n-pussy.jpeg
date: 2005-1-1
tracks:
  - path: >-
      /mnt/usb32gb/organized-sakrecoer.media/assets/albums/coolometrics/1-sis-n-pussy.mp3
    audio: /assets/albums/coolometrics/1-sis-n-pussy.mp3
    slug: coolometrics/1-sis-n-pussy
    albumSlug: coolometrics
    trackSlug: 1-sis-n-pussy
    coverPath: >-
      /mnt/usb32gb/organized-sakrecoer.media/assets/albums/coolometrics/1-sis-n-pussy.jpeg
    cover: /assets/albums/coolometrics/1-sis-n-pussy.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 2
      bitrate: 128000
      codecProfile: CBR
      numberOfSamples: 6271488
      duration: 142.21061224489796
    native:
      ID3v2.3:
        - id: TIT2
          value: Sis'n'Pussy
        - id: TPE1
          value: reSet Sakrecoer
        - id: TALB
          value: Coolometrics
        - id: TRCK
          value: '1'
        - id: TCON
          value: Pop
        - id: TYER
          value: '2005'
    quality:
      warnings: []
    common:
      track:
        'no': 1
        of: null
      disk:
        'no': null
        of: null
      title: Sis'n'Pussy
      artists:
        - reSet Sakrecoer
      artist: reSet Sakrecoer
      album: Coolometrics
      genre:
        - Pop
      year: 2005
    transformed:
      ID3v2.3:
        TIT2: Sis'n'Pussy
        TPE1: reSet Sakrecoer
        TALB: Coolometrics
        TRCK: '1'
        TCON: Pop
        TYER: '2005'
    all:
      TIT2: Sis'n'Pussy
      TPE1: reSet Sakrecoer
      TALB: Coolometrics
      TRCK: '1'
      TCON: Pop
      TYER: '2005'
  - path: >-
      /mnt/usb32gb/organized-sakrecoer.media/assets/albums/coolometrics/2-computers-kill.mp3
    audio: /assets/albums/coolometrics/2-computers-kill.mp3
    slug: coolometrics/2-computers-kill
    albumSlug: coolometrics
    trackSlug: 2-computers-kill
    coverPath: >-
      /mnt/usb32gb/organized-sakrecoer.media/assets/albums/coolometrics/2-computers-kill.jpeg
    cover: /assets/albums/coolometrics/2-computers-kill.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 2
      bitrate: 128000
      codecProfile: CBR
      numberOfSamples: 9440640
      duration: 214.0734693877551
    native:
      ID3v2.3:
        - id: TPE1
          value: reSet Sakrecoer
        - id: TALB
          value: Coolometrics
        - id: TCON
          value: Disco
        - id: COMM
          value:
            language: XXX
            description: Comment
            text: creative common licensed
        - id: TIT2
          value: Computers Kill
        - id: TRCK
          value: '2'
        - id: TYER
          value: '2005'
    quality:
      warnings: []
    common:
      track:
        'no': 2
        of: null
      disk:
        'no': null
        of: null
      artists:
        - reSet Sakrecoer
      artist: reSet Sakrecoer
      album: Coolometrics
      genre:
        - Disco
      comment:
        - creative common licensed
      title: Computers Kill
      year: 2005
    transformed:
      ID3v2.3:
        TPE1: reSet Sakrecoer
        TALB: Coolometrics
        TCON: Disco
        Comment: creative common licensed
        TIT2: Computers Kill
        TRCK: '2'
        TYER: '2005'
    all:
      TPE1: reSet Sakrecoer
      TALB: Coolometrics
      TCON: Disco
      Comment: creative common licensed
      TIT2: Computers Kill
      TRCK: '2'
      TYER: '2005'
  - path: >-
      /mnt/usb32gb/organized-sakrecoer.media/assets/albums/coolometrics/3-truning-thing-feat-muji.mp3
    audio: /assets/albums/coolometrics/3-truning-thing-feat-muji.mp3
    slug: coolometrics/3-truning-thing-feat-muji
    albumSlug: coolometrics
    trackSlug: 3-truning-thing-feat-muji
    coverPath: >-
      /mnt/usb32gb/organized-sakrecoer.media/assets/albums/coolometrics/3-truning-thing-feat-muji.jpeg
    cover: /assets/albums/coolometrics/3-truning-thing-feat-muji.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 2
      bitrate: 128000
      codecProfile: CBR
      numberOfSamples: 8704512
      duration: 197.38122448979593
    native:
      ID3v2.3:
        - id: TIT2
          value: Truning Thing Feat. Muji
        - id: TPE1
          value: reSet Sakrecoer
        - id: TALB
          value: Coolometrics
        - id: TRCK
          value: '3'
        - id: TCON
          value: Lo-Fi
        - id: TYER
          value: '2005'
    quality:
      warnings: []
    common:
      track:
        'no': 3
        of: null
      disk:
        'no': null
        of: null
      title: Truning Thing Feat. Muji
      artists:
        - reSet Sakrecoer
      artist: reSet Sakrecoer
      album: Coolometrics
      genre:
        - Lo-Fi
      year: 2005
    transformed:
      ID3v2.3:
        TIT2: Truning Thing Feat. Muji
        TPE1: reSet Sakrecoer
        TALB: Coolometrics
        TRCK: '3'
        TCON: Lo-Fi
        TYER: '2005'
    all:
      TIT2: Truning Thing Feat. Muji
      TPE1: reSet Sakrecoer
      TALB: Coolometrics
      TRCK: '3'
      TCON: Lo-Fi
      TYER: '2005'
  - path: >-
      /mnt/usb32gb/organized-sakrecoer.media/assets/albums/coolometrics/4-abject.mp3
    audio: /assets/albums/coolometrics/4-abject.mp3
    slug: coolometrics/4-abject
    albumSlug: coolometrics
    trackSlug: 4-abject
    coverPath: >-
      /mnt/usb32gb/organized-sakrecoer.media/assets/albums/coolometrics/4-abject.jpeg
    cover: /assets/albums/coolometrics/4-abject.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 2
      bitrate: 128000
      tool: LAME 3.100U
      codecProfile: CBR
      numberOfSamples: 13418496
      duration: 304.2742857142857
    native:
      ID3v2.3:
        - id: TPE1
          value: reSet Sakrecoer
        - id: TIT2
          value: Abject
        - id: TALB
          value: Coolometrics
        - id: TRCK
          value: '4'
        - id: TCON
          value: Disco
        - id: TYER
          value: '2005'
    quality:
      warnings: []
    common:
      track:
        'no': 4
        of: null
      disk:
        'no': null
        of: null
      artists:
        - reSet Sakrecoer
      artist: reSet Sakrecoer
      title: Abject
      album: Coolometrics
      genre:
        - Disco
      year: 2005
    transformed:
      ID3v2.3:
        TPE1: reSet Sakrecoer
        TIT2: Abject
        TALB: Coolometrics
        TRCK: '4'
        TCON: Disco
        TYER: '2005'
    all:
      TPE1: reSet Sakrecoer
      TIT2: Abject
      TALB: Coolometrics
      TRCK: '4'
      TCON: Disco
      TYER: '2005'
  - path: >-
      /mnt/usb32gb/organized-sakrecoer.media/assets/albums/coolometrics/5-donwanastay-feat-peter-hageus.mp3
    audio: /assets/albums/coolometrics/5-donwanastay-feat-peter-hageus.mp3
    slug: coolometrics/5-donwanastay-feat-peter-hageus
    albumSlug: coolometrics
    trackSlug: 5-donwanastay-feat-peter-hageus
    coverPath: >-
      /mnt/usb32gb/organized-sakrecoer.media/assets/albums/coolometrics/5-donwanastay-feat-peter-hageus.jpeg
    cover: /assets/albums/coolometrics/5-donwanastay-feat-peter-hageus.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 2
      bitrate: 128000
      codecProfile: CBR
      numberOfSamples: 7940736
      duration: 180.06204081632654
    native:
      ID3v2.3:
        - id: TIT2
          value: Donwanastay Feat. Peter Hageus
        - id: TPE1
          value: reSet Sakrecoer
        - id: TCON
          value: Drakpop
        - id: TALB
          value: Coolometrics
        - id: TRCK
          value: '5'
        - id: TYER
          value: '2005'
    quality:
      warnings: []
    common:
      track:
        'no': 5
        of: null
      disk:
        'no': null
        of: null
      title: Donwanastay Feat. Peter Hageus
      artists:
        - reSet Sakrecoer
      artist: reSet Sakrecoer
      genre:
        - Drakpop
      album: Coolometrics
      year: 2005
    transformed:
      ID3v2.3:
        TIT2: Donwanastay Feat. Peter Hageus
        TPE1: reSet Sakrecoer
        TCON: Drakpop
        TALB: Coolometrics
        TRCK: '5'
        TYER: '2005'
    all:
      TIT2: Donwanastay Feat. Peter Hageus
      TPE1: reSet Sakrecoer
      TCON: Drakpop
      TALB: Coolometrics
      TRCK: '5'
      TYER: '2005'
  - path: >-
      /mnt/usb32gb/organized-sakrecoer.media/assets/albums/coolometrics/6-sthlm-rave.mp3
    audio: /assets/albums/coolometrics/6-sthlm-rave.mp3
    slug: coolometrics/6-sthlm-rave
    albumSlug: coolometrics
    trackSlug: 6-sthlm-rave
    coverPath: >-
      /mnt/usb32gb/organized-sakrecoer.media/assets/albums/coolometrics/6-sthlm-rave.jpeg
    cover: /assets/albums/coolometrics/6-sthlm-rave.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 2
      bitrate: 128000
      codecProfile: CBR
      numberOfSamples: 7827840
      duration: 177.50204081632654
    native:
      ID3v2.3:
        - id: TIT2
          value: STHLM Rave
        - id: TPE1
          value: reSet Sakrecoer
        - id: TALB
          value: Coolometrics
        - id: TRCK
          value: '6'
        - id: TCON
          value: Lo-Fi
        - id: TYER
          value: '2005'
    quality:
      warnings: []
    common:
      track:
        'no': 6
        of: null
      disk:
        'no': null
        of: null
      title: STHLM Rave
      artists:
        - reSet Sakrecoer
      artist: reSet Sakrecoer
      album: Coolometrics
      genre:
        - Lo-Fi
      year: 2005
    transformed:
      ID3v2.3:
        TIT2: STHLM Rave
        TPE1: reSet Sakrecoer
        TALB: Coolometrics
        TRCK: '6'
        TCON: Lo-Fi
        TYER: '2005'
    all:
      TIT2: STHLM Rave
      TPE1: reSet Sakrecoer
      TALB: Coolometrics
      TRCK: '6'
      TCON: Lo-Fi
      TYER: '2005'
  - path: >-
      /mnt/usb32gb/organized-sakrecoer.media/assets/albums/coolometrics/7-i-hate-kidz-dieanna-remixed-.mp3
    audio: /assets/albums/coolometrics/7-i-hate-kidz-dieanna-remixed-.mp3
    slug: coolometrics/7-i-hate-kidz-dieanna-remixed-
    albumSlug: coolometrics
    trackSlug: 7-i-hate-kidz-dieanna-remixed-
    coverPath: >-
      /mnt/usb32gb/organized-sakrecoer.media/assets/albums/coolometrics/7-i-hate-kidz-dieanna-remixed-.jpeg
    cover: /assets/albums/coolometrics/7-i-hate-kidz-dieanna-remixed-.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 2
      bitrate: 128000
      tool: LAME 3.100U
      codecProfile: CBR
      numberOfSamples: 7803648
      duration: 176.9534693877551
    native:
      ID3v2.3:
        - id: TIT2
          value: I hate Kidz (DieAnna Remixed)
        - id: TPE1
          value: reSet Sakrecoer
        - id: TALB
          value: Coolometrics
        - id: TRCK
          value: '7'
        - id: TCON
          value: Punk
        - id: TYER
          value: '2005'
    quality:
      warnings: []
    common:
      track:
        'no': 7
        of: null
      disk:
        'no': null
        of: null
      title: I hate Kidz (DieAnna Remixed)
      artists:
        - reSet Sakrecoer
      artist: reSet Sakrecoer
      album: Coolometrics
      genre:
        - Punk
      year: 2005
    transformed:
      ID3v2.3:
        TIT2: I hate Kidz (DieAnna Remixed)
        TPE1: reSet Sakrecoer
        TALB: Coolometrics
        TRCK: '7'
        TCON: Punk
        TYER: '2005'
    all:
      TIT2: I hate Kidz (DieAnna Remixed)
      TPE1: reSet Sakrecoer
      TALB: Coolometrics
      TRCK: '7'
      TCON: Punk
      TYER: '2005'
  - path: >-
      /mnt/usb32gb/organized-sakrecoer.media/assets/albums/coolometrics/8-dr-mmar.mp3
    audio: /assets/albums/coolometrics/8-dr-mmar.mp3
    slug: coolometrics/8-dr-mmar
    albumSlug: coolometrics
    trackSlug: 8-dr-mmar
    coverPath: >-
      /mnt/usb32gb/organized-sakrecoer.media/assets/albums/coolometrics/8-dr-mmar.jpeg
    cover: /assets/albums/coolometrics/8-dr-mmar.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 2
      bitrate: 128000
      codecProfile: CBR
      numberOfSamples: 4983552
      duration: 113.00571428571429
    native:
      ID3v2.3:
        - id: TIT2
          value: Drömmar
        - id: TPE1
          value: reSet Sakrecoer
        - id: TALB
          value: Coolometrics
        - id: TRCK
          value: '8'
        - id: TCON
          value: Lo-Fi
        - id: TYER
          value: '2005'
    quality:
      warnings: []
    common:
      track:
        'no': 8
        of: null
      disk:
        'no': null
        of: null
      title: Drömmar
      artists:
        - reSet Sakrecoer
      artist: reSet Sakrecoer
      album: Coolometrics
      genre:
        - Lo-Fi
      year: 2005
    transformed:
      ID3v2.3:
        TIT2: Drömmar
        TPE1: reSet Sakrecoer
        TALB: Coolometrics
        TRCK: '8'
        TCON: Lo-Fi
        TYER: '2005'
    all:
      TIT2: Drömmar
      TPE1: reSet Sakrecoer
      TALB: Coolometrics
      TRCK: '8'
      TCON: Lo-Fi
      TYER: '2005'
  - path: >-
      /mnt/usb32gb/organized-sakrecoer.media/assets/albums/coolometrics/9-ordfilter.mp3
    audio: /assets/albums/coolometrics/9-ordfilter.mp3
    slug: coolometrics/9-ordfilter
    albumSlug: coolometrics
    trackSlug: 9-ordfilter
    coverPath: >-
      /mnt/usb32gb/organized-sakrecoer.media/assets/albums/coolometrics/9-ordfilter.jpeg
    cover: /assets/albums/coolometrics/9-ordfilter.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 2
      bitrate: 128000
      codecProfile: CBR
      numberOfSamples: 5109120
      duration: 115.85306122448979
    native:
      ID3v2.3:
        - id: TIT2
          value: Ordfilter
        - id: TPE1
          value: reSet Sakrecoer
        - id: TCON
          value: Swedish låfink
        - id: TALB
          value: Coolometrics
        - id: TRCK
          value: '9'
        - id: TYER
          value: '2005'
    quality:
      warnings: []
    common:
      track:
        'no': 9
        of: null
      disk:
        'no': null
        of: null
      title: Ordfilter
      artists:
        - reSet Sakrecoer
      artist: reSet Sakrecoer
      genre:
        - Swedish låfink
      album: Coolometrics
      year: 2005
    transformed:
      ID3v2.3:
        TIT2: Ordfilter
        TPE1: reSet Sakrecoer
        TCON: Swedish låfink
        TALB: Coolometrics
        TRCK: '9'
        TYER: '2005'
    all:
      TIT2: Ordfilter
      TPE1: reSet Sakrecoer
      TCON: Swedish låfink
      TALB: Coolometrics
      TRCK: '9'
      TYER: '2005'
  - path: >-
      /mnt/usb32gb/organized-sakrecoer.media/assets/albums/coolometrics/10-sms-poet.mp3
    audio: /assets/albums/coolometrics/10-sms-poet.mp3
    slug: coolometrics/10-sms-poet
    albumSlug: coolometrics
    trackSlug: 10-sms-poet
    coverPath: >-
      /mnt/usb32gb/organized-sakrecoer.media/assets/albums/coolometrics/10-sms-poet.jpeg
    cover: /assets/albums/coolometrics/10-sms-poet.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 2
      bitrate: 128000
      codecProfile: CBR
      numberOfSamples: 10012032
      duration: 227.03020408163266
    native:
      ID3v2.3:
        - id: TIT2
          value: SMS Poet
        - id: TPE1
          value: reSet Sakrecoer
        - id: TALB
          value: Coolometrics
        - id: TRCK
          value: '10'
        - id: TCON
          value: Pop
        - id: TYER
          value: '2005'
    quality:
      warnings: []
    common:
      track:
        'no': 10
        of: null
      disk:
        'no': null
        of: null
      title: SMS Poet
      artists:
        - reSet Sakrecoer
      artist: reSet Sakrecoer
      album: Coolometrics
      genre:
        - Pop
      year: 2005
    transformed:
      ID3v2.3:
        TIT2: SMS Poet
        TPE1: reSet Sakrecoer
        TALB: Coolometrics
        TRCK: '10'
        TCON: Pop
        TYER: '2005'
    all:
      TIT2: SMS Poet
      TPE1: reSet Sakrecoer
      TALB: Coolometrics
      TRCK: '10'
      TCON: Pop
      TYER: '2005'
  - path: >-
      /mnt/usb32gb/organized-sakrecoer.media/assets/albums/coolometrics/11-morning-scooper.mp3
    audio: /assets/albums/coolometrics/11-morning-scooper.mp3
    slug: coolometrics/11-morning-scooper
    albumSlug: coolometrics
    trackSlug: 11-morning-scooper
    coverPath: >-
      /mnt/usb32gb/organized-sakrecoer.media/assets/albums/coolometrics/11-morning-scooper.jpeg
    cover: /assets/albums/coolometrics/11-morning-scooper.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 2
      bitrate: 128000
      codecProfile: CBR
      numberOfSamples: 9517824
      duration: 215.82367346938776
    native:
      ID3v2.3:
        - id: TPE1
          value: reSet Sakrecoer
        - id: TIT2
          value: Morning Scooper
        - id: TALB
          value: Coolometrics
        - id: TRCK
          value: '11'
        - id: TCON
          value: Pop
        - id: TYER
          value: '2005'
    quality:
      warnings: []
    common:
      track:
        'no': 11
        of: null
      disk:
        'no': null
        of: null
      artists:
        - reSet Sakrecoer
      artist: reSet Sakrecoer
      title: Morning Scooper
      album: Coolometrics
      genre:
        - Pop
      year: 2005
    transformed:
      ID3v2.3:
        TPE1: reSet Sakrecoer
        TIT2: Morning Scooper
        TALB: Coolometrics
        TRCK: '11'
        TCON: Pop
        TYER: '2005'
    all:
      TPE1: reSet Sakrecoer
      TIT2: Morning Scooper
      TALB: Coolometrics
      TRCK: '11'
      TCON: Pop
      TYER: '2005'
  - path: >-
      /mnt/usb32gb/organized-sakrecoer.media/assets/albums/coolometrics/12-clap-clap.mp3
    audio: /assets/albums/coolometrics/12-clap-clap.mp3
    slug: coolometrics/12-clap-clap
    albumSlug: coolometrics
    trackSlug: 12-clap-clap
    coverPath: >-
      /mnt/usb32gb/organized-sakrecoer.media/assets/albums/coolometrics/12-clap-clap.jpeg
    cover: /assets/albums/coolometrics/12-clap-clap.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 2
      bitrate: 128000
      codecProfile: CBR
      numberOfSamples: 5874048
      duration: 133.19836734693877
    native:
      ID3v2.3:
        - id: TIT2
          value: Clap Clap
        - id: TPE1
          value: reSet Sakrecoer
        - id: TALB
          value: Coolometrics
        - id: TRCK
          value: '12'
        - id: TCON
          value: Lo-Fi
        - id: TYER
          value: '2005'
    quality:
      warnings: []
    common:
      track:
        'no': 12
        of: null
      disk:
        'no': null
        of: null
      title: Clap Clap
      artists:
        - reSet Sakrecoer
      artist: reSet Sakrecoer
      album: Coolometrics
      genre:
        - Lo-Fi
      year: 2005
    transformed:
      ID3v2.3:
        TIT2: Clap Clap
        TPE1: reSet Sakrecoer
        TALB: Coolometrics
        TRCK: '12'
        TCON: Lo-Fi
        TYER: '2005'
    all:
      TIT2: Clap Clap
      TPE1: reSet Sakrecoer
      TALB: Coolometrics
      TRCK: '12'
      TCON: Lo-Fi
      TYER: '2005'
  - path: >-
      /mnt/usb32gb/organized-sakrecoer.media/assets/albums/coolometrics/13-mes-sentiments-pour-toi.mp3
    audio: /assets/albums/coolometrics/13-mes-sentiments-pour-toi.mp3
    slug: coolometrics/13-mes-sentiments-pour-toi
    albumSlug: coolometrics
    trackSlug: 13-mes-sentiments-pour-toi
    coverPath: >-
      /mnt/usb32gb/organized-sakrecoer.media/assets/albums/coolometrics/13-mes-sentiments-pour-toi.jpeg
    cover: /assets/albums/coolometrics/13-mes-sentiments-pour-toi.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 2
      bitrate: 128000
      codecProfile: CBR
      numberOfSamples: 8839296
      duration: 200.43755102040816
    native:
      ID3v2.3:
        - id: TPE1
          value: reSet Sakrecoer
        - id: TALB
          value: Coolometrics
        - id: TCON
          value: Pop
        - id: COMM
          value:
            language: XXX
            description: Comment
            text: creative common licensed - gnashed records
        - id: TIT2
          value: Mes Sentiments Pour Toi
        - id: TRCK
          value: '13'
        - id: TYER
          value: '2005'
    quality:
      warnings: []
    common:
      track:
        'no': 13
        of: null
      disk:
        'no': null
        of: null
      artists:
        - reSet Sakrecoer
      artist: reSet Sakrecoer
      album: Coolometrics
      genre:
        - Pop
      comment:
        - creative common licensed - gnashed records
      title: Mes Sentiments Pour Toi
      year: 2005
    transformed:
      ID3v2.3:
        TPE1: reSet Sakrecoer
        TALB: Coolometrics
        TCON: Pop
        Comment: creative common licensed - gnashed records
        TIT2: Mes Sentiments Pour Toi
        TRCK: '13'
        TYER: '2005'
    all:
      TPE1: reSet Sakrecoer
      TALB: Coolometrics
      TCON: Pop
      Comment: creative common licensed - gnashed records
      TIT2: Mes Sentiments Pour Toi
      TRCK: '13'
      TYER: '2005'
  - path: >-
      /mnt/usb32gb/organized-sakrecoer.media/assets/albums/coolometrics/14-sn-lla-se-mig-feat-thickdick-arete-and-enapa.mp3
    audio: >-
      /assets/albums/coolometrics/14-sn-lla-se-mig-feat-thickdick-arete-and-enapa.mp3
    slug: coolometrics/14-sn-lla-se-mig-feat-thickdick-arete-and-enapa
    albumSlug: coolometrics
    trackSlug: 14-sn-lla-se-mig-feat-thickdick-arete-and-enapa
    coverPath: >-
      /mnt/usb32gb/organized-sakrecoer.media/assets/albums/coolometrics/14-sn-lla-se-mig-feat-thickdick-arete-and-enapa.jpeg
    cover: >-
      /assets/albums/coolometrics/14-sn-lla-se-mig-feat-thickdick-arete-and-enapa.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 2
      bitrate: 128000
      codecProfile: CBR
      numberOfSamples: 7226496
      duration: 163.86612244897958
    native:
      ID3v2.3:
        - id: TPE1
          value: reSet Sakrecoer
        - id: TBPM
          value: '126'
        - id: COMM
          value:
            language: XXX
            description: Comment
            text: 808 drum by thickdick various samples from Arete and EnApa
        - id: TCON
          value: Alternative Pop
        - id: TIT2
          value: 'Snälla Se Mig Feat. Thickdick, Arete and EnApa'
        - id: TALB
          value: Coolometrics
        - id: TRCK
          value: '14'
        - id: TYER
          value: '2005'
    quality:
      warnings: []
    common:
      track:
        'no': 14
        of: null
      disk:
        'no': null
        of: null
      artists:
        - reSet Sakrecoer
      artist: reSet Sakrecoer
      bpm: '126'
      comment:
        - 808 drum by thickdick various samples from Arete and EnApa
      genre:
        - Alternative Pop
      title: 'Snälla Se Mig Feat. Thickdick, Arete and EnApa'
      album: Coolometrics
      year: 2005
    transformed:
      ID3v2.3:
        TPE1: reSet Sakrecoer
        TBPM: '126'
        Comment: 808 drum by thickdick various samples from Arete and EnApa
        TCON: Alternative Pop
        TIT2: 'Snälla Se Mig Feat. Thickdick, Arete and EnApa'
        TALB: Coolometrics
        TRCK: '14'
        TYER: '2005'
    all:
      TPE1: reSet Sakrecoer
      TBPM: '126'
      Comment: 808 drum by thickdick various samples from Arete and EnApa
      TCON: Alternative Pop
      TIT2: 'Snälla Se Mig Feat. Thickdick, Arete and EnApa'
      TALB: Coolometrics
      TRCK: '14'
      TYER: '2005'
  - path: >-
      /mnt/usb32gb/organized-sakrecoer.media/assets/albums/coolometrics/15-exclamation-feat-arete.mp3
    audio: /assets/albums/coolometrics/15-exclamation-feat-arete.mp3
    slug: coolometrics/15-exclamation-feat-arete
    albumSlug: coolometrics
    trackSlug: 15-exclamation-feat-arete
    coverPath: >-
      /mnt/usb32gb/organized-sakrecoer.media/assets/albums/coolometrics/15-exclamation-feat-arete.jpeg
    cover: /assets/albums/coolometrics/15-exclamation-feat-arete.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 2
      bitrate: 128000
      codecProfile: CBR
      numberOfSamples: 7413120
      duration: 168.09795918367348
    native:
      ID3v2.3:
        - id: TIT2
          value: Exclamation Feat. Arete
        - id: TPE1
          value: reSet Sakrecoer
        - id: TALB
          value: Coolometrics
        - id: TRCK
          value: '15'
        - id: TCON
          value: Hippie Shit
        - id: TYER
          value: '2005'
    quality:
      warnings: []
    common:
      track:
        'no': 15
        of: null
      disk:
        'no': null
        of: null
      title: Exclamation Feat. Arete
      artists:
        - reSet Sakrecoer
      artist: reSet Sakrecoer
      album: Coolometrics
      genre:
        - Hippie Shit
      year: 2005
    transformed:
      ID3v2.3:
        TIT2: Exclamation Feat. Arete
        TPE1: reSet Sakrecoer
        TALB: Coolometrics
        TRCK: '15'
        TCON: Hippie Shit
        TYER: '2005'
    all:
      TIT2: Exclamation Feat. Arete
      TPE1: reSet Sakrecoer
      TALB: Coolometrics
      TRCK: '15'
      TCON: Hippie Shit
      TYER: '2005'
  - path: >-
      /mnt/usb32gb/organized-sakrecoer.media/assets/albums/coolometrics/16-tap-tap.mp3
    audio: /assets/albums/coolometrics/16-tap-tap.mp3
    slug: coolometrics/16-tap-tap
    albumSlug: coolometrics
    trackSlug: 16-tap-tap
    coverPath: >-
      /mnt/usb32gb/organized-sakrecoer.media/assets/albums/coolometrics/16-tap-tap.jpeg
    cover: /assets/albums/coolometrics/16-tap-tap.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 2
      bitrate: 128000
      codecProfile: CBR
      numberOfSamples: 4846464
      duration: 109.89714285714285
    native:
      ID3v2.3:
        - id: TIT2
          value: Tap Tap
        - id: TPE1
          value: reSet Sakrecoer
        - id: TALB
          value: Coolometrics
        - id: TRCK
          value: '16'
        - id: TCON
          value: Lo-Fi
        - id: TYER
          value: '2005'
    quality:
      warnings: []
    common:
      track:
        'no': 16
        of: null
      disk:
        'no': null
        of: null
      title: Tap Tap
      artists:
        - reSet Sakrecoer
      artist: reSet Sakrecoer
      album: Coolometrics
      genre:
        - Lo-Fi
      year: 2005
    transformed:
      ID3v2.3:
        TIT2: Tap Tap
        TPE1: reSet Sakrecoer
        TALB: Coolometrics
        TRCK: '16'
        TCON: Lo-Fi
        TYER: '2005'
    all:
      TIT2: Tap Tap
      TPE1: reSet Sakrecoer
      TALB: Coolometrics
      TRCK: '16'
      TCON: Lo-Fi
      TYER: '2005'
  - path: >-
      /mnt/usb32gb/organized-sakrecoer.media/assets/albums/coolometrics/17-battlezoo.mp3
    audio: /assets/albums/coolometrics/17-battlezoo.mp3
    slug: coolometrics/17-battlezoo
    albumSlug: coolometrics
    trackSlug: 17-battlezoo
    coverPath: >-
      /mnt/usb32gb/organized-sakrecoer.media/assets/albums/coolometrics/17-battlezoo.jpeg
    cover: /assets/albums/coolometrics/17-battlezoo.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 2
      bitrate: 128000
      codecProfile: CBR
      numberOfSamples: 10275840
      duration: 233.0122448979592
    native:
      ID3v2.3:
        - id: TIT2
          value: Battlezoo
        - id: TPE1
          value: reSet Sakrecoer
        - id: TALB
          value: Coolometrics
        - id: TCON
          value: Emo
        - id: TRCK
          value: '17'
        - id: TYER
          value: '2005'
    quality:
      warnings: []
    common:
      track:
        'no': 17
        of: null
      disk:
        'no': null
        of: null
      title: Battlezoo
      artists:
        - reSet Sakrecoer
      artist: reSet Sakrecoer
      album: Coolometrics
      genre:
        - Emo
      year: 2005
    transformed:
      ID3v2.3:
        TIT2: Battlezoo
        TPE1: reSet Sakrecoer
        TALB: Coolometrics
        TCON: Emo
        TRCK: '17'
        TYER: '2005'
    all:
      TIT2: Battlezoo
      TPE1: reSet Sakrecoer
      TALB: Coolometrics
      TCON: Emo
      TRCK: '17'
      TYER: '2005'
  - path: >-
      /mnt/usb32gb/organized-sakrecoer.media/assets/albums/coolometrics/18-love-for-fame.mp3
    audio: /assets/albums/coolometrics/18-love-for-fame.mp3
    slug: coolometrics/18-love-for-fame
    albumSlug: coolometrics
    trackSlug: 18-love-for-fame
    coverPath: >-
      /mnt/usb32gb/organized-sakrecoer.media/assets/albums/coolometrics/18-love-for-fame.jpeg
    cover: /assets/albums/coolometrics/18-love-for-fame.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 2
      bitrate: 128000
      codecProfile: CBR
      numberOfSamples: 13549824
      duration: 307.2522448979592
    native:
      ID3v2.3:
        - id: TPE1
          value: reSet Sakrecoer
        - id: TALB
          value: Coolometrics
        - id: TIT2
          value: Love For Fame
        - id: TRCK
          value: '18'
        - id: TCON
          value: Emo
        - id: TYER
          value: '2005'
    quality:
      warnings: []
    common:
      track:
        'no': 18
        of: null
      disk:
        'no': null
        of: null
      artists:
        - reSet Sakrecoer
      artist: reSet Sakrecoer
      album: Coolometrics
      title: Love For Fame
      genre:
        - Emo
      year: 2005
    transformed:
      ID3v2.3:
        TPE1: reSet Sakrecoer
        TALB: Coolometrics
        TIT2: Love For Fame
        TRCK: '18'
        TCON: Emo
        TYER: '2005'
    all:
      TPE1: reSet Sakrecoer
      TALB: Coolometrics
      TIT2: Love For Fame
      TRCK: '18'
      TCON: Emo
      TYER: '2005'
  - path: >-
      /mnt/usb32gb/organized-sakrecoer.media/assets/albums/coolometrics/19-la-douce-chanson-du-robot.mp3
    audio: /assets/albums/coolometrics/19-la-douce-chanson-du-robot.mp3
    slug: coolometrics/19-la-douce-chanson-du-robot
    albumSlug: coolometrics
    trackSlug: 19-la-douce-chanson-du-robot
    coverPath: >-
      /mnt/usb32gb/organized-sakrecoer.media/assets/albums/coolometrics/19-la-douce-chanson-du-robot.jpeg
    cover: /assets/albums/coolometrics/19-la-douce-chanson-du-robot.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 2
      bitrate: 128000
      codecProfile: CBR
      numberOfSamples: 7853184
      duration: 178.07673469387754
    native:
      ID3v2.3:
        - id: TIT2
          value: La Douce Chanson Du Robot
        - id: TPE1
          value: reSet Sakrecoer
        - id: TALB
          value: Coolometrics
        - id: TCON
          value: Crooner
        - id: TRCK
          value: '19'
        - id: TYER
          value: '2005'
    quality:
      warnings: []
    common:
      track:
        'no': 19
        of: null
      disk:
        'no': null
        of: null
      title: La Douce Chanson Du Robot
      artists:
        - reSet Sakrecoer
      artist: reSet Sakrecoer
      album: Coolometrics
      genre:
        - Crooner
      year: 2005
    transformed:
      ID3v2.3:
        TIT2: La Douce Chanson Du Robot
        TPE1: reSet Sakrecoer
        TALB: Coolometrics
        TCON: Crooner
        TRCK: '19'
        TYER: '2005'
    all:
      TIT2: La Douce Chanson Du Robot
      TPE1: reSet Sakrecoer
      TALB: Coolometrics
      TCON: Crooner
      TRCK: '19'
      TYER: '2005'
---
