---
layout: album
slug: the-lonely-hearts-club-techno-band
name: The Lonely Hearts Club Techno Band
artists: reSet Sakrecoer
bitrate: 128000
trackCount: 8
cover: /assets/albums/the-lonely-hearts-club-techno-band/1-fuck-dishit.jpeg
date: 2008-1-1
tracks:
  - path: >-
      /mnt/usb32gb/organized-sakrecoer.media/assets/albums/the-lonely-hearts-club-techno-band/1-fuck-dishit.mp3
    audio: /assets/albums/the-lonely-hearts-club-techno-band/1-fuck-dishit.mp3
    slug: the-lonely-hearts-club-techno-band/1-fuck-dishit
    albumSlug: the-lonely-hearts-club-techno-band
    trackSlug: 1-fuck-dishit
    coverPath: >-
      /mnt/usb32gb/organized-sakrecoer.media/assets/albums/the-lonely-hearts-club-techno-band/1-fuck-dishit.jpeg
    cover: /assets/albums/the-lonely-hearts-club-techno-band/1-fuck-dishit.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 2
      bitrate: 128000
      tool: LAME 3.100U
      codecProfile: CBR
      numberOfSamples: 15525504
      duration: 352.05224489795916
    native:
      ID3v2.3:
        - id: TPE1
          value: reSet Sakrecoer
        - id: TCON
          value: Techno
        - id: TIT2
          value: Fuck Dishit
        - id: COMM
          value: &ref_0
            language: eng
            description: ''
            text: Released on ArtifexBCN
        - id: TALB
          value: The Lonely Hearts Club Techno Band
        - id: TRCK
          value: '1'
        - id: TYER
          value: '2008'
    quality:
      warnings: []
    common:
      track:
        'no': 1
        of: null
      disk:
        'no': null
        of: null
      artists:
        - reSet Sakrecoer
      artist: reSet Sakrecoer
      genre:
        - Techno
      title: Fuck Dishit
      comment:
        - Released on ArtifexBCN
      album: The Lonely Hearts Club Techno Band
      year: 2008
    transformed:
      ID3v2.3:
        TPE1: reSet Sakrecoer
        TCON: Techno
        TIT2: Fuck Dishit
        COMM: *ref_0
        TALB: The Lonely Hearts Club Techno Band
        TRCK: '1'
        TYER: '2008'
    all:
      TPE1: reSet Sakrecoer
      TCON: Techno
      TIT2: Fuck Dishit
      COMM: *ref_0
      TALB: The Lonely Hearts Club Techno Band
      TRCK: '1'
      TYER: '2008'
  - path: >-
      /mnt/usb32gb/organized-sakrecoer.media/assets/albums/the-lonely-hearts-club-techno-band/2-birthday-voltures.mp3
    audio: /assets/albums/the-lonely-hearts-club-techno-band/2-birthday-voltures.mp3
    slug: the-lonely-hearts-club-techno-band/2-birthday-voltures
    albumSlug: the-lonely-hearts-club-techno-band
    trackSlug: 2-birthday-voltures
    coverPath: >-
      /mnt/usb32gb/organized-sakrecoer.media/assets/albums/the-lonely-hearts-club-techno-band/2-birthday-voltures.jpeg
    cover: /assets/albums/the-lonely-hearts-club-techno-band/2-birthday-voltures.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 2
      bitrate: 128000
      codecProfile: CBR
      numberOfSamples: 12245760
      duration: 277.68163265306123
    native:
      ID3v2.3:
        - id: TIT2
          value: Birthday Voltures
        - id: TPE1
          value: reSet Sakrecoer
        - id: TCON
          value: SynthPop
        - id: COMM
          value: &ref_1
            language: eng
            description: ''
            text: Released on ArtifexBCN
        - id: TALB
          value: The Lonely Hearts Club Techno Band
        - id: TRCK
          value: '2'
        - id: TYER
          value: '2008'
    quality:
      warnings: []
    common:
      track:
        'no': 2
        of: null
      disk:
        'no': null
        of: null
      title: Birthday Voltures
      artists:
        - reSet Sakrecoer
      artist: reSet Sakrecoer
      genre:
        - SynthPop
      comment:
        - Released on ArtifexBCN
      album: The Lonely Hearts Club Techno Band
      year: 2008
    transformed:
      ID3v2.3:
        TIT2: Birthday Voltures
        TPE1: reSet Sakrecoer
        TCON: SynthPop
        COMM: *ref_1
        TALB: The Lonely Hearts Club Techno Band
        TRCK: '2'
        TYER: '2008'
    all:
      TIT2: Birthday Voltures
      TPE1: reSet Sakrecoer
      TCON: SynthPop
      COMM: *ref_1
      TALB: The Lonely Hearts Club Techno Band
      TRCK: '2'
      TYER: '2008'
  - path: >-
      /mnt/usb32gb/organized-sakrecoer.media/assets/albums/the-lonely-hearts-club-techno-band/3-ya-eyes.mp3
    audio: /assets/albums/the-lonely-hearts-club-techno-band/3-ya-eyes.mp3
    slug: the-lonely-hearts-club-techno-band/3-ya-eyes
    albumSlug: the-lonely-hearts-club-techno-band
    trackSlug: 3-ya-eyes
    coverPath: >-
      /mnt/usb32gb/organized-sakrecoer.media/assets/albums/the-lonely-hearts-club-techno-band/3-ya-eyes.jpeg
    cover: /assets/albums/the-lonely-hearts-club-techno-band/3-ya-eyes.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 2
      bitrate: 128000
      codecProfile: CBR
      numberOfSamples: 11113344
      duration: 252.00326530612244
    native:
      ID3v2.3:
        - id: TPE1
          value: reSet Sakrecoer
        - id: TIT2
          value: Ya Eyes
        - id: COMM
          value: &ref_2
            language: eng
            description: ''
            text: Released on ArtifexBCN
        - id: TALB
          value: The Lonely Hearts Club Techno Band
        - id: TRCK
          value: '3'
        - id: TCON
          value: Electro
        - id: TYER
          value: '2008'
    quality:
      warnings: []
    common:
      track:
        'no': 3
        of: null
      disk:
        'no': null
        of: null
      artists:
        - reSet Sakrecoer
      artist: reSet Sakrecoer
      title: Ya Eyes
      comment:
        - Released on ArtifexBCN
      album: The Lonely Hearts Club Techno Band
      genre:
        - Electro
      year: 2008
    transformed:
      ID3v2.3:
        TPE1: reSet Sakrecoer
        TIT2: Ya Eyes
        COMM: *ref_2
        TALB: The Lonely Hearts Club Techno Band
        TRCK: '3'
        TCON: Electro
        TYER: '2008'
    all:
      TPE1: reSet Sakrecoer
      TIT2: Ya Eyes
      COMM: *ref_2
      TALB: The Lonely Hearts Club Techno Band
      TRCK: '3'
      TCON: Electro
      TYER: '2008'
  - path: >-
      /mnt/usb32gb/organized-sakrecoer.media/assets/albums/the-lonely-hearts-club-techno-band/4-love-de-ma-life.mp3
    audio: /assets/albums/the-lonely-hearts-club-techno-band/4-love-de-ma-life.mp3
    slug: the-lonely-hearts-club-techno-band/4-love-de-ma-life
    albumSlug: the-lonely-hearts-club-techno-band
    trackSlug: 4-love-de-ma-life
    coverPath: >-
      /mnt/usb32gb/organized-sakrecoer.media/assets/albums/the-lonely-hearts-club-techno-band/4-love-de-ma-life.jpeg
    cover: /assets/albums/the-lonely-hearts-club-techno-band/4-love-de-ma-life.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 2
      bitrate: 128000
      codecProfile: CBR
      numberOfSamples: 10584576
      duration: 240.0130612244898
    native:
      ID3v2.3:
        - id: TPE1
          value: reSet Sakrecoer
        - id: TCON
          value: Disco
        - id: TIT2
          value: Love De Ma Life
        - id: COMM
          value: &ref_3
            language: eng
            description: ''
            text: Released on ArtifexBCN
        - id: TALB
          value: The Lonely Hearts Club Techno Band
        - id: TRCK
          value: '4'
        - id: TYER
          value: '2008'
    quality:
      warnings: []
    common:
      track:
        'no': 4
        of: null
      disk:
        'no': null
        of: null
      artists:
        - reSet Sakrecoer
      artist: reSet Sakrecoer
      genre:
        - Disco
      title: Love De Ma Life
      comment:
        - Released on ArtifexBCN
      album: The Lonely Hearts Club Techno Band
      year: 2008
    transformed:
      ID3v2.3:
        TPE1: reSet Sakrecoer
        TCON: Disco
        TIT2: Love De Ma Life
        COMM: *ref_3
        TALB: The Lonely Hearts Club Techno Band
        TRCK: '4'
        TYER: '2008'
    all:
      TPE1: reSet Sakrecoer
      TCON: Disco
      TIT2: Love De Ma Life
      COMM: *ref_3
      TALB: The Lonely Hearts Club Techno Band
      TRCK: '4'
      TYER: '2008'
  - path: >-
      /mnt/usb32gb/organized-sakrecoer.media/assets/albums/the-lonely-hearts-club-techno-band/5-para-las-chicas.mp3
    audio: /assets/albums/the-lonely-hearts-club-techno-band/5-para-las-chicas.mp3
    slug: the-lonely-hearts-club-techno-band/5-para-las-chicas
    albumSlug: the-lonely-hearts-club-techno-band
    trackSlug: 5-para-las-chicas
    coverPath: >-
      /mnt/usb32gb/organized-sakrecoer.media/assets/albums/the-lonely-hearts-club-techno-band/5-para-las-chicas.jpeg
    cover: /assets/albums/the-lonely-hearts-club-techno-band/5-para-las-chicas.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 2
      bitrate: 128000
      codecProfile: CBR
      numberOfSamples: 13253760
      duration: 300.53877551020406
    native:
      ID3v2.3:
        - id: TIT2
          value: Para Las Chicas
        - id: TPE1
          value: reSet Sakrecoer
        - id: TCON
          value: Disco
        - id: COMM
          value: &ref_4
            language: eng
            description: ''
            text: Released on ArtifexBCN
        - id: TALB
          value: The Lonely Hearts Club Techno Band
        - id: TRCK
          value: '5'
        - id: TYER
          value: '2008'
    quality:
      warnings: []
    common:
      track:
        'no': 5
        of: null
      disk:
        'no': null
        of: null
      title: Para Las Chicas
      artists:
        - reSet Sakrecoer
      artist: reSet Sakrecoer
      genre:
        - Disco
      comment:
        - Released on ArtifexBCN
      album: The Lonely Hearts Club Techno Band
      year: 2008
    transformed:
      ID3v2.3:
        TIT2: Para Las Chicas
        TPE1: reSet Sakrecoer
        TCON: Disco
        COMM: *ref_4
        TALB: The Lonely Hearts Club Techno Band
        TRCK: '5'
        TYER: '2008'
    all:
      TIT2: Para Las Chicas
      TPE1: reSet Sakrecoer
      TCON: Disco
      COMM: *ref_4
      TALB: The Lonely Hearts Club Techno Band
      TRCK: '5'
      TYER: '2008'
  - path: >-
      /mnt/usb32gb/organized-sakrecoer.media/assets/albums/the-lonely-hearts-club-techno-band/6-weed-buddy.mp3
    audio: /assets/albums/the-lonely-hearts-club-techno-band/6-weed-buddy.mp3
    slug: the-lonely-hearts-club-techno-band/6-weed-buddy
    albumSlug: the-lonely-hearts-club-techno-band
    trackSlug: 6-weed-buddy
    coverPath: >-
      /mnt/usb32gb/organized-sakrecoer.media/assets/albums/the-lonely-hearts-club-techno-band/6-weed-buddy.jpeg
    cover: /assets/albums/the-lonely-hearts-club-techno-band/6-weed-buddy.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 2
      bitrate: 128000
      codecProfile: CBR
      numberOfSamples: 9970560
      duration: 226.08979591836734
    native:
      ID3v2.3:
        - id: TPE1
          value: reSet Sakrecoer
        - id: TCON
          value: Disco
        - id: TIT2
          value: Weed Buddy
        - id: COMM
          value: &ref_5
            language: eng
            description: ''
            text: Released on ArtifexBCN
        - id: TALB
          value: The Lonely Hearts Club Techno Band
        - id: TRCK
          value: '6'
        - id: TYER
          value: '2008'
    quality:
      warnings: []
    common:
      track:
        'no': 6
        of: null
      disk:
        'no': null
        of: null
      artists:
        - reSet Sakrecoer
      artist: reSet Sakrecoer
      genre:
        - Disco
      title: Weed Buddy
      comment:
        - Released on ArtifexBCN
      album: The Lonely Hearts Club Techno Band
      year: 2008
    transformed:
      ID3v2.3:
        TPE1: reSet Sakrecoer
        TCON: Disco
        TIT2: Weed Buddy
        COMM: *ref_5
        TALB: The Lonely Hearts Club Techno Band
        TRCK: '6'
        TYER: '2008'
    all:
      TPE1: reSet Sakrecoer
      TCON: Disco
      TIT2: Weed Buddy
      COMM: *ref_5
      TALB: The Lonely Hearts Club Techno Band
      TRCK: '6'
      TYER: '2008'
  - path: >-
      /mnt/usb32gb/organized-sakrecoer.media/assets/albums/the-lonely-hearts-club-techno-band/7-enfermo.mp3
    audio: /assets/albums/the-lonely-hearts-club-techno-band/7-enfermo.mp3
    slug: the-lonely-hearts-club-techno-band/7-enfermo
    albumSlug: the-lonely-hearts-club-techno-band
    trackSlug: 7-enfermo
    coverPath: >-
      /mnt/usb32gb/organized-sakrecoer.media/assets/albums/the-lonely-hearts-club-techno-band/7-enfermo.jpeg
    cover: /assets/albums/the-lonely-hearts-club-techno-band/7-enfermo.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 2
      bitrate: 128000
      codecProfile: CBR
      numberOfSamples: 14224896
      duration: 322.56
    native:
      ID3v2.3:
        - id: TPE1
          value: reSet Sakrecoer
        - id: TIT2
          value: Enfermo
        - id: COMM
          value: &ref_6
            language: eng
            description: ''
            text: Released on ArtifexBCN
        - id: TALB
          value: The Lonely Hearts Club Techno Band
        - id: TRCK
          value: '7'
        - id: TCON
          value: Electro
        - id: TYER
          value: '2008'
    quality:
      warnings: []
    common:
      track:
        'no': 7
        of: null
      disk:
        'no': null
        of: null
      artists:
        - reSet Sakrecoer
      artist: reSet Sakrecoer
      title: Enfermo
      comment:
        - Released on ArtifexBCN
      album: The Lonely Hearts Club Techno Band
      genre:
        - Electro
      year: 2008
    transformed:
      ID3v2.3:
        TPE1: reSet Sakrecoer
        TIT2: Enfermo
        COMM: *ref_6
        TALB: The Lonely Hearts Club Techno Band
        TRCK: '7'
        TCON: Electro
        TYER: '2008'
    all:
      TPE1: reSet Sakrecoer
      TIT2: Enfermo
      COMM: *ref_6
      TALB: The Lonely Hearts Club Techno Band
      TRCK: '7'
      TCON: Electro
      TYER: '2008'
  - path: >-
      /mnt/usb32gb/organized-sakrecoer.media/assets/albums/the-lonely-hearts-club-techno-band/8-bizarfeeling.mp3
    audio: /assets/albums/the-lonely-hearts-club-techno-band/8-bizarfeeling.mp3
    slug: the-lonely-hearts-club-techno-band/8-bizarfeeling
    albumSlug: the-lonely-hearts-club-techno-band
    trackSlug: 8-bizarfeeling
    coverPath: >-
      /mnt/usb32gb/organized-sakrecoer.media/assets/albums/the-lonely-hearts-club-techno-band/8-bizarfeeling.jpeg
    cover: /assets/albums/the-lonely-hearts-club-techno-band/8-bizarfeeling.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 2
      bitrate: 128000
      tool: LAME 3.100U
      codecProfile: CBR
      numberOfSamples: 9283968
      duration: 210.52081632653062
    native:
      ID3v2.3:
        - id: TPE1
          value: reSet Sakrecoer
        - id: TIT2
          value: Bizarfeeling
        - id: COMM
          value: &ref_7
            language: eng
            description: ''
            text: Released on ArtifexBCN
        - id: TALB
          value: The Lonely Hearts Club Techno Band
        - id: TRCK
          value: '8'
        - id: TCON
          value: Electro
        - id: TYER
          value: '2008'
    quality:
      warnings: []
    common:
      track:
        'no': 8
        of: null
      disk:
        'no': null
        of: null
      artists:
        - reSet Sakrecoer
      artist: reSet Sakrecoer
      title: Bizarfeeling
      comment:
        - Released on ArtifexBCN
      album: The Lonely Hearts Club Techno Band
      genre:
        - Electro
      year: 2008
    transformed:
      ID3v2.3:
        TPE1: reSet Sakrecoer
        TIT2: Bizarfeeling
        COMM: *ref_7
        TALB: The Lonely Hearts Club Techno Band
        TRCK: '8'
        TCON: Electro
        TYER: '2008'
    all:
      TPE1: reSet Sakrecoer
      TIT2: Bizarfeeling
      COMM: *ref_7
      TALB: The Lonely Hearts Club Techno Band
      TRCK: '8'
      TCON: Electro
      TYER: '2008'
---
