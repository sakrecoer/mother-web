---
layout: album
slug: vaporizer
name: Vaporizer
artists: Sakrecoer
bitrate:
  - 128000
  - 256000
trackCount: 4
cover: /assets/albums/vaporizer/1-99gf.jpeg
date: 2015-1-1
tracks:
  - path: /mnt/usb32gb/organized-sakrecoer.media/assets/albums/vaporizer/1-99gf.mp3
    audio: /assets/albums/vaporizer/1-99gf.mp3
    slug: vaporizer/1-99gf
    albumSlug: vaporizer
    trackSlug: 1-99gf
    coverPath: /mnt/usb32gb/organized-sakrecoer.media/assets/albums/vaporizer/1-99gf.jpeg
    cover: /assets/albums/vaporizer/1-99gf.jpeg
    format:
      tagTypes:
        - ID3v2.4
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 2
      bitrate: 128000
      codecProfile: CBR
      numberOfSamples: 12450816
      duration: 282.33142857142855
    native:
      ID3v2.4:
        - id: TRCK
          value: '1'
        - id: TDRC
          value: '2015'
        - id: TIT2
          value: 99GF
        - id: TPE1
          value: Sakrecoer
        - id: TALB
          value: Vaporizer
        - id: COMM
          value: &ref_0
            language: XXX
            description: ''
            text: sakrecoer.com
        - id: TCON
          value: Vapor Break
    quality:
      warnings: []
    common:
      track:
        'no': 1
        of: null
      disk:
        'no': null
        of: null
      year: 2015
      date: '2015'
      title: 99GF
      artists:
        - Sakrecoer
      artist: Sakrecoer
      album: Vaporizer
      comment:
        - sakrecoer.com
      genre:
        - Vapor Break
    transformed:
      ID3v2.4:
        TRCK: '1'
        TDRC: '2015'
        TIT2: 99GF
        TPE1: Sakrecoer
        TALB: Vaporizer
        COMM: *ref_0
        TCON: Vapor Break
    all:
      TRCK: '1'
      TDRC: '2015'
      TIT2: 99GF
      TPE1: Sakrecoer
      TALB: Vaporizer
      COMM: *ref_0
      TCON: Vapor Break
  - path: >-
      /mnt/usb32gb/organized-sakrecoer.media/assets/albums/vaporizer/2-7-stepper.mp3
    audio: /assets/albums/vaporizer/2-7-stepper.mp3
    slug: vaporizer/2-7-stepper
    albumSlug: vaporizer
    trackSlug: 2-7-stepper
    coverPath: >-
      /mnt/usb32gb/organized-sakrecoer.media/assets/albums/vaporizer/2-7-stepper.jpeg
    cover: /assets/albums/vaporizer/2-7-stepper.jpeg
    format:
      tagTypes:
        - ID3v2.4
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 2
      bitrate: 256000
      codecProfile: CBR
      numberOfSamples: 8396928
      duration: 190.4065306122449
    native:
      ID3v2.4:
        - id: TRCK
          value: '2'
        - id: TDRC
          value: '2015'
        - id: TIT2
          value: 7 Stepper
        - id: TPE1
          value: Sakrecoer
        - id: TALB
          value: Vaporizer
        - id: COMM
          value: &ref_1
            language: XXX
            description: ''
            text: sakrecoer.com
        - id: TCON
          value: Vapor Break
    quality:
      warnings: []
    common:
      track:
        'no': 2
        of: null
      disk:
        'no': null
        of: null
      year: 2015
      date: '2015'
      title: 7 Stepper
      artists:
        - Sakrecoer
      artist: Sakrecoer
      album: Vaporizer
      comment:
        - sakrecoer.com
      genre:
        - Vapor Break
    transformed:
      ID3v2.4:
        TRCK: '2'
        TDRC: '2015'
        TIT2: 7 Stepper
        TPE1: Sakrecoer
        TALB: Vaporizer
        COMM: *ref_1
        TCON: Vapor Break
    all:
      TRCK: '2'
      TDRC: '2015'
      TIT2: 7 Stepper
      TPE1: Sakrecoer
      TALB: Vaporizer
      COMM: *ref_1
      TCON: Vapor Break
  - path: >-
      /mnt/usb32gb/organized-sakrecoer.media/assets/albums/vaporizer/3-vapor-break.mp3
    audio: /assets/albums/vaporizer/3-vapor-break.mp3
    slug: vaporizer/3-vapor-break
    albumSlug: vaporizer
    trackSlug: 3-vapor-break
    coverPath: >-
      /mnt/usb32gb/organized-sakrecoer.media/assets/albums/vaporizer/3-vapor-break.jpeg
    cover: /assets/albums/vaporizer/3-vapor-break.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 2
      bitrate: 128000
      codecProfile: CBR
      numberOfSamples: 12613248
      duration: 286.014693877551
    native:
      ID3v2.3:
        - id: TIT2
          value: Vapor Break
        - id: TPE1
          value: Sakrecoer
        - id: COMM
          value: &ref_2
            language: eng
            description: ''
            text: sakrecoer.com
        - id: TRCK
          value: '3'
        - id: TCON
          value: Vapor Break
        - id: TALB
          value: Vaporizer
        - id: TYER
          value: '2015'
    quality:
      warnings: []
    common:
      track:
        'no': 3
        of: null
      disk:
        'no': null
        of: null
      title: Vapor Break
      artists:
        - Sakrecoer
      artist: Sakrecoer
      comment:
        - sakrecoer.com
      genre:
        - Vapor Break
      album: Vaporizer
      year: 2015
    transformed:
      ID3v2.3:
        TIT2: Vapor Break
        TPE1: Sakrecoer
        COMM: *ref_2
        TRCK: '3'
        TCON: Vapor Break
        TALB: Vaporizer
        TYER: '2015'
    all:
      TIT2: Vapor Break
      TPE1: Sakrecoer
      COMM: *ref_2
      TRCK: '3'
      TCON: Vapor Break
      TALB: Vaporizer
      TYER: '2015'
  - path: >-
      /mnt/usb32gb/organized-sakrecoer.media/assets/albums/vaporizer/4-subossa.mp3
    audio: /assets/albums/vaporizer/4-subossa.mp3
    slug: vaporizer/4-subossa
    albumSlug: vaporizer
    trackSlug: 4-subossa
    coverPath: >-
      /mnt/usb32gb/organized-sakrecoer.media/assets/albums/vaporizer/4-subossa.jpeg
    cover: /assets/albums/vaporizer/4-subossa.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 2
      bitrate: 128000
      codecProfile: CBR
      numberOfSamples: 9635328
      duration: 218.48816326530613
    native:
      ID3v2.3:
        - id: TIT2
          value: Subossa
        - id: TPE1
          value: Sakrecoer
        - id: TALB
          value: Vaporizer
        - id: COMM
          value: &ref_3
            language: eng
            description: ''
            text: sakrecoer.com
        - id: TRCK
          value: '4'
        - id: TCON
          value: Vapor Break
        - id: TYER
          value: '2015'
    quality:
      warnings: []
    common:
      track:
        'no': 4
        of: null
      disk:
        'no': null
        of: null
      title: Subossa
      artists:
        - Sakrecoer
      artist: Sakrecoer
      album: Vaporizer
      comment:
        - sakrecoer.com
      genre:
        - Vapor Break
      year: 2015
    transformed:
      ID3v2.3:
        TIT2: Subossa
        TPE1: Sakrecoer
        TALB: Vaporizer
        COMM: *ref_3
        TRCK: '4'
        TCON: Vapor Break
        TYER: '2015'
    all:
      TIT2: Subossa
      TPE1: Sakrecoer
      TALB: Vaporizer
      COMM: *ref_3
      TRCK: '4'
      TCON: Vapor Break
      TYER: '2015'
---
