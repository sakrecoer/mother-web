---
layout: album
slug: the-house-of-set
name: The House Of Set
artists: Simio Sakrecoer
bitrate: 128000
trackCount: 8
cover: /assets/albums/the-house-of-set/1-subway.jpeg
date: 2012-1-1
tracks:
  - path: >-
      /mnt/usb32gb/organized-sakrecoer.media/assets/albums/the-house-of-set/1-subway.mp3
    audio: /assets/albums/the-house-of-set/1-subway.mp3
    slug: the-house-of-set/1-subway
    albumSlug: the-house-of-set
    trackSlug: 1-subway
    coverPath: >-
      /mnt/usb32gb/organized-sakrecoer.media/assets/albums/the-house-of-set/1-subway.jpeg
    cover: /assets/albums/the-house-of-set/1-subway.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 2
      bitrate: 128000
      codecProfile: CBR
      numberOfSamples: 15352704
      duration: 348.1338775510204
    native:
      ID3v2.3:
        - id: TIT2
          value: Subway
        - id: TPE1
          value: Simio Sakrecoer
        - id: TALB
          value: The House Of Set
        - id: TYER
          value: '2012'
        - id: TRCK
          value: '1'
        - id: TCON
          value: Electronic
        - id: COMM
          value:
            language: XXX
            description: Comment
            text: sakrecoer.com/simio
    quality:
      warnings: []
    common:
      track:
        'no': 1
        of: null
      disk:
        'no': null
        of: null
      title: Subway
      artists:
        - Simio Sakrecoer
      artist: Simio Sakrecoer
      album: The House Of Set
      year: 2012
      genre:
        - Electronic
      comment:
        - sakrecoer.com/simio
    transformed:
      ID3v2.3:
        TIT2: Subway
        TPE1: Simio Sakrecoer
        TALB: The House Of Set
        TYER: '2012'
        TRCK: '1'
        TCON: Electronic
        Comment: sakrecoer.com/simio
    all:
      TIT2: Subway
      TPE1: Simio Sakrecoer
      TALB: The House Of Set
      TYER: '2012'
      TRCK: '1'
      TCON: Electronic
      Comment: sakrecoer.com/simio
  - path: >-
      /mnt/usb32gb/organized-sakrecoer.media/assets/albums/the-house-of-set/2-everyday-is-a-life.mp3
    audio: /assets/albums/the-house-of-set/2-everyday-is-a-life.mp3
    slug: the-house-of-set/2-everyday-is-a-life
    albumSlug: the-house-of-set
    trackSlug: 2-everyday-is-a-life
    coverPath: >-
      /mnt/usb32gb/organized-sakrecoer.media/assets/albums/the-house-of-set/2-everyday-is-a-life.jpeg
    cover: /assets/albums/the-house-of-set/2-everyday-is-a-life.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 2
      bitrate: 128000
      codecProfile: CBR
      numberOfSamples: 14555520
      duration: 330.0571428571429
    native:
      ID3v2.3:
        - id: TIT2
          value: Everyday Is A Life
        - id: TPE1
          value: Simio Sakrecoer
        - id: TALB
          value: The House Of Set
        - id: TRCK
          value: '2'
        - id: TCON
          value: Electronic
        - id: COMM
          value:
            language: XXX
            description: Comment
            text: sakrecoer.com/simio
        - id: TYER
          value: '2012'
    quality:
      warnings: []
    common:
      track:
        'no': 2
        of: null
      disk:
        'no': null
        of: null
      title: Everyday Is A Life
      artists:
        - Simio Sakrecoer
      artist: Simio Sakrecoer
      album: The House Of Set
      genre:
        - Electronic
      comment:
        - sakrecoer.com/simio
      year: 2012
    transformed:
      ID3v2.3:
        TIT2: Everyday Is A Life
        TPE1: Simio Sakrecoer
        TALB: The House Of Set
        TRCK: '2'
        TCON: Electronic
        Comment: sakrecoer.com/simio
        TYER: '2012'
    all:
      TIT2: Everyday Is A Life
      TPE1: Simio Sakrecoer
      TALB: The House Of Set
      TRCK: '2'
      TCON: Electronic
      Comment: sakrecoer.com/simio
      TYER: '2012'
  - path: >-
      /mnt/usb32gb/organized-sakrecoer.media/assets/albums/the-house-of-set/3-mossos-de-chicago.mp3
    audio: /assets/albums/the-house-of-set/3-mossos-de-chicago.mp3
    slug: the-house-of-set/3-mossos-de-chicago
    albumSlug: the-house-of-set
    trackSlug: 3-mossos-de-chicago
    coverPath: >-
      /mnt/usb32gb/organized-sakrecoer.media/assets/albums/the-house-of-set/3-mossos-de-chicago.jpeg
    cover: /assets/albums/the-house-of-set/3-mossos-de-chicago.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 2
      bitrate: 128000
      codecProfile: CBR
      numberOfSamples: 15466752
      duration: 350.72
    native:
      ID3v2.3:
        - id: TIT2
          value: Mossos De Chicago
        - id: TPE1
          value: Simio Sakrecoer
        - id: TALB
          value: The House Of Set
        - id: TYER
          value: '2012'
        - id: TRCK
          value: '3'
        - id: TCON
          value: Electronic
        - id: COMM
          value:
            language: XXX
            description: Comment
            text: sakrecoer.com/simio
    quality:
      warnings: []
    common:
      track:
        'no': 3
        of: null
      disk:
        'no': null
        of: null
      title: Mossos De Chicago
      artists:
        - Simio Sakrecoer
      artist: Simio Sakrecoer
      album: The House Of Set
      year: 2012
      genre:
        - Electronic
      comment:
        - sakrecoer.com/simio
    transformed:
      ID3v2.3:
        TIT2: Mossos De Chicago
        TPE1: Simio Sakrecoer
        TALB: The House Of Set
        TYER: '2012'
        TRCK: '3'
        TCON: Electronic
        Comment: sakrecoer.com/simio
    all:
      TIT2: Mossos De Chicago
      TPE1: Simio Sakrecoer
      TALB: The House Of Set
      TYER: '2012'
      TRCK: '3'
      TCON: Electronic
      Comment: sakrecoer.com/simio
  - path: >-
      /mnt/usb32gb/organized-sakrecoer.media/assets/albums/the-house-of-set/4-quasi-lucid.mp3
    audio: /assets/albums/the-house-of-set/4-quasi-lucid.mp3
    slug: the-house-of-set/4-quasi-lucid
    albumSlug: the-house-of-set
    trackSlug: 4-quasi-lucid
    coverPath: >-
      /mnt/usb32gb/organized-sakrecoer.media/assets/albums/the-house-of-set/4-quasi-lucid.jpeg
    cover: /assets/albums/the-house-of-set/4-quasi-lucid.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 2
      bitrate: 128000
      codecProfile: CBR
      numberOfSamples: 16237440
      duration: 368.19591836734696
    native:
      ID3v2.3:
        - id: TIT2
          value: Quasi Lucid
        - id: TPE1
          value: Simio Sakrecoer
        - id: TALB
          value: The House Of Set
        - id: TRCK
          value: '4'
        - id: TCON
          value: Electronic
        - id: COMM
          value:
            language: XXX
            description: Comment
            text: sakrecoer.com/simio
        - id: TYER
          value: '2012'
    quality:
      warnings: []
    common:
      track:
        'no': 4
        of: null
      disk:
        'no': null
        of: null
      title: Quasi Lucid
      artists:
        - Simio Sakrecoer
      artist: Simio Sakrecoer
      album: The House Of Set
      genre:
        - Electronic
      comment:
        - sakrecoer.com/simio
      year: 2012
    transformed:
      ID3v2.3:
        TIT2: Quasi Lucid
        TPE1: Simio Sakrecoer
        TALB: The House Of Set
        TRCK: '4'
        TCON: Electronic
        Comment: sakrecoer.com/simio
        TYER: '2012'
    all:
      TIT2: Quasi Lucid
      TPE1: Simio Sakrecoer
      TALB: The House Of Set
      TRCK: '4'
      TCON: Electronic
      Comment: sakrecoer.com/simio
      TYER: '2012'
  - path: >-
      /mnt/usb32gb/organized-sakrecoer.media/assets/albums/the-house-of-set/5-acid-forest.mp3
    audio: /assets/albums/the-house-of-set/5-acid-forest.mp3
    slug: the-house-of-set/5-acid-forest
    albumSlug: the-house-of-set
    trackSlug: 5-acid-forest
    coverPath: >-
      /mnt/usb32gb/organized-sakrecoer.media/assets/albums/the-house-of-set/5-acid-forest.jpeg
    cover: /assets/albums/the-house-of-set/5-acid-forest.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 2
      bitrate: 128000
      codecProfile: CBR
      numberOfSamples: 20156544
      duration: 457.06448979591835
    native:
      ID3v2.3:
        - id: TIT2
          value: Acid Forest
        - id: TPE1
          value: Simio Sakrecoer
        - id: TALB
          value: The House Of Set
        - id: TYER
          value: '2012'
        - id: TRCK
          value: '5'
        - id: TCON
          value: Electronic
        - id: COMM
          value:
            language: XXX
            description: Comment
            text: sakrecoer.com/simio
    quality:
      warnings: []
    common:
      track:
        'no': 5
        of: null
      disk:
        'no': null
        of: null
      title: Acid Forest
      artists:
        - Simio Sakrecoer
      artist: Simio Sakrecoer
      album: The House Of Set
      year: 2012
      genre:
        - Electronic
      comment:
        - sakrecoer.com/simio
    transformed:
      ID3v2.3:
        TIT2: Acid Forest
        TPE1: Simio Sakrecoer
        TALB: The House Of Set
        TYER: '2012'
        TRCK: '5'
        TCON: Electronic
        Comment: sakrecoer.com/simio
    all:
      TIT2: Acid Forest
      TPE1: Simio Sakrecoer
      TALB: The House Of Set
      TYER: '2012'
      TRCK: '5'
      TCON: Electronic
      Comment: sakrecoer.com/simio
  - path: >-
      /mnt/usb32gb/organized-sakrecoer.media/assets/albums/the-house-of-set/6-ask-me.mp3
    audio: /assets/albums/the-house-of-set/6-ask-me.mp3
    slug: the-house-of-set/6-ask-me
    albumSlug: the-house-of-set
    trackSlug: 6-ask-me
    coverPath: >-
      /mnt/usb32gb/organized-sakrecoer.media/assets/albums/the-house-of-set/6-ask-me.jpeg
    cover: /assets/albums/the-house-of-set/6-ask-me.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 2
      bitrate: 128000
      codecProfile: CBR
      numberOfSamples: 15260544
      duration: 346.04408163265305
    native:
      ID3v2.3:
        - id: TIT2
          value: Ask Me
        - id: TPE1
          value: Simio Sakrecoer
        - id: TALB
          value: The House Of Set
        - id: TRCK
          value: '6'
        - id: TCON
          value: Electronic
        - id: COMM
          value:
            language: XXX
            description: Comment
            text: sakrecoer.com/simio
        - id: TYER
          value: '2012'
    quality:
      warnings: []
    common:
      track:
        'no': 6
        of: null
      disk:
        'no': null
        of: null
      title: Ask Me
      artists:
        - Simio Sakrecoer
      artist: Simio Sakrecoer
      album: The House Of Set
      genre:
        - Electronic
      comment:
        - sakrecoer.com/simio
      year: 2012
    transformed:
      ID3v2.3:
        TIT2: Ask Me
        TPE1: Simio Sakrecoer
        TALB: The House Of Set
        TRCK: '6'
        TCON: Electronic
        Comment: sakrecoer.com/simio
        TYER: '2012'
    all:
      TIT2: Ask Me
      TPE1: Simio Sakrecoer
      TALB: The House Of Set
      TRCK: '6'
      TCON: Electronic
      Comment: sakrecoer.com/simio
      TYER: '2012'
  - path: >-
      /mnt/usb32gb/organized-sakrecoer.media/assets/albums/the-house-of-set/7-transcend-hip-hop.mp3
    audio: /assets/albums/the-house-of-set/7-transcend-hip-hop.mp3
    slug: the-house-of-set/7-transcend-hip-hop
    albumSlug: the-house-of-set
    trackSlug: 7-transcend-hip-hop
    coverPath: >-
      /mnt/usb32gb/organized-sakrecoer.media/assets/albums/the-house-of-set/7-transcend-hip-hop.jpeg
    cover: /assets/albums/the-house-of-set/7-transcend-hip-hop.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 2
      bitrate: 128000
      codecProfile: CBR
      numberOfSamples: 14911488
      duration: 338.12897959183675
    native:
      ID3v2.3:
        - id: TIT2
          value: Transcend Hip Hop
        - id: TPE1
          value: Simio Sakrecoer
        - id: TALB
          value: The House Of Set
        - id: TYER
          value: '2012'
        - id: TRCK
          value: '7'
        - id: TCON
          value: Electronic
        - id: COMM
          value:
            language: XXX
            description: Comment
            text: sakrecoer.com/simio
    quality:
      warnings: []
    common:
      track:
        'no': 7
        of: null
      disk:
        'no': null
        of: null
      title: Transcend Hip Hop
      artists:
        - Simio Sakrecoer
      artist: Simio Sakrecoer
      album: The House Of Set
      year: 2012
      genre:
        - Electronic
      comment:
        - sakrecoer.com/simio
    transformed:
      ID3v2.3:
        TIT2: Transcend Hip Hop
        TPE1: Simio Sakrecoer
        TALB: The House Of Set
        TYER: '2012'
        TRCK: '7'
        TCON: Electronic
        Comment: sakrecoer.com/simio
    all:
      TIT2: Transcend Hip Hop
      TPE1: Simio Sakrecoer
      TALB: The House Of Set
      TYER: '2012'
      TRCK: '7'
      TCON: Electronic
      Comment: sakrecoer.com/simio
  - path: >-
      /mnt/usb32gb/organized-sakrecoer.media/assets/albums/the-house-of-set/8-saint-fragle-day.mp3
    audio: /assets/albums/the-house-of-set/8-saint-fragle-day.mp3
    slug: the-house-of-set/8-saint-fragle-day
    albumSlug: the-house-of-set
    trackSlug: 8-saint-fragle-day
    coverPath: >-
      /mnt/usb32gb/organized-sakrecoer.media/assets/albums/the-house-of-set/8-saint-fragle-day.jpeg
    cover: /assets/albums/the-house-of-set/8-saint-fragle-day.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 2
      bitrate: 128000
      codecProfile: CBR
      numberOfSamples: 14337792
      duration: 325.12
    native:
      ID3v2.3:
        - id: TIT2
          value: Saint Fragle Day
        - id: TPE1
          value: Simio Sakrecoer
        - id: TALB
          value: The House Of Set
        - id: TRCK
          value: '8'
        - id: TCON
          value: Electronic
        - id: COMM
          value:
            language: XXX
            description: Comment
            text: sakrecoer.com/simio
        - id: TYER
          value: '2012'
    quality:
      warnings: []
    common:
      track:
        'no': 8
        of: null
      disk:
        'no': null
        of: null
      title: Saint Fragle Day
      artists:
        - Simio Sakrecoer
      artist: Simio Sakrecoer
      album: The House Of Set
      genre:
        - Electronic
      comment:
        - sakrecoer.com/simio
      year: 2012
    transformed:
      ID3v2.3:
        TIT2: Saint Fragle Day
        TPE1: Simio Sakrecoer
        TALB: The House Of Set
        TRCK: '8'
        TCON: Electronic
        Comment: sakrecoer.com/simio
        TYER: '2012'
    all:
      TIT2: Saint Fragle Day
      TPE1: Simio Sakrecoer
      TALB: The House Of Set
      TRCK: '8'
      TCON: Electronic
      Comment: sakrecoer.com/simio
      TYER: '2012'
---
