---
layout: album
slug: psykedelisk-pop
name: Psykedelisk Pop
artists: reSet Sakrecoer
bitrate: 128000
trackCount: 10
cover: /assets/albums/psykedelisk-pop/1-janne-i-min-hj-rna.jpeg
date: 2010-1-1
tracks:
  - path: >-
      /mnt/usb32gb/organized-sakrecoer.media/assets/albums/psykedelisk-pop/1-janne-i-min-hj-rna.mp3
    audio: /assets/albums/psykedelisk-pop/1-janne-i-min-hj-rna.mp3
    slug: psykedelisk-pop/1-janne-i-min-hj-rna
    albumSlug: psykedelisk-pop
    trackSlug: 1-janne-i-min-hj-rna
    coverPath: >-
      /mnt/usb32gb/organized-sakrecoer.media/assets/albums/psykedelisk-pop/1-janne-i-min-hj-rna.jpeg
    cover: /assets/albums/psykedelisk-pop/1-janne-i-min-hj-rna.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 2
      bitrate: 128000
      codecProfile: CBR
      numberOfSamples: 9080064
      duration: 205.89714285714285
    native:
      ID3v2.3:
        - id: TIT2
          value: Janne I Min Hjärna
        - id: TPE1
          value: reSet Sakrecoer
        - id: TPE2
          value: reSet Sakrecoer
        - id: TCOM
          value: reSet Sakrecoer
        - id: TALB
          value: Psykedelisk Pop
        - id: TRCK
          value: 1/10
        - id: TCON
          value: Pop
        - id: COMM
          value:
            language: XXX
            description: Comment
            text: 'https://sakrecoer.com'
        - id: TYER
          value: '2010'
    quality:
      warnings: []
    common:
      track:
        'no': 1
        of: 10
      disk:
        'no': null
        of: null
      title: Janne I Min Hjärna
      artists:
        - reSet Sakrecoer
      artist: reSet Sakrecoer
      albumartist: reSet Sakrecoer
      composer:
        - reSet Sakrecoer
      album: Psykedelisk Pop
      genre:
        - Pop
      comment:
        - 'https://sakrecoer.com'
      year: 2010
    transformed:
      ID3v2.3:
        TIT2: Janne I Min Hjärna
        TPE1: reSet Sakrecoer
        TPE2: reSet Sakrecoer
        TCOM: reSet Sakrecoer
        TALB: Psykedelisk Pop
        TRCK: 1/10
        TCON: Pop
        Comment: 'https://sakrecoer.com'
        TYER: '2010'
    all:
      TIT2: Janne I Min Hjärna
      TPE1: reSet Sakrecoer
      TPE2: reSet Sakrecoer
      TCOM: reSet Sakrecoer
      TALB: Psykedelisk Pop
      TRCK: 1/10
      TCON: Pop
      Comment: 'https://sakrecoer.com'
      TYER: '2010'
  - path: >-
      /mnt/usb32gb/organized-sakrecoer.media/assets/albums/psykedelisk-pop/2-step-up.mp3
    audio: /assets/albums/psykedelisk-pop/2-step-up.mp3
    slug: psykedelisk-pop/2-step-up
    albumSlug: psykedelisk-pop
    trackSlug: 2-step-up
    coverPath: >-
      /mnt/usb32gb/organized-sakrecoer.media/assets/albums/psykedelisk-pop/2-step-up.jpeg
    cover: /assets/albums/psykedelisk-pop/2-step-up.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 2
      bitrate: 128000
      codecProfile: CBR
      numberOfSamples: 8176896
      duration: 185.41714285714286
    native:
      ID3v2.3:
        - id: TIT2
          value: Step Up
        - id: TPE1
          value: reSet Sakrecoer
        - id: TPE2
          value: reSet Sakrecoer
        - id: TCOM
          value: reSet Sakrecoer
        - id: TALB
          value: Psykedelisk Pop
        - id: TRCK
          value: 2/10
        - id: TCON
          value: Hip-Hop
        - id: COMM
          value:
            language: XXX
            description: Comment
            text: 'https://sakrecoer.com'
        - id: TYER
          value: '2010'
    quality:
      warnings: []
    common:
      track:
        'no': 2
        of: 10
      disk:
        'no': null
        of: null
      title: Step Up
      artists:
        - reSet Sakrecoer
      artist: reSet Sakrecoer
      albumartist: reSet Sakrecoer
      composer:
        - reSet Sakrecoer
      album: Psykedelisk Pop
      genre:
        - Hip-Hop
      comment:
        - 'https://sakrecoer.com'
      year: 2010
    transformed:
      ID3v2.3:
        TIT2: Step Up
        TPE1: reSet Sakrecoer
        TPE2: reSet Sakrecoer
        TCOM: reSet Sakrecoer
        TALB: Psykedelisk Pop
        TRCK: 2/10
        TCON: Hip-Hop
        Comment: 'https://sakrecoer.com'
        TYER: '2010'
    all:
      TIT2: Step Up
      TPE1: reSet Sakrecoer
      TPE2: reSet Sakrecoer
      TCOM: reSet Sakrecoer
      TALB: Psykedelisk Pop
      TRCK: 2/10
      TCON: Hip-Hop
      Comment: 'https://sakrecoer.com'
      TYER: '2010'
  - path: >-
      /mnt/usb32gb/organized-sakrecoer.media/assets/albums/psykedelisk-pop/3-roevhjaelmen.mp3
    audio: /assets/albums/psykedelisk-pop/3-roevhjaelmen.mp3
    slug: psykedelisk-pop/3-roevhjaelmen
    albumSlug: psykedelisk-pop
    trackSlug: 3-roevhjaelmen
    coverPath: >-
      /mnt/usb32gb/organized-sakrecoer.media/assets/albums/psykedelisk-pop/3-roevhjaelmen.jpeg
    cover: /assets/albums/psykedelisk-pop/3-roevhjaelmen.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 2
      bitrate: 128000
      codecProfile: CBR
      numberOfSamples: 11242368
      duration: 254.92897959183674
    native:
      ID3v2.3:
        - id: TIT2
          value: Roevhjaelmen
        - id: TPE1
          value: reSet Sakrecoer
        - id: TPE2
          value: reSet Sakrecoer
        - id: TCOM
          value: reSet Sakrecoer
        - id: TALB
          value: Psykedelisk Pop
        - id: TRCK
          value: 3/10
        - id: TCON
          value: House
        - id: COMM
          value:
            language: XXX
            description: Comment
            text: 'https://sakrecoer.com'
        - id: TYER
          value: '2010'
    quality:
      warnings: []
    common:
      track:
        'no': 3
        of: 10
      disk:
        'no': null
        of: null
      title: Roevhjaelmen
      artists:
        - reSet Sakrecoer
      artist: reSet Sakrecoer
      albumartist: reSet Sakrecoer
      composer:
        - reSet Sakrecoer
      album: Psykedelisk Pop
      genre:
        - House
      comment:
        - 'https://sakrecoer.com'
      year: 2010
    transformed:
      ID3v2.3:
        TIT2: Roevhjaelmen
        TPE1: reSet Sakrecoer
        TPE2: reSet Sakrecoer
        TCOM: reSet Sakrecoer
        TALB: Psykedelisk Pop
        TRCK: 3/10
        TCON: House
        Comment: 'https://sakrecoer.com'
        TYER: '2010'
    all:
      TIT2: Roevhjaelmen
      TPE1: reSet Sakrecoer
      TPE2: reSet Sakrecoer
      TCOM: reSet Sakrecoer
      TALB: Psykedelisk Pop
      TRCK: 3/10
      TCON: House
      Comment: 'https://sakrecoer.com'
      TYER: '2010'
  - path: >-
      /mnt/usb32gb/organized-sakrecoer.media/assets/albums/psykedelisk-pop/4-it-s-all-over.mp3
    audio: /assets/albums/psykedelisk-pop/4-it-s-all-over.mp3
    slug: psykedelisk-pop/4-it-s-all-over
    albumSlug: psykedelisk-pop
    trackSlug: 4-it-s-all-over
    coverPath: >-
      /mnt/usb32gb/organized-sakrecoer.media/assets/albums/psykedelisk-pop/4-it-s-all-over.jpeg
    cover: /assets/albums/psykedelisk-pop/4-it-s-all-over.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 2
      bitrate: 128000
      codecProfile: CBR
      numberOfSamples: 11700864
      duration: 265.3257142857143
    native:
      ID3v2.3:
        - id: TIT2
          value: It's All Over
        - id: TPE1
          value: reSet Sakrecoer
        - id: TPE2
          value: reSet Sakrecoer
        - id: TCOM
          value: reSet Sakrecoer
        - id: TALB
          value: Psykedelisk Pop
        - id: TRCK
          value: 4/10
        - id: TCON
          value: Emo
        - id: COMM
          value:
            language: XXX
            description: Comment
            text: 'https://sakrecoer.com'
        - id: TYER
          value: '2010'
    quality:
      warnings: []
    common:
      track:
        'no': 4
        of: 10
      disk:
        'no': null
        of: null
      title: It's All Over
      artists:
        - reSet Sakrecoer
      artist: reSet Sakrecoer
      albumartist: reSet Sakrecoer
      composer:
        - reSet Sakrecoer
      album: Psykedelisk Pop
      genre:
        - Emo
      comment:
        - 'https://sakrecoer.com'
      year: 2010
    transformed:
      ID3v2.3:
        TIT2: It's All Over
        TPE1: reSet Sakrecoer
        TPE2: reSet Sakrecoer
        TCOM: reSet Sakrecoer
        TALB: Psykedelisk Pop
        TRCK: 4/10
        TCON: Emo
        Comment: 'https://sakrecoer.com'
        TYER: '2010'
    all:
      TIT2: It's All Over
      TPE1: reSet Sakrecoer
      TPE2: reSet Sakrecoer
      TCOM: reSet Sakrecoer
      TALB: Psykedelisk Pop
      TRCK: 4/10
      TCON: Emo
      Comment: 'https://sakrecoer.com'
      TYER: '2010'
  - path: >-
      /mnt/usb32gb/organized-sakrecoer.media/assets/albums/psykedelisk-pop/5-luftboejning.mp3
    audio: /assets/albums/psykedelisk-pop/5-luftboejning.mp3
    slug: psykedelisk-pop/5-luftboejning
    albumSlug: psykedelisk-pop
    trackSlug: 5-luftboejning
    coverPath: >-
      /mnt/usb32gb/organized-sakrecoer.media/assets/albums/psykedelisk-pop/5-luftboejning.jpeg
    cover: /assets/albums/psykedelisk-pop/5-luftboejning.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 2
      bitrate: 128000
      codecProfile: CBR
      numberOfSamples: 19203840
      duration: 435.46122448979594
    native:
      ID3v2.3:
        - id: TIT2
          value: Luftboejning
        - id: TPE1
          value: reSet Sakrecoer
        - id: TPE2
          value: reSet Sakrecoer
        - id: TCOM
          value: reSet Sakrecoer
        - id: TALB
          value: Psykedelisk Pop
        - id: TRCK
          value: 5/10
        - id: TCON
          value: Alternative Pop
        - id: COMM
          value:
            language: XXX
            description: Comment
            text: 'https://sakrecoer.com'
        - id: TYER
          value: '2010'
    quality:
      warnings: []
    common:
      track:
        'no': 5
        of: 10
      disk:
        'no': null
        of: null
      title: Luftboejning
      artists:
        - reSet Sakrecoer
      artist: reSet Sakrecoer
      albumartist: reSet Sakrecoer
      composer:
        - reSet Sakrecoer
      album: Psykedelisk Pop
      genre:
        - Alternative Pop
      comment:
        - 'https://sakrecoer.com'
      year: 2010
    transformed:
      ID3v2.3:
        TIT2: Luftboejning
        TPE1: reSet Sakrecoer
        TPE2: reSet Sakrecoer
        TCOM: reSet Sakrecoer
        TALB: Psykedelisk Pop
        TRCK: 5/10
        TCON: Alternative Pop
        Comment: 'https://sakrecoer.com'
        TYER: '2010'
    all:
      TIT2: Luftboejning
      TPE1: reSet Sakrecoer
      TPE2: reSet Sakrecoer
      TCOM: reSet Sakrecoer
      TALB: Psykedelisk Pop
      TRCK: 5/10
      TCON: Alternative Pop
      Comment: 'https://sakrecoer.com'
      TYER: '2010'
  - path: >-
      /mnt/usb32gb/organized-sakrecoer.media/assets/albums/psykedelisk-pop/6-itelligent-musik.mp3
    audio: /assets/albums/psykedelisk-pop/6-itelligent-musik.mp3
    slug: psykedelisk-pop/6-itelligent-musik
    albumSlug: psykedelisk-pop
    trackSlug: 6-itelligent-musik
    coverPath: >-
      /mnt/usb32gb/organized-sakrecoer.media/assets/albums/psykedelisk-pop/6-itelligent-musik.jpeg
    cover: /assets/albums/psykedelisk-pop/6-itelligent-musik.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 2
      bitrate: 128000
      codecProfile: CBR
      numberOfSamples: 10062720
      duration: 228.1795918367347
    native:
      ID3v2.3:
        - id: TIT2
          value: Itelligent Musik
        - id: TPE1
          value: reSet Sakrecoer
        - id: TPE2
          value: reSet Sakrecoer
        - id: TCOM
          value: reSet Sakrecoer
        - id: TALB
          value: Psykedelisk Pop
        - id: TRCK
          value: 6/10
        - id: TBPM
          value: '127'
        - id: TCON
          value: Alternative Pop
        - id: COMM
          value:
            language: XXX
            description: Comment
            text: 'https://sakrecoer.com'
        - id: TYER
          value: '2010'
    quality:
      warnings: []
    common:
      track:
        'no': 6
        of: 10
      disk:
        'no': null
        of: null
      title: Itelligent Musik
      artists:
        - reSet Sakrecoer
      artist: reSet Sakrecoer
      albumartist: reSet Sakrecoer
      composer:
        - reSet Sakrecoer
      album: Psykedelisk Pop
      bpm: '127'
      genre:
        - Alternative Pop
      comment:
        - 'https://sakrecoer.com'
      year: 2010
    transformed:
      ID3v2.3:
        TIT2: Itelligent Musik
        TPE1: reSet Sakrecoer
        TPE2: reSet Sakrecoer
        TCOM: reSet Sakrecoer
        TALB: Psykedelisk Pop
        TRCK: 6/10
        TBPM: '127'
        TCON: Alternative Pop
        Comment: 'https://sakrecoer.com'
        TYER: '2010'
    all:
      TIT2: Itelligent Musik
      TPE1: reSet Sakrecoer
      TPE2: reSet Sakrecoer
      TCOM: reSet Sakrecoer
      TALB: Psykedelisk Pop
      TRCK: 6/10
      TBPM: '127'
      TCON: Alternative Pop
      Comment: 'https://sakrecoer.com'
      TYER: '2010'
  - path: >-
      /mnt/usb32gb/organized-sakrecoer.media/assets/albums/psykedelisk-pop/7-alive.mp3
    audio: /assets/albums/psykedelisk-pop/7-alive.mp3
    slug: psykedelisk-pop/7-alive
    albumSlug: psykedelisk-pop
    trackSlug: 7-alive
    coverPath: >-
      /mnt/usb32gb/organized-sakrecoer.media/assets/albums/psykedelisk-pop/7-alive.jpeg
    cover: /assets/albums/psykedelisk-pop/7-alive.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 2
      bitrate: 128000
      codecProfile: CBR
      numberOfSamples: 8669952
      duration: 196.59755102040816
    native:
      ID3v2.3:
        - id: TIT2
          value: Alive
        - id: TPE1
          value: reSet Sakrecoer
        - id: TPE2
          value: reSet Sakrecoer
        - id: TCOM
          value: reSet Sakrecoer
        - id: TALB
          value: Psykedelisk Pop
        - id: TRCK
          value: 7/10
        - id: TBPM
          value: '127'
        - id: TCON
          value: Alternative Pop
        - id: COMM
          value:
            language: XXX
            description: Comment
            text: 'https://sakrecoer.com'
        - id: TYER
          value: '2010'
    quality:
      warnings: []
    common:
      track:
        'no': 7
        of: 10
      disk:
        'no': null
        of: null
      title: Alive
      artists:
        - reSet Sakrecoer
      artist: reSet Sakrecoer
      albumartist: reSet Sakrecoer
      composer:
        - reSet Sakrecoer
      album: Psykedelisk Pop
      bpm: '127'
      genre:
        - Alternative Pop
      comment:
        - 'https://sakrecoer.com'
      year: 2010
    transformed:
      ID3v2.3:
        TIT2: Alive
        TPE1: reSet Sakrecoer
        TPE2: reSet Sakrecoer
        TCOM: reSet Sakrecoer
        TALB: Psykedelisk Pop
        TRCK: 7/10
        TBPM: '127'
        TCON: Alternative Pop
        Comment: 'https://sakrecoer.com'
        TYER: '2010'
    all:
      TIT2: Alive
      TPE1: reSet Sakrecoer
      TPE2: reSet Sakrecoer
      TCOM: reSet Sakrecoer
      TALB: Psykedelisk Pop
      TRCK: 7/10
      TBPM: '127'
      TCON: Alternative Pop
      Comment: 'https://sakrecoer.com'
      TYER: '2010'
  - path: >-
      /mnt/usb32gb/organized-sakrecoer.media/assets/albums/psykedelisk-pop/8-invitiation-to-dance.mp3
    audio: /assets/albums/psykedelisk-pop/8-invitiation-to-dance.mp3
    slug: psykedelisk-pop/8-invitiation-to-dance
    albumSlug: psykedelisk-pop
    trackSlug: 8-invitiation-to-dance
    coverPath: >-
      /mnt/usb32gb/organized-sakrecoer.media/assets/albums/psykedelisk-pop/8-invitiation-to-dance.jpeg
    cover: /assets/albums/psykedelisk-pop/8-invitiation-to-dance.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 2
      bitrate: 128000
      codecProfile: CBR
      numberOfSamples: 22284288
      duration: 505.31265306122447
    native:
      ID3v2.3:
        - id: TIT2
          value: Invitiation To Dance
        - id: TPE1
          value: reSet Sakrecoer
        - id: TPE2
          value: reSet Sakrecoer
        - id: TCOM
          value: reSet Sakrecoer
        - id: TALB
          value: Psykedelisk Pop
        - id: TRCK
          value: 8/10
        - id: TBPM
          value: '127'
        - id: TCON
          value: House
        - id: COMM
          value:
            language: XXX
            description: Comment
            text: 'https://sakrecoer.com'
        - id: TYER
          value: '2010'
    quality:
      warnings: []
    common:
      track:
        'no': 8
        of: 10
      disk:
        'no': null
        of: null
      title: Invitiation To Dance
      artists:
        - reSet Sakrecoer
      artist: reSet Sakrecoer
      albumartist: reSet Sakrecoer
      composer:
        - reSet Sakrecoer
      album: Psykedelisk Pop
      bpm: '127'
      genre:
        - House
      comment:
        - 'https://sakrecoer.com'
      year: 2010
    transformed:
      ID3v2.3:
        TIT2: Invitiation To Dance
        TPE1: reSet Sakrecoer
        TPE2: reSet Sakrecoer
        TCOM: reSet Sakrecoer
        TALB: Psykedelisk Pop
        TRCK: 8/10
        TBPM: '127'
        TCON: House
        Comment: 'https://sakrecoer.com'
        TYER: '2010'
    all:
      TIT2: Invitiation To Dance
      TPE1: reSet Sakrecoer
      TPE2: reSet Sakrecoer
      TCOM: reSet Sakrecoer
      TALB: Psykedelisk Pop
      TRCK: 8/10
      TBPM: '127'
      TCON: House
      Comment: 'https://sakrecoer.com'
      TYER: '2010'
  - path: >-
      /mnt/usb32gb/organized-sakrecoer.media/assets/albums/psykedelisk-pop/9-drugs.mp3
    audio: /assets/albums/psykedelisk-pop/9-drugs.mp3
    slug: psykedelisk-pop/9-drugs
    albumSlug: psykedelisk-pop
    trackSlug: 9-drugs
    coverPath: >-
      /mnt/usb32gb/organized-sakrecoer.media/assets/albums/psykedelisk-pop/9-drugs.jpeg
    cover: /assets/albums/psykedelisk-pop/9-drugs.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 2
      bitrate: 128000
      codecProfile: CBR
      numberOfSamples: 10166400
      duration: 230.53061224489795
    native:
      ID3v2.3:
        - id: TIT2
          value: Drugs
        - id: TPE1
          value: reSet Sakrecoer
        - id: TPE2
          value: reSet Sakrecoer
        - id: TCOM
          value: reSet Sakrecoer
        - id: TALB
          value: Psykedelisk Pop
        - id: TRCK
          value: 9/10
        - id: TBPM
          value: '127'
        - id: TCON
          value: Alternative Pop
        - id: COMM
          value:
            language: XXX
            description: Comment
            text: 'https://sakrecoer.com'
        - id: TYER
          value: '2010'
    quality:
      warnings: []
    common:
      track:
        'no': 9
        of: 10
      disk:
        'no': null
        of: null
      title: Drugs
      artists:
        - reSet Sakrecoer
      artist: reSet Sakrecoer
      albumartist: reSet Sakrecoer
      composer:
        - reSet Sakrecoer
      album: Psykedelisk Pop
      bpm: '127'
      genre:
        - Alternative Pop
      comment:
        - 'https://sakrecoer.com'
      year: 2010
    transformed:
      ID3v2.3:
        TIT2: Drugs
        TPE1: reSet Sakrecoer
        TPE2: reSet Sakrecoer
        TCOM: reSet Sakrecoer
        TALB: Psykedelisk Pop
        TRCK: 9/10
        TBPM: '127'
        TCON: Alternative Pop
        Comment: 'https://sakrecoer.com'
        TYER: '2010'
    all:
      TIT2: Drugs
      TPE1: reSet Sakrecoer
      TPE2: reSet Sakrecoer
      TCOM: reSet Sakrecoer
      TALB: Psykedelisk Pop
      TRCK: 9/10
      TBPM: '127'
      TCON: Alternative Pop
      Comment: 'https://sakrecoer.com'
      TYER: '2010'
  - path: >-
      /mnt/usb32gb/organized-sakrecoer.media/assets/albums/psykedelisk-pop/10-hidden-track.mp3
    audio: /assets/albums/psykedelisk-pop/10-hidden-track.mp3
    slug: psykedelisk-pop/10-hidden-track
    albumSlug: psykedelisk-pop
    trackSlug: 10-hidden-track
    coverPath: >-
      /mnt/usb32gb/organized-sakrecoer.media/assets/albums/psykedelisk-pop/10-hidden-track.jpeg
    cover: /assets/albums/psykedelisk-pop/10-hidden-track.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 2
      bitrate: 128000
      codecProfile: CBR
      numberOfSamples: 6984576
      duration: 158.38040816326532
    native:
      ID3v2.3:
        - id: TIT2
          value: HIdden Track
        - id: TPE1
          value: reSet Sakrecoer
        - id: TPE2
          value: reSet Sakrecoer
        - id: TCOM
          value: reSet Sakrecoer
        - id: TALB
          value: Psykedelisk Pop
        - id: TRCK
          value: 10/10
        - id: TBPM
          value: '127'
        - id: TCON
          value: Punk
        - id: COMM
          value:
            language: XXX
            description: Comment
            text: 'https://sakrecoer.com'
        - id: TYER
          value: '2010'
    quality:
      warnings: []
    common:
      track:
        'no': 10
        of: 10
      disk:
        'no': null
        of: null
      title: HIdden Track
      artists:
        - reSet Sakrecoer
      artist: reSet Sakrecoer
      albumartist: reSet Sakrecoer
      composer:
        - reSet Sakrecoer
      album: Psykedelisk Pop
      bpm: '127'
      genre:
        - Punk
      comment:
        - 'https://sakrecoer.com'
      year: 2010
    transformed:
      ID3v2.3:
        TIT2: HIdden Track
        TPE1: reSet Sakrecoer
        TPE2: reSet Sakrecoer
        TCOM: reSet Sakrecoer
        TALB: Psykedelisk Pop
        TRCK: 10/10
        TBPM: '127'
        TCON: Punk
        Comment: 'https://sakrecoer.com'
        TYER: '2010'
    all:
      TIT2: HIdden Track
      TPE1: reSet Sakrecoer
      TPE2: reSet Sakrecoer
      TCOM: reSet Sakrecoer
      TALB: Psykedelisk Pop
      TRCK: 10/10
      TBPM: '127'
      TCON: Punk
      Comment: 'https://sakrecoer.com'
      TYER: '2010'
---
