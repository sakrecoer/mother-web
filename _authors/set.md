---
name: Set Sakrecoer
position: 'Artist'
image: /assets/img/staff/set.png
url_staff: 'https://sakrecoer.com'
email: 'public@sakrecoer.com'
gpg: 'https://keyserver.ubuntu.com/pks/lookup?op=get&search=0x92401D3F102A0208'
blurb_markup: 'Multidisciplinary humano&iuml;d.'
---